<?php return array (
  'fb2' => 
  array (
    0 => 'application/x-fictionbook',
  ),
  'gz' => 
  array (
    0 => 'application/x-gzip',
    1 => 'application/gzip',
  ),
  'rtf' => 
  array (
    0 => 'text/rtf',
    1 => 'application/rtf',
  ),
  123 => 
  array (
    0 => 'application/vnd.lotus-1-2-3',
  ),
  '3dml' => 
  array (
    0 => 'text/vnd.in3d.3dml',
  ),
  '3g2' => 
  array (
    0 => 'video/3gpp2',
  ),
  '3gp' => 
  array (
    0 => 'video/3gpp',
  ),
  '7z' => 
  array (
    0 => 'application/x-7z-compressed',
  ),
  'aab' => 
  array (
    0 => 'application/x-authorware-bin',
  ),
  'aac' => 
  array (
    0 => 'audio/x-aac',
  ),
  'aam' => 
  array (
    0 => 'application/x-authorware-map',
  ),
  'aas' => 
  array (
    0 => 'application/x-authorware-seg',
  ),
  'abw' => 
  array (
    0 => 'application/x-abiword',
  ),
  'ac' => 
  array (
    0 => 'application/pkix-attr-cert',
  ),
  'acc' => 
  array (
    0 => 'application/vnd.americandynamics.acc',
  ),
  'ace' => 
  array (
    0 => 'application/x-ace-compressed',
  ),
  'acu' => 
  array (
    0 => 'application/vnd.acucobol',
  ),
  'adp' => 
  array (
    0 => 'audio/adpcm',
  ),
  'aep' => 
  array (
    0 => 'application/vnd.audiograph',
  ),
  'afp' => 
  array (
    0 => 'application/vnd.ibm.modcap',
  ),
  'ahead' => 
  array (
    0 => 'application/vnd.ahead.space',
  ),
  'ai' => 
  array (
    0 => 'application/postscript',
  ),
  'aif' => 
  array (
    0 => 'audio/x-aiff',
  ),
  'air' => 
  array (
    0 => 'application/vnd.adobe.air-application-installer-package+zip',
  ),
  'ait' => 
  array (
    0 => 'application/vnd.dvb.ait',
  ),
  'ami' => 
  array (
    0 => 'application/vnd.amiga.ami',
  ),
  'apk' => 
  array (
    0 => 'application/vnd.android.package-archive',
  ),
  'application' => 
  array (
    0 => 'application/x-ms-application',
  ),
  'apr' => 
  array (
    0 => 'application/vnd.lotus-approach',
  ),
  'asf' => 
  array (
    0 => 'video/x-ms-asf',
  ),
  'aso' => 
  array (
    0 => 'application/vnd.accpac.simply.aso',
  ),
  'atc' => 
  array (
    0 => 'application/vnd.acucorp',
  ),
  'atom' => 
  array (
    0 => 'application/atom+xml',
  ),
  'atomcat' => 
  array (
    0 => 'application/atomcat+xml',
  ),
  'atomsvc' => 
  array (
    0 => 'application/atomsvc+xml',
  ),
  'atx' => 
  array (
    0 => 'application/vnd.antix.game-component',
  ),
  'au' => 
  array (
    0 => 'audio/basic',
  ),
  'avi' => 
  array (
    0 => 'video/x-msvideo',
  ),
  'aw' => 
  array (
    0 => 'application/applixware',
  ),
  'azf' => 
  array (
    0 => 'application/vnd.airzip.filesecure.azf',
  ),
  'azs' => 
  array (
    0 => 'application/vnd.airzip.filesecure.azs',
  ),
  'azw' => 
  array (
    0 => 'application/vnd.amazon.ebook',
  ),
  'bcpio' => 
  array (
    0 => 'application/x-bcpio',
  ),
  'bdf' => 
  array (
    0 => 'application/x-font-bdf',
  ),
  'bdm' => 
  array (
    0 => 'application/vnd.syncml.dm+wbxml',
  ),
  'bed' => 
  array (
    0 => 'application/vnd.realvnc.bed',
  ),
  'bh2' => 
  array (
    0 => 'application/vnd.fujitsu.oasysprs',
  ),
  'bin' => 
  array (
    0 => 'application/octet-stream',
  ),
  'bmi' => 
  array (
    0 => 'application/vnd.bmi',
  ),
  'bmp' => 
  array (
    0 => 'image/bmp',
    1 => 'image/x-ms-bmp',
  ),
  'box' => 
  array (
    0 => 'application/vnd.previewsystems.box',
  ),
  'btif' => 
  array (
    0 => 'image/prs.btif',
  ),
  'bz' => 
  array (
    0 => 'application/x-bzip',
  ),
  'bz2' => 
  array (
    0 => 'application/x-bzip2',
  ),
  'c' => 
  array (
    0 => 'text/x-c',
    1 => 'text/x-csrc',
  ),
  'c11amc' => 
  array (
    0 => 'application/vnd.cluetrust.cartomobile-config',
  ),
  'c11amz' => 
  array (
    0 => 'application/vnd.cluetrust.cartomobile-config-pkg',
  ),
  'c4g' => 
  array (
    0 => 'application/vnd.clonk.c4group',
  ),
  'cab' => 
  array (
    0 => 'application/vnd.ms-cab-compressed',
    1 => 'application/x-cab',
  ),
  'car' => 
  array (
    0 => 'application/vnd.curl.car',
  ),
  'cat' => 
  array (
    0 => 'application/vnd.ms-pki.seccat',
  ),
  'ccxml' => 
  array (
    0 => 'application/ccxml+xml,',
    1 => 'application/ccxml+xml',
  ),
  'cdbcmsg' => 
  array (
    0 => 'application/vnd.contact.cmsg',
  ),
  'cdkey' => 
  array (
    0 => 'application/vnd.mediastation.cdkey',
  ),
  'cdmia' => 
  array (
    0 => 'application/cdmi-capability',
  ),
  'cdmic' => 
  array (
    0 => 'application/cdmi-container',
  ),
  'cdmid' => 
  array (
    0 => 'application/cdmi-domain',
  ),
  'cdmio' => 
  array (
    0 => 'application/cdmi-object',
  ),
  'cdmiq' => 
  array (
    0 => 'application/cdmi-queue',
  ),
  'cdx' => 
  array (
    0 => 'chemical/x-cdx',
  ),
  'cdxml' => 
  array (
    0 => 'application/vnd.chemdraw+xml',
  ),
  'cdy' => 
  array (
    0 => 'application/vnd.cinderella',
  ),
  'cer' => 
  array (
    0 => 'application/pkix-cert',
    1 => 'chemical/x-cerius',
  ),
  'cgm' => 
  array (
    0 => 'image/cgm',
  ),
  'chat' => 
  array (
    0 => 'application/x-chat',
  ),
  'chm' => 
  array (
    0 => 'application/vnd.ms-htmlhelp',
    1 => 'chemical/x-chemdraw',
  ),
  'chrt' => 
  array (
    0 => 'application/vnd.kde.kchart',
    1 => 'application/x-kchart',
  ),
  'cif' => 
  array (
    0 => 'chemical/x-cif',
  ),
  'cii' => 
  array (
    0 => 'application/vnd.anser-web-certificate-issue-initiation',
  ),
  'cil' => 
  array (
    0 => 'application/vnd.ms-artgalry',
  ),
  'cla' => 
  array (
    0 => 'application/vnd.claymore',
  ),
  'class' => 
  array (
    0 => 'application/java-vm',
  ),
  'clkk' => 
  array (
    0 => 'application/vnd.crick.clicker.keyboard',
  ),
  'clkp' => 
  array (
    0 => 'application/vnd.crick.clicker.palette',
  ),
  'clkt' => 
  array (
    0 => 'application/vnd.crick.clicker.template',
  ),
  'clkw' => 
  array (
    0 => 'application/vnd.crick.clicker.wordbank',
  ),
  'clkx' => 
  array (
    0 => 'application/vnd.crick.clicker',
  ),
  'clp' => 
  array (
    0 => 'application/x-msclip',
  ),
  'cmc' => 
  array (
    0 => 'application/vnd.cosmocaller',
  ),
  'cmdf' => 
  array (
    0 => 'chemical/x-cmdf',
  ),
  'cml' => 
  array (
    0 => 'chemical/x-cml',
  ),
  'cmp' => 
  array (
    0 => 'application/vnd.yellowriver-custom-menu',
  ),
  'cmx' => 
  array (
    0 => 'image/x-cmx',
  ),
  'cod' => 
  array (
    0 => 'application/vnd.rim.cod',
  ),
  'cpio' => 
  array (
    0 => 'application/x-cpio',
  ),
  'cpt' => 
  array (
    0 => 'application/mac-compactpro',
    1 => 'image/x-corelphotopaint',
  ),
  'crd' => 
  array (
    0 => 'application/x-mscardfile',
  ),
  'crl' => 
  array (
    0 => 'application/pkix-crl',
  ),
  'cryptonote' => 
  array (
    0 => 'application/vnd.rig.cryptonote',
  ),
  'csh' => 
  array (
    0 => 'application/x-csh',
    1 => 'text/x-csh',
  ),
  'csml' => 
  array (
    0 => 'chemical/x-csml',
  ),
  'csp' => 
  array (
    0 => 'application/vnd.commonspace',
  ),
  'css' => 
  array (
    0 => 'text/css',
  ),
  'csv' => 
  array (
    0 => 'text/csv',
  ),
  'cu' => 
  array (
    0 => 'application/cu-seeme',
  ),
  'curl' => 
  array (
    0 => 'text/vnd.curl',
  ),
  'cww' => 
  array (
    0 => 'application/prs.cww',
  ),
  'dae' => 
  array (
    0 => 'model/vnd.collada+xml',
  ),
  'daf' => 
  array (
    0 => 'application/vnd.mobius.daf',
  ),
  'davmount' => 
  array (
    0 => 'application/davmount+xml',
  ),
  'dcurl' => 
  array (
    0 => 'text/vnd.curl.dcurl',
  ),
  'dd2' => 
  array (
    0 => 'application/vnd.oma.dd2+xml',
  ),
  'ddd' => 
  array (
    0 => 'application/vnd.fujixerox.ddd',
  ),
  'deb' => 
  array (
    0 => 'application/x-debian-package',
    1 => 'application/vnd.debian.binary-package',
    2 => 'application/octet-stream',
  ),
  'der' => 
  array (
    0 => 'application/x-x509-ca-cert',
  ),
  'dfac' => 
  array (
    0 => 'application/vnd.dreamfactory',
  ),
  'dir' => 
  array (
    0 => 'application/x-director',
  ),
  'dis' => 
  array (
    0 => 'application/vnd.mobius.dis',
  ),
  'djvu' => 
  array (
    0 => 'image/vnd.djvu',
  ),
  'dna' => 
  array (
    0 => 'application/vnd.dna',
  ),
  'doc' => 
  array (
    0 => 'application/msword',
  ),
  'docm' => 
  array (
    0 => 'application/vnd.ms-word.document.macroenabled.12',
  ),
  'docx' => 
  array (
    0 => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  ),
  'dotm' => 
  array (
    0 => 'application/vnd.ms-word.template.macroenabled.12',
  ),
  'dotx' => 
  array (
    0 => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
  ),
  'dp' => 
  array (
    0 => 'application/vnd.osgi.dp',
  ),
  'dpg' => 
  array (
    0 => 'application/vnd.dpgraph',
  ),
  'dra' => 
  array (
    0 => 'audio/vnd.dra',
  ),
  'dsc' => 
  array (
    0 => 'text/prs.lines.tag',
  ),
  'dssc' => 
  array (
    0 => 'application/dssc+der',
  ),
  'dtb' => 
  array (
    0 => 'application/x-dtbook+xml',
  ),
  'dtd' => 
  array (
    0 => 'application/xml-dtd',
  ),
  'dts' => 
  array (
    0 => 'audio/vnd.dts',
  ),
  'dtshd' => 
  array (
    0 => 'audio/vnd.dts.hd',
  ),
  'dvi' => 
  array (
    0 => 'application/x-dvi',
  ),
  'dwf' => 
  array (
    0 => 'model/vnd.dwf',
  ),
  'dwg' => 
  array (
    0 => 'image/vnd.dwg',
  ),
  'dxf' => 
  array (
    0 => 'image/vnd.dxf',
  ),
  'dxp' => 
  array (
    0 => 'application/vnd.spotfire.dxp',
  ),
  'ecelp4800' => 
  array (
    0 => 'audio/vnd.nuera.ecelp4800',
  ),
  'ecelp7470' => 
  array (
    0 => 'audio/vnd.nuera.ecelp7470',
  ),
  'ecelp9600' => 
  array (
    0 => 'audio/vnd.nuera.ecelp9600',
  ),
  'edm' => 
  array (
    0 => 'application/vnd.novadigm.edm',
  ),
  'edx' => 
  array (
    0 => 'application/vnd.novadigm.edx',
  ),
  'efif' => 
  array (
    0 => 'application/vnd.picsel',
  ),
  'ei6' => 
  array (
    0 => 'application/vnd.pg.osasli',
  ),
  'eml' => 
  array (
    0 => 'message/rfc822',
  ),
  'emma' => 
  array (
    0 => 'application/emma+xml',
  ),
  'eol' => 
  array (
    0 => 'audio/vnd.digital-winds',
  ),
  'eot' => 
  array (
    0 => 'application/vnd.ms-fontobject',
  ),
  'epub' => 
  array (
    0 => 'application/epub+zip',
  ),
  'es' => 
  array (
    0 => 'application/ecmascript',
  ),
  'es3' => 
  array (
    0 => 'application/vnd.eszigno3+xml',
  ),
  'esf' => 
  array (
    0 => 'application/vnd.epson.esf',
  ),
  'etx' => 
  array (
    0 => 'text/x-setext',
  ),
  'exe' => 
  array (
    0 => 'application/x-msdownload',
    1 => 'application/x-msdos-program',
    2 => 'application/octet-stream',
  ),
  'exi' => 
  array (
    0 => 'application/exi',
  ),
  'ext' => 
  array (
    0 => 'application/vnd.novadigm.ext',
  ),
  'ez2' => 
  array (
    0 => 'application/vnd.ezpix-album',
  ),
  'ez3' => 
  array (
    0 => 'application/vnd.ezpix-package',
  ),
  'f' => 
  array (
    0 => 'text/x-fortran',
  ),
  'f4v' => 
  array (
    0 => 'video/x-f4v',
  ),
  'fbs' => 
  array (
    0 => 'image/vnd.fastbidsheet',
  ),
  'fcs' => 
  array (
    0 => 'application/vnd.isac.fcs',
  ),
  'fdf' => 
  array (
    0 => 'application/vnd.fdf',
  ),
  'fe_launch' => 
  array (
    0 => 'application/vnd.denovo.fcselayout-link',
  ),
  'fg5' => 
  array (
    0 => 'application/vnd.fujitsu.oasysgp',
  ),
  'fh' => 
  array (
    0 => 'image/x-freehand',
  ),
  'fig' => 
  array (
    0 => 'application/x-xfig',
  ),
  'fli' => 
  array (
    0 => 'video/x-fli',
    1 => 'video/fli',
  ),
  'flo' => 
  array (
    0 => 'application/vnd.micrografx.flo',
  ),
  'flv' => 
  array (
    0 => 'video/x-flv',
  ),
  'flw' => 
  array (
    0 => 'application/vnd.kde.kivio',
  ),
  'flx' => 
  array (
    0 => 'text/vnd.fmi.flexstor',
  ),
  'fly' => 
  array (
    0 => 'text/vnd.fly',
  ),
  'fm' => 
  array (
    0 => 'application/vnd.framemaker',
    1 => 'application/x-maker',
  ),
  'fnc' => 
  array (
    0 => 'application/vnd.frogans.fnc',
  ),
  'fpx' => 
  array (
    0 => 'image/vnd.fpx',
  ),
  'fsc' => 
  array (
    0 => 'application/vnd.fsc.weblaunch',
  ),
  'fst' => 
  array (
    0 => 'image/vnd.fst',
  ),
  'ftc' => 
  array (
    0 => 'application/vnd.fluxtime.clip',
  ),
  'fti' => 
  array (
    0 => 'application/vnd.anser-web-funds-transfer-initiation',
  ),
  'fvt' => 
  array (
    0 => 'video/vnd.fvt',
  ),
  'fxp' => 
  array (
    0 => 'application/vnd.adobe.fxp',
  ),
  'fzs' => 
  array (
    0 => 'application/vnd.fuzzysheet',
  ),
  'g2w' => 
  array (
    0 => 'application/vnd.geoplan',
  ),
  'g3' => 
  array (
    0 => 'image/g3fax',
  ),
  'g3w' => 
  array (
    0 => 'application/vnd.geospace',
  ),
  'gac' => 
  array (
    0 => 'application/vnd.groove-account',
  ),
  'gdl' => 
  array (
    0 => 'model/vnd.gdl',
  ),
  'geo' => 
  array (
    0 => 'application/vnd.dynageo',
  ),
  'gex' => 
  array (
    0 => 'application/vnd.geometry-explorer',
  ),
  'ggb' => 
  array (
    0 => 'application/vnd.geogebra.file',
  ),
  'ggt' => 
  array (
    0 => 'application/vnd.geogebra.tool',
  ),
  'ghf' => 
  array (
    0 => 'application/vnd.groove-help',
  ),
  'gif' => 
  array (
    0 => 'image/gif',
  ),
  'gim' => 
  array (
    0 => 'application/vnd.groove-identity-message',
  ),
  'gmx' => 
  array (
    0 => 'application/vnd.gmx',
  ),
  'gnumeric' => 
  array (
    0 => 'application/x-gnumeric',
  ),
  'gph' => 
  array (
    0 => 'application/vnd.flographit',
  ),
  'gqf' => 
  array (
    0 => 'application/vnd.grafeq',
  ),
  'gram' => 
  array (
    0 => 'application/srgs',
  ),
  'grv' => 
  array (
    0 => 'application/vnd.groove-injector',
  ),
  'grxml' => 
  array (
    0 => 'application/srgs+xml',
  ),
  'gsf' => 
  array (
    0 => 'application/x-font-ghostscript',
    1 => 'application/x-font',
  ),
  'gtar' => 
  array (
    0 => 'application/x-gtar',
  ),
  'gtm' => 
  array (
    0 => 'application/vnd.groove-tool-message',
  ),
  'gtw' => 
  array (
    0 => 'model/vnd.gtw',
  ),
  'gv' => 
  array (
    0 => 'text/vnd.graphviz',
  ),
  'gxt' => 
  array (
    0 => 'application/vnd.geonext',
  ),
  'h261' => 
  array (
    0 => 'video/h261',
  ),
  'h263' => 
  array (
    0 => 'video/h263',
  ),
  'h264' => 
  array (
    0 => 'video/h264',
  ),
  'hal' => 
  array (
    0 => 'application/vnd.hal+xml',
  ),
  'hbci' => 
  array (
    0 => 'application/vnd.hbci',
  ),
  'hdf' => 
  array (
    0 => 'application/x-hdf',
  ),
  'hlp' => 
  array (
    0 => 'application/winhlp',
  ),
  'hpgl' => 
  array (
    0 => 'application/vnd.hp-hpgl',
  ),
  'hpid' => 
  array (
    0 => 'application/vnd.hp-hpid',
  ),
  'hps' => 
  array (
    0 => 'application/vnd.hp-hps',
  ),
  'hqx' => 
  array (
    0 => 'application/mac-binhex40',
  ),
  'htke' => 
  array (
    0 => 'application/vnd.kenameaapp',
  ),
  'html' => 
  array (
    0 => 'text/html',
  ),
  'hvd' => 
  array (
    0 => 'application/vnd.yamaha.hv-dic',
  ),
  'hvp' => 
  array (
    0 => 'application/vnd.yamaha.hv-voice',
  ),
  'hvs' => 
  array (
    0 => 'application/vnd.yamaha.hv-script',
  ),
  'i2g' => 
  array (
    0 => 'application/vnd.intergeo',
  ),
  'icc' => 
  array (
    0 => 'application/vnd.iccprofile',
  ),
  'ice' => 
  array (
    0 => 'x-conference/x-cooltalk',
  ),
  'ico' => 
  array (
    0 => 'image/x-icon',
    1 => 'image/vnd.microsoft.icon',
  ),
  'ics' => 
  array (
    0 => 'text/calendar',
  ),
  'ief' => 
  array (
    0 => 'image/ief',
  ),
  'ifm' => 
  array (
    0 => 'application/vnd.shana.informed.formdata',
  ),
  'igl' => 
  array (
    0 => 'application/vnd.igloader',
  ),
  'igm' => 
  array (
    0 => 'application/vnd.insors.igm',
  ),
  'igs' => 
  array (
    0 => 'model/iges',
  ),
  'igx' => 
  array (
    0 => 'application/vnd.micrografx.igx',
  ),
  'iif' => 
  array (
    0 => 'application/vnd.shana.informed.interchange',
  ),
  'imp' => 
  array (
    0 => 'application/vnd.accpac.simply.imp',
  ),
  'ims' => 
  array (
    0 => 'application/vnd.ms-ims',
  ),
  'ipfix' => 
  array (
    0 => 'application/ipfix',
  ),
  'ipk' => 
  array (
    0 => 'application/vnd.shana.informed.package',
  ),
  'irm' => 
  array (
    0 => 'application/vnd.ibm.rights-management',
  ),
  'irp' => 
  array (
    0 => 'application/vnd.irepository.package+xml',
  ),
  'itp' => 
  array (
    0 => 'application/vnd.shana.informed.formtemplate',
  ),
  'ivp' => 
  array (
    0 => 'application/vnd.immervision-ivp',
  ),
  'ivu' => 
  array (
    0 => 'application/vnd.immervision-ivu',
  ),
  'jad' => 
  array (
    0 => 'text/vnd.sun.j2me.app-descriptor',
  ),
  'jam' => 
  array (
    0 => 'application/vnd.jam',
    1 => 'application/x-jam',
  ),
  'jar' => 
  array (
    0 => 'application/java-archive',
  ),
  'java' => 
  array (
    0 => 'text/x-java-source,java',
    1 => 'text/x-java-source',
    2 => 'text/x-java',
  ),
  'jisp' => 
  array (
    0 => 'application/vnd.jisp',
  ),
  'jlt' => 
  array (
    0 => 'application/vnd.hp-jlyt',
  ),
  'jnlp' => 
  array (
    0 => 'application/x-java-jnlp-file',
  ),
  'joda' => 
  array (
    0 => 'application/vnd.joost.joda-archive',
  ),
  'jpeg' => 
  array (
    0 => 'image/jpeg',
  ),
  'jpgv' => 
  array (
    0 => 'video/jpeg',
  ),
  'jpm' => 
  array (
    0 => 'video/jpm',
    1 => 'image/jpm',
  ),
  'js' => 
  array (
    0 => 'application/javascript',
  ),
  'json' => 
  array (
    0 => 'application/json',
  ),
  'jsond' => 
  array (
    0 => 'application/json',
  ),
  'karbon' => 
  array (
    0 => 'application/vnd.kde.karbon',
  ),
  'kfo' => 
  array (
    0 => 'application/vnd.kde.kformula',
  ),
  'kia' => 
  array (
    0 => 'application/vnd.kidspiration',
  ),
  'kml' => 
  array (
    0 => 'application/vnd.google-earth.kml+xml',
  ),
  'kmz' => 
  array (
    0 => 'application/vnd.google-earth.kmz',
  ),
  'kne' => 
  array (
    0 => 'application/vnd.kinar',
  ),
  'kon' => 
  array (
    0 => 'application/vnd.kde.kontour',
  ),
  'kpr' => 
  array (
    0 => 'application/vnd.kde.kpresenter',
    1 => 'application/x-kpresenter',
  ),
  'ksp' => 
  array (
    0 => 'application/vnd.kde.kspread',
    1 => 'application/x-kspread',
  ),
  'ktx' => 
  array (
    0 => 'image/ktx',
  ),
  'ktz' => 
  array (
    0 => 'application/vnd.kahootz',
  ),
  'kwd' => 
  array (
    0 => 'application/vnd.kde.kword',
    1 => 'application/x-kword',
  ),
  'lasxml' => 
  array (
    0 => 'application/vnd.las.las+xml',
  ),
  'latex' => 
  array (
    0 => 'application/x-latex',
  ),
  'lbd' => 
  array (
    0 => 'application/vnd.llamagraphics.life-balance.desktop',
  ),
  'lbe' => 
  array (
    0 => 'application/vnd.llamagraphics.life-balance.exchange+xml',
  ),
  'les' => 
  array (
    0 => 'application/vnd.hhe.lesson-player',
  ),
  'link66' => 
  array (
    0 => 'application/vnd.route66.link66+xml',
  ),
  'lrm' => 
  array (
    0 => 'application/vnd.ms-lrm',
  ),
  'ltf' => 
  array (
    0 => 'application/vnd.frogans.ltf',
  ),
  'lvp' => 
  array (
    0 => 'audio/vnd.lucent.voice',
  ),
  'lwp' => 
  array (
    0 => 'application/vnd.lotus-wordpro',
  ),
  'm21' => 
  array (
    0 => 'application/mp21',
  ),
  'm3u' => 
  array (
    0 => 'audio/x-mpegurl',
  ),
  'm3u8' => 
  array (
    0 => 'application/vnd.apple.mpegurl',
  ),
  'm4v' => 
  array (
    0 => 'video/x-m4v',
  ),
  'ma' => 
  array (
    0 => 'application/mathematica',
  ),
  'mads' => 
  array (
    0 => 'application/mads+xml',
  ),
  'mag' => 
  array (
    0 => 'application/vnd.ecowin.chart',
  ),
  'mathml' => 
  array (
    0 => 'application/mathml+xml',
  ),
  'mbk' => 
  array (
    0 => 'application/vnd.mobius.mbk',
  ),
  'mbox' => 
  array (
    0 => 'application/mbox',
  ),
  'mc1' => 
  array (
    0 => 'application/vnd.medcalcdata',
  ),
  'mcd' => 
  array (
    0 => 'application/vnd.mcd',
  ),
  'mcurl' => 
  array (
    0 => 'text/vnd.curl.mcurl',
  ),
  'mdb' => 
  array (
    0 => 'application/x-msaccess',
    1 => 'application/msaccess',
  ),
  'mdi' => 
  array (
    0 => 'image/vnd.ms-modi',
  ),
  'meta4' => 
  array (
    0 => 'application/metalink4+xml',
  ),
  'mets' => 
  array (
    0 => 'application/mets+xml',
  ),
  'mfm' => 
  array (
    0 => 'application/vnd.mfmp',
  ),
  'mgp' => 
  array (
    0 => 'application/vnd.osgeo.mapguide.package',
  ),
  'mgz' => 
  array (
    0 => 'application/vnd.proteus.magazine',
  ),
  'mid' => 
  array (
    0 => 'audio/midi',
  ),
  'mif' => 
  array (
    0 => 'application/vnd.mif',
    1 => 'application/x-mif',
  ),
  'mj2' => 
  array (
    0 => 'video/mj2',
  ),
  'mlp' => 
  array (
    0 => 'application/vnd.dolby.mlp',
  ),
  'mmd' => 
  array (
    0 => 'application/vnd.chipnuts.karaoke-mmd',
    1 => 'chemical/x-macromodel-input',
  ),
  'mmf' => 
  array (
    0 => 'application/vnd.smaf',
  ),
  'mmr' => 
  array (
    0 => 'image/vnd.fujixerox.edmics-mmr',
  ),
  'mny' => 
  array (
    0 => 'application/x-msmoney',
  ),
  'mods' => 
  array (
    0 => 'application/mods+xml',
  ),
  'movie' => 
  array (
    0 => 'video/x-sgi-movie',
  ),
  'mp4' => 
  array (
    0 => 'video/mp4',
  ),
  'mp4a' => 
  array (
    0 => 'audio/mp4',
  ),
  'mpc' => 
  array (
    0 => 'application/vnd.mophun.certificate',
    1 => 'chemical/x-mopac-input',
  ),
  'mpeg' => 
  array (
    0 => 'video/mpeg',
  ),
  'mpga' => 
  array (
    0 => 'audio/mpeg',
  ),
  'mpkg' => 
  array (
    0 => 'application/vnd.apple.installer+xml',
  ),
  'mpm' => 
  array (
    0 => 'application/vnd.blueice.multipass',
  ),
  'mpn' => 
  array (
    0 => 'application/vnd.mophun.application',
  ),
  'mpp' => 
  array (
    0 => 'application/vnd.ms-project',
  ),
  'mpy' => 
  array (
    0 => 'application/vnd.ibm.minipay',
  ),
  'mqy' => 
  array (
    0 => 'application/vnd.mobius.mqy',
  ),
  'mrc' => 
  array (
    0 => 'application/marc',
  ),
  'mrcx' => 
  array (
    0 => 'application/marcxml+xml',
  ),
  'mscml' => 
  array (
    0 => 'application/mediaservercontrol+xml',
  ),
  'mseq' => 
  array (
    0 => 'application/vnd.mseq',
  ),
  'msf' => 
  array (
    0 => 'application/vnd.epson.msf',
  ),
  'msh' => 
  array (
    0 => 'model/mesh',
  ),
  'msl' => 
  array (
    0 => 'application/vnd.mobius.msl',
  ),
  'msty' => 
  array (
    0 => 'application/vnd.muvee.style',
  ),
  'mts' => 
  array (
    0 => 'model/vnd.mts',
  ),
  'mus' => 
  array (
    0 => 'application/vnd.musician',
  ),
  'musicxml' => 
  array (
    0 => 'application/vnd.recordare.musicxml+xml',
  ),
  'mvb' => 
  array (
    0 => 'application/x-msmediaview',
    1 => 'chemical/x-mopac-vib',
  ),
  'mwf' => 
  array (
    0 => 'application/vnd.mfer',
  ),
  'mxf' => 
  array (
    0 => 'application/mxf',
  ),
  'mxl' => 
  array (
    0 => 'application/vnd.recordare.musicxml',
  ),
  'mxml' => 
  array (
    0 => 'application/xv+xml',
  ),
  'mxs' => 
  array (
    0 => 'application/vnd.triscape.mxs',
  ),
  'mxu' => 
  array (
    0 => 'video/vnd.mpegurl',
  ),
  'n-gage' => 
  array (
    0 => 'application/vnd.nokia.n-gage.symbian.install',
  ),
  'n3' => 
  array (
    0 => 'text/n3',
  ),
  'nbp' => 
  array (
    0 => 'application/vnd.wolfram.player',
    1 => 'application/mathematica',
  ),
  'nc' => 
  array (
    0 => 'application/x-netcdf',
  ),
  'ncx' => 
  array (
    0 => 'application/x-dtbncx+xml',
  ),
  'ngdat' => 
  array (
    0 => 'application/vnd.nokia.n-gage.data',
  ),
  'nlu' => 
  array (
    0 => 'application/vnd.neurolanguage.nlu',
  ),
  'nml' => 
  array (
    0 => 'application/vnd.enliven',
  ),
  'nnd' => 
  array (
    0 => 'application/vnd.noblenet-directory',
  ),
  'nns' => 
  array (
    0 => 'application/vnd.noblenet-sealer',
  ),
  'nnw' => 
  array (
    0 => 'application/vnd.noblenet-web',
  ),
  'npx' => 
  array (
    0 => 'image/vnd.net-fpx',
  ),
  'nsf' => 
  array (
    0 => 'application/vnd.lotus-notes',
  ),
  'oa2' => 
  array (
    0 => 'application/vnd.fujitsu.oasys2',
  ),
  'oa3' => 
  array (
    0 => 'application/vnd.fujitsu.oasys3',
  ),
  'oas' => 
  array (
    0 => 'application/vnd.fujitsu.oasys',
  ),
  'obd' => 
  array (
    0 => 'application/x-msbinder',
  ),
  'oda' => 
  array (
    0 => 'application/oda',
  ),
  'odb' => 
  array (
    0 => 'application/vnd.oasis.opendocument.database',
  ),
  'odc' => 
  array (
    0 => 'application/vnd.oasis.opendocument.chart',
  ),
  'odf' => 
  array (
    0 => 'application/vnd.oasis.opendocument.formula',
  ),
  'odft' => 
  array (
    0 => 'application/vnd.oasis.opendocument.formula-template',
  ),
  'odg' => 
  array (
    0 => 'application/vnd.oasis.opendocument.graphics',
  ),
  'odi' => 
  array (
    0 => 'application/vnd.oasis.opendocument.image',
  ),
  'odm' => 
  array (
    0 => 'application/vnd.oasis.opendocument.text-master',
  ),
  'odp' => 
  array (
    0 => 'application/vnd.oasis.opendocument.presentation',
  ),
  'ods' => 
  array (
    0 => 'application/vnd.oasis.opendocument.spreadsheet',
  ),
  'odt' => 
  array (
    0 => 'application/vnd.oasis.opendocument.text',
  ),
  'oga' => 
  array (
    0 => 'audio/ogg',
  ),
  'ogv' => 
  array (
    0 => 'video/ogg',
  ),
  'ogx' => 
  array (
    0 => 'application/ogg',
  ),
  'onetoc' => 
  array (
    0 => 'application/onenote',
  ),
  'opf' => 
  array (
    0 => 'application/oebps-package+xml',
  ),
  'org' => 
  array (
    0 => 'application/vnd.lotus-organizer',
  ),
  'osf' => 
  array (
    0 => 'application/vnd.yamaha.openscoreformat',
  ),
  'osfpvg' => 
  array (
    0 => 'application/vnd.yamaha.openscoreformat.osfpvg+xml',
  ),
  'otc' => 
  array (
    0 => 'application/vnd.oasis.opendocument.chart-template',
  ),
  'otf' => 
  array (
    0 => 'application/x-font-otf',
    1 => 'application/font-sfnt',
  ),
  'otg' => 
  array (
    0 => 'application/vnd.oasis.opendocument.graphics-template',
  ),
  'oth' => 
  array (
    0 => 'application/vnd.oasis.opendocument.text-web',
  ),
  'oti' => 
  array (
    0 => 'application/vnd.oasis.opendocument.image-template',
  ),
  'otp' => 
  array (
    0 => 'application/vnd.oasis.opendocument.presentation-template',
  ),
  'ots' => 
  array (
    0 => 'application/vnd.oasis.opendocument.spreadsheet-template',
  ),
  'ott' => 
  array (
    0 => 'application/vnd.oasis.opendocument.text-template',
  ),
  'oxt' => 
  array (
    0 => 'application/vnd.openofficeorg.extension',
  ),
  'p' => 
  array (
    0 => 'text/x-pascal',
  ),
  'p10' => 
  array (
    0 => 'application/pkcs10',
  ),
  'p12' => 
  array (
    0 => 'application/x-pkcs12',
  ),
  'p7b' => 
  array (
    0 => 'application/x-pkcs7-certificates',
  ),
  'p7m' => 
  array (
    0 => 'application/pkcs7-mime',
  ),
  'p7r' => 
  array (
    0 => 'application/x-pkcs7-certreqresp',
  ),
  'p7s' => 
  array (
    0 => 'application/pkcs7-signature',
  ),
  'p8' => 
  array (
    0 => 'application/pkcs8',
  ),
  'par' => 
  array (
    0 => 'text/plain-bas',
  ),
  'paw' => 
  array (
    0 => 'application/vnd.pawaafile',
  ),
  'pbd' => 
  array (
    0 => 'application/vnd.powerbuilder6',
  ),
  'pbm' => 
  array (
    0 => 'image/x-portable-bitmap',
  ),
  'pcf' => 
  array (
    0 => 'application/x-font-pcf',
  ),
  'pcl' => 
  array (
    0 => 'application/vnd.hp-pcl',
  ),
  'pclxl' => 
  array (
    0 => 'application/vnd.hp-pclxl',
  ),
  'pcurl' => 
  array (
    0 => 'application/vnd.curl.pcurl',
  ),
  'pcx' => 
  array (
    0 => 'image/x-pcx',
    1 => 'image/pcx',
  ),
  'pdb' => 
  array (
    0 => 'application/vnd.palm',
    1 => 'chemical/x-pdb',
    2 => 'application/x-pilot',
  ),
  'pdf' => 
  array (
    0 => 'application/pdf',
  ),
  'pfa' => 
  array (
    0 => 'application/x-font-type1',
    1 => 'application/x-font',
  ),
  'pfr' => 
  array (
    0 => 'application/font-tdpfr',
  ),
  'pgm' => 
  array (
    0 => 'image/x-portable-graymap',
  ),
  'pgn' => 
  array (
    0 => 'application/x-chess-pgn',
  ),
  'pgp' => 
  array (
    0 => 'application/pgp-signature',
    1 => 'application/pgp-encrypted',
  ),
  'pic' => 
  array (
    0 => 'image/x-pict',
  ),
  'pki' => 
  array (
    0 => 'application/pkixcmp',
  ),
  'pkipath' => 
  array (
    0 => 'application/pkix-pkipath',
  ),
  'plb' => 
  array (
    0 => 'application/vnd.3gpp.pic-bw-large',
  ),
  'plc' => 
  array (
    0 => 'application/vnd.mobius.plc',
  ),
  'plf' => 
  array (
    0 => 'application/vnd.pocketlearn',
  ),
  'pls' => 
  array (
    0 => 'application/pls+xml',
    1 => 'audio/x-scpls',
  ),
  'pml' => 
  array (
    0 => 'application/vnd.ctc-posml',
  ),
  'png' => 
  array (
    0 => 'image/png',
  ),
  'pnm' => 
  array (
    0 => 'image/x-portable-anymap',
  ),
  'portpkg' => 
  array (
    0 => 'application/vnd.macports.portpkg',
  ),
  'potm' => 
  array (
    0 => 'application/vnd.ms-powerpoint.template.macroenabled.12',
  ),
  'potx' => 
  array (
    0 => 'application/vnd.openxmlformats-officedocument.presentationml.template',
  ),
  'ppam' => 
  array (
    0 => 'application/vnd.ms-powerpoint.addin.macroenabled.12',
  ),
  'ppd' => 
  array (
    0 => 'application/vnd.cups-ppd',
  ),
  'ppm' => 
  array (
    0 => 'image/x-portable-pixmap',
  ),
  'ppsm' => 
  array (
    0 => 'application/vnd.ms-powerpoint.slideshow.macroenabled.12',
  ),
  'ppsx' => 
  array (
    0 => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
  ),
  'ppt' => 
  array (
    0 => 'application/vnd.ms-powerpoint',
  ),
  'pptm' => 
  array (
    0 => 'application/vnd.ms-powerpoint.presentation.macroenabled.12',
  ),
  'pptx' => 
  array (
    0 => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  ),
  'prc' => 
  array (
    0 => 'application/x-mobipocket-ebook',
    1 => 'application/x-pilot',
  ),
  'pre' => 
  array (
    0 => 'application/vnd.lotus-freelance',
  ),
  'prf' => 
  array (
    0 => 'application/pics-rules',
  ),
  'psb' => 
  array (
    0 => 'application/vnd.3gpp.pic-bw-small',
  ),
  'psd' => 
  array (
    0 => 'image/vnd.adobe.photoshop',
    1 => 'image/x-photoshop',
  ),
  'psf' => 
  array (
    0 => 'application/x-font-linux-psf',
  ),
  'pskcxml' => 
  array (
    0 => 'application/pskc+xml',
  ),
  'ptid' => 
  array (
    0 => 'application/vnd.pvi.ptid1',
  ),
  'pub' => 
  array (
    0 => 'application/x-mspublisher',
  ),
  'pvb' => 
  array (
    0 => 'application/vnd.3gpp.pic-bw-var',
  ),
  'pwn' => 
  array (
    0 => 'application/vnd.3m.post-it-notes',
  ),
  'pya' => 
  array (
    0 => 'audio/vnd.ms-playready.media.pya',
  ),
  'pyv' => 
  array (
    0 => 'video/vnd.ms-playready.media.pyv',
  ),
  'qam' => 
  array (
    0 => 'application/vnd.epson.quickanime',
  ),
  'qbo' => 
  array (
    0 => 'application/vnd.intu.qbo',
  ),
  'qfx' => 
  array (
    0 => 'application/vnd.intu.qfx',
  ),
  'qps' => 
  array (
    0 => 'application/vnd.publishare-delta-tree',
  ),
  'qt' => 
  array (
    0 => 'video/quicktime',
  ),
  'qxd' => 
  array (
    0 => 'application/vnd.quark.quarkxpress',
  ),
  'ram' => 
  array (
    0 => 'audio/x-pn-realaudio',
  ),
  'rar' => 
  array (
    0 => 'application/x-rar-compressed',
    1 => 'application/rar',
  ),
  'ras' => 
  array (
    0 => 'image/x-cmu-raster',
  ),
  'rcprofile' => 
  array (
    0 => 'application/vnd.ipunplugged.rcprofile',
  ),
  'rdf' => 
  array (
    0 => 'application/rdf+xml',
  ),
  'rdz' => 
  array (
    0 => 'application/vnd.data-vision.rdz',
  ),
  'rep' => 
  array (
    0 => 'application/vnd.businessobjects',
  ),
  'res' => 
  array (
    0 => 'application/x-dtbresource+xml',
  ),
  'rgb' => 
  array (
    0 => 'image/x-rgb',
  ),
  'rif' => 
  array (
    0 => 'application/reginfo+xml',
  ),
  'rip' => 
  array (
    0 => 'audio/vnd.rip',
  ),
  'rl' => 
  array (
    0 => 'application/resource-lists+xml',
  ),
  'rlc' => 
  array (
    0 => 'image/vnd.fujixerox.edmics-rlc',
  ),
  'rld' => 
  array (
    0 => 'application/resource-lists-diff+xml',
  ),
  'rm' => 
  array (
    0 => 'application/vnd.rn-realmedia',
    1 => 'audio/x-pn-realaudio',
  ),
  'rmp' => 
  array (
    0 => 'audio/x-pn-realaudio-plugin',
  ),
  'rms' => 
  array (
    0 => 'application/vnd.jcp.javame.midlet-rms',
  ),
  'rnc' => 
  array (
    0 => 'application/relax-ng-compact-syntax',
  ),
  'rp9' => 
  array (
    0 => 'application/vnd.cloanto.rp9',
  ),
  'rpss' => 
  array (
    0 => 'application/vnd.nokia.radio-presets',
  ),
  'rpst' => 
  array (
    0 => 'application/vnd.nokia.radio-preset',
  ),
  'rq' => 
  array (
    0 => 'application/sparql-query',
  ),
  'rs' => 
  array (
    0 => 'application/rls-services+xml',
  ),
  'rsd' => 
  array (
    0 => 'application/rsd+xml',
  ),
  'rss' => 
  array (
    0 => 'application/rss+xml',
  ),
  'rtx' => 
  array (
    0 => 'text/richtext',
  ),
  's' => 
  array (
    0 => 'text/x-asm',
  ),
  'saf' => 
  array (
    0 => 'application/vnd.yamaha.smaf-audio',
  ),
  'sbml' => 
  array (
    0 => 'application/sbml+xml',
  ),
  'sc' => 
  array (
    0 => 'application/vnd.ibm.secure-container',
  ),
  'scd' => 
  array (
    0 => 'application/x-msschedule',
  ),
  'scm' => 
  array (
    0 => 'application/vnd.lotus-screencam',
  ),
  'scq' => 
  array (
    0 => 'application/scvp-cv-request',
  ),
  'scs' => 
  array (
    0 => 'application/scvp-cv-response',
  ),
  'scurl' => 
  array (
    0 => 'text/vnd.curl.scurl',
  ),
  'sda' => 
  array (
    0 => 'application/vnd.stardivision.draw',
  ),
  'sdc' => 
  array (
    0 => 'application/vnd.stardivision.calc',
  ),
  'sdd' => 
  array (
    0 => 'application/vnd.stardivision.impress',
  ),
  'sdkm' => 
  array (
    0 => 'application/vnd.solent.sdkm+xml',
  ),
  'sdp' => 
  array (
    0 => 'application/sdp',
  ),
  'sdw' => 
  array (
    0 => 'application/vnd.stardivision.writer',
  ),
  'see' => 
  array (
    0 => 'application/vnd.seemail',
  ),
  'seed' => 
  array (
    0 => 'application/vnd.fdsn.seed',
  ),
  'sema' => 
  array (
    0 => 'application/vnd.sema',
  ),
  'semd' => 
  array (
    0 => 'application/vnd.semd',
  ),
  'semf' => 
  array (
    0 => 'application/vnd.semf',
  ),
  'ser' => 
  array (
    0 => 'application/java-serialized-object',
  ),
  'setpay' => 
  array (
    0 => 'application/set-payment-initiation',
  ),
  'setreg' => 
  array (
    0 => 'application/set-registration-initiation',
  ),
  'sfd-hdstx' => 
  array (
    0 => 'application/vnd.hydrostatix.sof-data',
  ),
  'sfs' => 
  array (
    0 => 'application/vnd.spotfire.sfs',
  ),
  'sgl' => 
  array (
    0 => 'application/vnd.stardivision.writer-global',
  ),
  'sgml' => 
  array (
    0 => 'text/sgml',
  ),
  'sh' => 
  array (
    0 => 'application/x-sh',
    1 => 'text/x-sh',
  ),
  'shar' => 
  array (
    0 => 'application/x-shar',
  ),
  'shf' => 
  array (
    0 => 'application/shf+xml',
  ),
  'sis' => 
  array (
    0 => 'application/vnd.symbian.install',
  ),
  'sit' => 
  array (
    0 => 'application/x-stuffit',
  ),
  'sitx' => 
  array (
    0 => 'application/x-stuffitx',
    1 => 'application/x-stuffit',
  ),
  'skp' => 
  array (
    0 => 'application/vnd.koan',
    1 => 'application/x-koan',
  ),
  'sldm' => 
  array (
    0 => 'application/vnd.ms-powerpoint.slide.macroenabled.12',
  ),
  'sldx' => 
  array (
    0 => 'application/vnd.openxmlformats-officedocument.presentationml.slide',
  ),
  'slt' => 
  array (
    0 => 'application/vnd.epson.salt',
  ),
  'sm' => 
  array (
    0 => 'application/vnd.stepmania.stepchart',
  ),
  'smf' => 
  array (
    0 => 'application/vnd.stardivision.math',
  ),
  'smi' => 
  array (
    0 => 'application/smil+xml',
  ),
  'snf' => 
  array (
    0 => 'application/x-font-snf',
  ),
  'spf' => 
  array (
    0 => 'application/vnd.yamaha.smaf-phrase',
  ),
  'spl' => 
  array (
    0 => 'application/x-futuresplash',
    1 => 'application/futuresplash',
  ),
  'spot' => 
  array (
    0 => 'text/vnd.in3d.spot',
  ),
  'spp' => 
  array (
    0 => 'application/scvp-vp-response',
  ),
  'spq' => 
  array (
    0 => 'application/scvp-vp-request',
  ),
  'src' => 
  array (
    0 => 'application/x-wais-source',
  ),
  'sru' => 
  array (
    0 => 'application/sru+xml',
  ),
  'srx' => 
  array (
    0 => 'application/sparql-results+xml',
  ),
  'sse' => 
  array (
    0 => 'application/vnd.kodak-descriptor',
  ),
  'ssf' => 
  array (
    0 => 'application/vnd.epson.ssf',
  ),
  'ssml' => 
  array (
    0 => 'application/ssml+xml',
  ),
  'st' => 
  array (
    0 => 'application/vnd.sailingtracker.track',
  ),
  'stc' => 
  array (
    0 => 'application/vnd.sun.xml.calc.template',
  ),
  'std' => 
  array (
    0 => 'application/vnd.sun.xml.draw.template',
  ),
  'stf' => 
  array (
    0 => 'application/vnd.wt.stf',
  ),
  'sti' => 
  array (
    0 => 'application/vnd.sun.xml.impress.template',
  ),
  'stk' => 
  array (
    0 => 'application/hyperstudio',
  ),
  'stl' => 
  array (
    0 => 'application/vnd.ms-pki.stl',
    1 => 'application/sla',
  ),
  'str' => 
  array (
    0 => 'application/vnd.pg.format',
  ),
  'stw' => 
  array (
    0 => 'application/vnd.sun.xml.writer.template',
  ),
  'sub' => 
  array (
    0 => 'image/vnd.dvb.subtitle',
    1 => 'text/vnd.dvb.subtitle',
  ),
  'sus' => 
  array (
    0 => 'application/vnd.sus-calendar',
  ),
  'sv4cpio' => 
  array (
    0 => 'application/x-sv4cpio',
  ),
  'sv4crc' => 
  array (
    0 => 'application/x-sv4crc',
  ),
  'svc' => 
  array (
    0 => 'application/vnd.dvb.service',
  ),
  'svd' => 
  array (
    0 => 'application/vnd.svd',
  ),
  'svg' => 
  array (
    0 => 'image/svg+xml',
  ),
  'swf' => 
  array (
    0 => 'application/x-shockwave-flash',
  ),
  'swi' => 
  array (
    0 => 'application/vnd.aristanetworks.swi',
  ),
  'sxc' => 
  array (
    0 => 'application/vnd.sun.xml.calc',
  ),
  'sxd' => 
  array (
    0 => 'application/vnd.sun.xml.draw',
  ),
  'sxg' => 
  array (
    0 => 'application/vnd.sun.xml.writer.global',
  ),
  'sxi' => 
  array (
    0 => 'application/vnd.sun.xml.impress',
  ),
  'sxm' => 
  array (
    0 => 'application/vnd.sun.xml.math',
  ),
  'sxw' => 
  array (
    0 => 'application/vnd.sun.xml.writer',
  ),
  't' => 
  array (
    0 => 'text/troff',
    1 => 'application/x-troff',
  ),
  'tao' => 
  array (
    0 => 'application/vnd.tao.intent-module-archive',
  ),
  'tar' => 
  array (
    0 => 'application/x-tar',
  ),
  'tcap' => 
  array (
    0 => 'application/vnd.3gpp2.tcap',
  ),
  'tcl' => 
  array (
    0 => 'application/x-tcl',
    1 => 'text/x-tcl',
  ),
  'teacher' => 
  array (
    0 => 'application/vnd.smart.teacher',
  ),
  'tei' => 
  array (
    0 => 'application/tei+xml',
  ),
  'tex' => 
  array (
    0 => 'application/x-tex',
    1 => 'text/x-tex',
  ),
  'texinfo' => 
  array (
    0 => 'application/x-texinfo',
  ),
  'tfi' => 
  array (
    0 => 'application/thraud+xml',
  ),
  'tfm' => 
  array (
    0 => 'application/x-tex-tfm',
  ),
  'thmx' => 
  array (
    0 => 'application/vnd.ms-officetheme',
  ),
  'tiff' => 
  array (
    0 => 'image/tiff',
  ),
  'tmo' => 
  array (
    0 => 'application/vnd.tmobile-livetv',
  ),
  'torrent' => 
  array (
    0 => 'application/x-bittorrent',
  ),
  'tpl' => 
  array (
    0 => 'application/vnd.groove-tool-template',
  ),
  'tpt' => 
  array (
    0 => 'application/vnd.trid.tpt',
  ),
  'tra' => 
  array (
    0 => 'application/vnd.trueapp',
  ),
  'trm' => 
  array (
    0 => 'application/x-msterminal',
  ),
  'tsd' => 
  array (
    0 => 'application/timestamped-data',
  ),
  'tsv' => 
  array (
    0 => 'text/tab-separated-values',
  ),
  'ttf' => 
  array (
    0 => 'application/x-font-ttf',
    1 => 'application/font-sfnt',
  ),
  'ttl' => 
  array (
    0 => 'text/turtle',
  ),
  'twd' => 
  array (
    0 => 'application/vnd.simtech-mindmapper',
  ),
  'txd' => 
  array (
    0 => 'application/vnd.genomatix.tuxedo',
  ),
  'txf' => 
  array (
    0 => 'application/vnd.mobius.txf',
  ),
  'txt' => 
  array (
    0 => 'text/plain',
  ),
  'ufd' => 
  array (
    0 => 'application/vnd.ufdl',
  ),
  'umj' => 
  array (
    0 => 'application/vnd.umajin',
  ),
  'unityweb' => 
  array (
    0 => 'application/vnd.unity',
  ),
  'uoml' => 
  array (
    0 => 'application/vnd.uoml+xml',
  ),
  'uri' => 
  array (
    0 => 'text/uri-list',
  ),
  'ustar' => 
  array (
    0 => 'application/x-ustar',
  ),
  'utz' => 
  array (
    0 => 'application/vnd.uiq.theme',
  ),
  'uu' => 
  array (
    0 => 'text/x-uuencode',
  ),
  'uva' => 
  array (
    0 => 'audio/vnd.dece.audio',
  ),
  'uvh' => 
  array (
    0 => 'video/vnd.dece.hd',
  ),
  'uvi' => 
  array (
    0 => 'image/vnd.dece.graphic',
  ),
  'uvm' => 
  array (
    0 => 'video/vnd.dece.mobile',
  ),
  'uvp' => 
  array (
    0 => 'video/vnd.dece.pd',
  ),
  'uvs' => 
  array (
    0 => 'video/vnd.dece.sd',
  ),
  'uvu' => 
  array (
    0 => 'video/vnd.uvvu.mp4',
  ),
  'uvv' => 
  array (
    0 => 'video/vnd.dece.video',
  ),
  'vcd' => 
  array (
    0 => 'application/x-cdlink',
  ),
  'vcf' => 
  array (
    0 => 'text/x-vcard',
    1 => 'text/vcard',
  ),
  'vcg' => 
  array (
    0 => 'application/vnd.groove-vcard',
  ),
  'vcs' => 
  array (
    0 => 'text/x-vcalendar',
  ),
  'vcx' => 
  array (
    0 => 'application/vnd.vcx',
  ),
  'vis' => 
  array (
    0 => 'application/vnd.visionary',
  ),
  'viv' => 
  array (
    0 => 'video/vnd.vivo',
  ),
  'vsd' => 
  array (
    0 => 'application/vnd.visio',
  ),
  'vsf' => 
  array (
    0 => 'application/vnd.vsf',
  ),
  'vtu' => 
  array (
    0 => 'model/vnd.vtu',
  ),
  'vxml' => 
  array (
    0 => 'application/voicexml+xml',
  ),
  'wad' => 
  array (
    0 => 'application/x-doom',
  ),
  'wav' => 
  array (
    0 => 'audio/x-wav',
  ),
  'wax' => 
  array (
    0 => 'audio/x-ms-wax',
  ),
  'wbmp' => 
  array (
    0 => 'image/vnd.wap.wbmp',
  ),
  'wbs' => 
  array (
    0 => 'application/vnd.criticaltools.wbs+xml',
  ),
  'wbxml' => 
  array (
    0 => 'application/vnd.wap.wbxml',
  ),
  'weba' => 
  array (
    0 => 'audio/webm',
  ),
  'webm' => 
  array (
    0 => 'video/webm',
  ),
  'webp' => 
  array (
    0 => 'image/webp',
  ),
  'wg' => 
  array (
    0 => 'application/vnd.pmi.widget',
  ),
  'wgt' => 
  array (
    0 => 'application/widget',
  ),
  'wm' => 
  array (
    0 => 'video/x-ms-wm',
  ),
  'wma' => 
  array (
    0 => 'audio/x-ms-wma',
  ),
  'wmd' => 
  array (
    0 => 'application/x-ms-wmd',
  ),
  'wmf' => 
  array (
    0 => 'application/x-msmetafile',
  ),
  'wml' => 
  array (
    0 => 'text/vnd.wap.wml',
  ),
  'wmlc' => 
  array (
    0 => 'application/vnd.wap.wmlc',
  ),
  'wmls' => 
  array (
    0 => 'text/vnd.wap.wmlscript',
  ),
  'wmlsc' => 
  array (
    0 => 'application/vnd.wap.wmlscriptc',
  ),
  'wmv' => 
  array (
    0 => 'video/x-ms-wmv',
  ),
  'wmx' => 
  array (
    0 => 'video/x-ms-wmx',
  ),
  'wmz' => 
  array (
    0 => 'application/x-ms-wmz',
    1 => 'application/x-msmetafile',
  ),
  'woff' => 
  array (
    0 => 'application/x-font-woff',
    1 => 'application/font-woff',
  ),
  'woff2' => 
  array (
    0 => 'application/x-font-woff2',
  ),
  'wpd' => 
  array (
    0 => 'application/vnd.wordperfect',
  ),
  'wpl' => 
  array (
    0 => 'application/vnd.ms-wpl',
  ),
  'wps' => 
  array (
    0 => 'application/vnd.ms-works',
  ),
  'wqd' => 
  array (
    0 => 'application/vnd.wqd',
  ),
  'wri' => 
  array (
    0 => 'application/x-mswrite',
  ),
  'wrl' => 
  array (
    0 => 'model/vrml',
  ),
  'wsdl' => 
  array (
    0 => 'application/wsdl+xml',
  ),
  'wspolicy' => 
  array (
    0 => 'application/wspolicy+xml',
  ),
  'wtb' => 
  array (
    0 => 'application/vnd.webturbo',
  ),
  'wvx' => 
  array (
    0 => 'video/x-ms-wvx',
  ),
  'x3d' => 
  array (
    0 => 'application/vnd.hzn-3d-crossword',
    1 => 'model/x3d+xml',
  ),
  'xap' => 
  array (
    0 => 'application/x-silverlight-app',
  ),
  'xar' => 
  array (
    0 => 'application/vnd.xara',
  ),
  'xbap' => 
  array (
    0 => 'application/x-ms-xbap',
  ),
  'xbd' => 
  array (
    0 => 'application/vnd.fujixerox.docuworks.binder',
  ),
  'xbm' => 
  array (
    0 => 'image/x-xbitmap',
  ),
  'xdf' => 
  array (
    0 => 'application/xcap-diff+xml',
  ),
  'xdm' => 
  array (
    0 => 'application/vnd.syncml.dm+xml',
  ),
  'xdp' => 
  array (
    0 => 'application/vnd.adobe.xdp+xml',
  ),
  'xdssc' => 
  array (
    0 => 'application/dssc+xml',
  ),
  'xdw' => 
  array (
    0 => 'application/vnd.fujixerox.docuworks',
  ),
  'xenc' => 
  array (
    0 => 'application/xenc+xml',
  ),
  'xer' => 
  array (
    0 => 'application/patch-ops-error+xml',
  ),
  'xfdf' => 
  array (
    0 => 'application/vnd.adobe.xfdf',
  ),
  'xfdl' => 
  array (
    0 => 'application/vnd.xfdl',
  ),
  'xhtml' => 
  array (
    0 => 'application/xhtml+xml',
  ),
  'xif' => 
  array (
    0 => 'image/vnd.xiff',
  ),
  'xlam' => 
  array (
    0 => 'application/vnd.ms-excel.addin.macroenabled.12',
  ),
  'xls' => 
  array (
    0 => 'application/vnd.ms-excel',
  ),
  'xlsb' => 
  array (
    0 => 'application/vnd.ms-excel.sheet.binary.macroenabled.12',
  ),
  'xlsm' => 
  array (
    0 => 'application/vnd.ms-excel.sheet.macroenabled.12',
  ),
  'xlsx' => 
  array (
    0 => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  ),
  'xltm' => 
  array (
    0 => 'application/vnd.ms-excel.template.macroenabled.12',
  ),
  'xltx' => 
  array (
    0 => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
  ),
  'xml' => 
  array (
    0 => 'application/xml',
    1 => 'text/xml',
  ),
  'xo' => 
  array (
    0 => 'application/vnd.olpc-sugar',
  ),
  'xop' => 
  array (
    0 => 'application/xop+xml',
  ),
  'xpi' => 
  array (
    0 => 'application/x-xpinstall',
  ),
  'xpm' => 
  array (
    0 => 'image/x-xpixmap',
  ),
  'xpr' => 
  array (
    0 => 'application/vnd.is-xpr',
  ),
  'xps' => 
  array (
    0 => 'application/vnd.ms-xpsdocument',
  ),
  'xpw' => 
  array (
    0 => 'application/vnd.intercon.formnet',
  ),
  'xslt' => 
  array (
    0 => 'application/xslt+xml',
  ),
  'xsm' => 
  array (
    0 => 'application/vnd.syncml+xml',
  ),
  'xspf' => 
  array (
    0 => 'application/xspf+xml',
  ),
  'xul' => 
  array (
    0 => 'application/vnd.mozilla.xul+xml',
  ),
  'xwd' => 
  array (
    0 => 'image/x-xwindowdump',
  ),
  'xyz' => 
  array (
    0 => 'chemical/x-xyz',
  ),
  'yaml' => 
  array (
    0 => 'text/yaml',
  ),
  'yang' => 
  array (
    0 => 'application/yang',
  ),
  'yin' => 
  array (
    0 => 'application/yin+xml',
  ),
  'zaz' => 
  array (
    0 => 'application/vnd.zzazz.deck+xml',
  ),
  'zip' => 
  array (
    0 => 'application/zip',
  ),
  'zir' => 
  array (
    0 => 'application/vnd.zul',
  ),
  'ez' => 
  array (
    0 => 'application/andrew-inset',
  ),
  'dbk' => 
  array (
    0 => 'application/docbook+xml',
  ),
  'ecma' => 
  array (
    0 => 'application/ecmascript',
  ),
  'gml' => 
  array (
    0 => 'application/gml+xml',
  ),
  'gpx' => 
  array (
    0 => 'application/gpx+xml',
  ),
  'gxf' => 
  array (
    0 => 'application/gxf',
  ),
  'ink' => 
  array (
    0 => 'application/inkml+xml',
  ),
  'inkml' => 
  array (
    0 => 'application/inkml+xml',
  ),
  'jsonml' => 
  array (
    0 => 'application/jsonml+json',
  ),
  'lostxml' => 
  array (
    0 => 'application/lost+xml',
  ),
  'nb' => 
  array (
    0 => 'application/mathematica',
  ),
  'mb' => 
  array (
    0 => 'application/mathematica',
  ),
  'metalink' => 
  array (
    0 => 'application/metalink+xml',
  ),
  'mp21' => 
  array (
    0 => 'application/mp21',
  ),
  'mp4s' => 
  array (
    0 => 'application/mp4',
  ),
  'dot' => 
  array (
    0 => 'application/msword',
  ),
  'dms' => 
  array (
    0 => 'application/octet-stream',
    1 => 'application/x-dms',
  ),
  'lrf' => 
  array (
    0 => 'application/octet-stream',
  ),
  'mar' => 
  array (
    0 => 'application/octet-stream',
  ),
  'so' => 
  array (
    0 => 'application/octet-stream',
  ),
  'dist' => 
  array (
    0 => 'application/octet-stream',
  ),
  'distz' => 
  array (
    0 => 'application/octet-stream',
  ),
  'pkg' => 
  array (
    0 => 'application/octet-stream',
  ),
  'bpk' => 
  array (
    0 => 'application/octet-stream',
  ),
  'dump' => 
  array (
    0 => 'application/octet-stream',
  ),
  'elc' => 
  array (
    0 => 'application/octet-stream',
  ),
  'deploy' => 
  array (
    0 => 'application/octet-stream',
  ),
  'omdoc' => 
  array (
    0 => 'application/omdoc+xml',
  ),
  'onetoc2' => 
  array (
    0 => 'application/onenote',
  ),
  'onetmp' => 
  array (
    0 => 'application/onenote',
  ),
  'onepkg' => 
  array (
    0 => 'application/onenote',
  ),
  'oxps' => 
  array (
    0 => 'application/oxps',
  ),
  'asc' => 
  array (
    0 => 'application/pgp-signature',
    1 => 'text/plain',
  ),
  'sig' => 
  array (
    0 => 'application/pgp-signature',
  ),
  'p7c' => 
  array (
    0 => 'application/pkcs7-mime',
  ),
  'eps' => 
  array (
    0 => 'application/postscript',
  ),
  'ps' => 
  array (
    0 => 'application/postscript',
  ),
  'gbr' => 
  array (
    0 => 'application/rpki-ghostbusters',
  ),
  'mft' => 
  array (
    0 => 'application/rpki-manifest',
  ),
  'roa' => 
  array (
    0 => 'application/rpki-roa',
  ),
  'smil' => 
  array (
    0 => 'application/smil+xml',
  ),
  'ssdl' => 
  array (
    0 => 'application/ssdl+xml',
  ),
  'teicorpus' => 
  array (
    0 => 'application/tei+xml',
  ),
  'acutc' => 
  array (
    0 => 'application/vnd.acucorp',
  ),
  'fcdt' => 
  array (
    0 => 'application/vnd.adobe.formscentral.fcdt',
  ),
  'fxpl' => 
  array (
    0 => 'application/vnd.adobe.fxp',
  ),
  'iota' => 
  array (
    0 => 'application/vnd.astraea-software.iota',
  ),
  'c4d' => 
  array (
    0 => 'application/vnd.clonk.c4group',
  ),
  'c4f' => 
  array (
    0 => 'application/vnd.clonk.c4group',
  ),
  'c4p' => 
  array (
    0 => 'application/vnd.clonk.c4group',
  ),
  'c4u' => 
  array (
    0 => 'application/vnd.clonk.c4group',
  ),
  'dart' => 
  array (
    0 => 'application/vnd.dart',
  ),
  'uvf' => 
  array (
    0 => 'application/vnd.dece.data',
  ),
  'uvvf' => 
  array (
    0 => 'application/vnd.dece.data',
  ),
  'uvd' => 
  array (
    0 => 'application/vnd.dece.data',
  ),
  'uvvd' => 
  array (
    0 => 'application/vnd.dece.data',
  ),
  'uvt' => 
  array (
    0 => 'application/vnd.dece.ttml+xml',
  ),
  'uvvt' => 
  array (
    0 => 'application/vnd.dece.ttml+xml',
  ),
  'uvx' => 
  array (
    0 => 'application/vnd.dece.unspecified',
  ),
  'uvvx' => 
  array (
    0 => 'application/vnd.dece.unspecified',
  ),
  'uvz' => 
  array (
    0 => 'application/vnd.dece.zip',
  ),
  'uvvz' => 
  array (
    0 => 'application/vnd.dece.zip',
  ),
  'kpxx' => 
  array (
    0 => 'application/vnd.ds-keypoint',
  ),
  'et3' => 
  array (
    0 => 'application/vnd.eszigno3+xml',
  ),
  'mseed' => 
  array (
    0 => 'application/vnd.fdsn.mseed',
  ),
  'dataless' => 
  array (
    0 => 'application/vnd.fdsn.seed',
  ),
  'frame' => 
  array (
    0 => 'application/vnd.framemaker',
    1 => 'application/x-maker',
  ),
  'maker' => 
  array (
    0 => 'application/vnd.framemaker',
    1 => 'application/x-maker',
  ),
  'book' => 
  array (
    0 => 'application/vnd.framemaker',
    1 => 'application/x-maker',
  ),
  'gre' => 
  array (
    0 => 'application/vnd.geometry-explorer',
  ),
  'gqs' => 
  array (
    0 => 'application/vnd.grafeq',
  ),
  'zmm' => 
  array (
    0 => 'application/vnd.handheld-entertainment+xml',
  ),
  'listafp' => 
  array (
    0 => 'application/vnd.ibm.modcap',
  ),
  'list3820' => 
  array (
    0 => 'application/vnd.ibm.modcap',
  ),
  'icm' => 
  array (
    0 => 'application/vnd.iccprofile',
  ),
  'xpx' => 
  array (
    0 => 'application/vnd.intercon.formnet',
  ),
  'ktr' => 
  array (
    0 => 'application/vnd.kahootz',
  ),
  'kpt' => 
  array (
    0 => 'application/vnd.kde.kpresenter',
    1 => 'application/x-kpresenter',
  ),
  'kwt' => 
  array (
    0 => 'application/vnd.kde.kword',
    1 => 'application/x-kword',
  ),
  'knp' => 
  array (
    0 => 'application/vnd.kinar',
  ),
  'skd' => 
  array (
    0 => 'application/vnd.koan',
    1 => 'application/x-koan',
  ),
  'skt' => 
  array (
    0 => 'application/vnd.koan',
    1 => 'application/x-koan',
  ),
  'skm' => 
  array (
    0 => 'application/vnd.koan',
    1 => 'application/x-koan',
  ),
  'xlm' => 
  array (
    0 => 'application/vnd.ms-excel',
  ),
  'xla' => 
  array (
    0 => 'application/vnd.ms-excel',
  ),
  'xlc' => 
  array (
    0 => 'application/vnd.ms-excel',
  ),
  'xlt' => 
  array (
    0 => 'application/vnd.ms-excel',
  ),
  'xlw' => 
  array (
    0 => 'application/vnd.ms-excel',
  ),
  'pps' => 
  array (
    0 => 'application/vnd.ms-powerpoint',
  ),
  'pot' => 
  array (
    0 => 'application/vnd.ms-powerpoint',
    1 => 'text/plain',
  ),
  'mpt' => 
  array (
    0 => 'application/vnd.ms-project',
  ),
  'wks' => 
  array (
    0 => 'application/vnd.ms-works',
  ),
  'wcm' => 
  array (
    0 => 'application/vnd.ms-works',
  ),
  'wdb' => 
  array (
    0 => 'application/vnd.ms-works',
  ),
  'taglet' => 
  array (
    0 => 'application/vnd.mynfc',
  ),
  'ntf' => 
  array (
    0 => 'application/vnd.nitf',
  ),
  'nitf' => 
  array (
    0 => 'application/vnd.nitf',
  ),
  'esa' => 
  array (
    0 => 'application/vnd.osgi.subsystem',
  ),
  'pqa' => 
  array (
    0 => 'application/vnd.palm',
  ),
  'oprc' => 
  array (
    0 => 'application/vnd.palm',
  ),
  'qxt' => 
  array (
    0 => 'application/vnd.quark.quarkxpress',
  ),
  'qwd' => 
  array (
    0 => 'application/vnd.quark.quarkxpress',
  ),
  'qwt' => 
  array (
    0 => 'application/vnd.quark.quarkxpress',
  ),
  'qxl' => 
  array (
    0 => 'application/vnd.quark.quarkxpress',
  ),
  'qxb' => 
  array (
    0 => 'application/vnd.quark.quarkxpress',
  ),
  'rmvb' => 
  array (
    0 => 'application/vnd.rn-realmedia-vbr',
  ),
  'twds' => 
  array (
    0 => 'application/vnd.simtech-mindmapper',
  ),
  'sdkd' => 
  array (
    0 => 'application/vnd.solent.sdkm+xml',
  ),
  'vor' => 
  array (
    0 => 'application/vnd.stardivision.writer',
  ),
  'smzip' => 
  array (
    0 => 'application/vnd.stepmania.package',
  ),
  'susp' => 
  array (
    0 => 'application/vnd.sus-calendar',
  ),
  'sisx' => 
  array (
    0 => 'application/vnd.symbian.install',
  ),
  'pcap' => 
  array (
    0 => 'application/vnd.tcpdump.pcap',
  ),
  'cap' => 
  array (
    0 => 'application/vnd.tcpdump.pcap',
  ),
  'dmp' => 
  array (
    0 => 'application/vnd.tcpdump.pcap',
  ),
  'ufdl' => 
  array (
    0 => 'application/vnd.ufdl',
  ),
  'vst' => 
  array (
    0 => 'application/vnd.visio',
  ),
  'vss' => 
  array (
    0 => 'application/vnd.visio',
  ),
  'vsw' => 
  array (
    0 => 'application/vnd.visio',
  ),
  'zirz' => 
  array (
    0 => 'application/vnd.zul',
  ),
  'dmg' => 
  array (
    0 => 'application/x-apple-diskimage',
    1 => 'application/octet-stream',
  ),
  'x32' => 
  array (
    0 => 'application/x-authorware-bin',
  ),
  'u32' => 
  array (
    0 => 'application/x-authorware-bin',
  ),
  'vox' => 
  array (
    0 => 'application/x-authorware-bin',
  ),
  'blb' => 
  array (
    0 => 'application/x-blorb',
  ),
  'blorb' => 
  array (
    0 => 'application/x-blorb',
  ),
  'boz' => 
  array (
    0 => 'application/x-bzip2',
  ),
  'cbr' => 
  array (
    0 => 'application/x-cbr',
  ),
  'cba' => 
  array (
    0 => 'application/x-cbr',
  ),
  'cbt' => 
  array (
    0 => 'application/x-cbr',
  ),
  'cbz' => 
  array (
    0 => 'application/x-cbr',
    1 => 'application/x-cbz',
  ),
  'cb7' => 
  array (
    0 => 'application/x-cbr',
  ),
  'cfs' => 
  array (
    0 => 'application/x-cfs-compressed',
  ),
  'nsc' => 
  array (
    0 => 'application/x-conference',
  ),
  'udeb' => 
  array (
    0 => 'application/x-debian-package',
    1 => 'application/vnd.debian.binary-package',
  ),
  'dgc' => 
  array (
    0 => 'application/x-dgc-compressed',
  ),
  'dcr' => 
  array (
    0 => 'application/x-director',
  ),
  'dxr' => 
  array (
    0 => 'application/x-director',
  ),
  'cst' => 
  array (
    0 => 'application/x-director',
  ),
  'cct' => 
  array (
    0 => 'application/x-director',
  ),
  'cxt' => 
  array (
    0 => 'application/x-director',
  ),
  'w3d' => 
  array (
    0 => 'application/x-director',
  ),
  'fgd' => 
  array (
    0 => 'application/x-director',
  ),
  'swa' => 
  array (
    0 => 'application/x-director',
  ),
  'evy' => 
  array (
    0 => 'application/x-envoy',
  ),
  'eva' => 
  array (
    0 => 'application/x-eva',
  ),
  'ttc' => 
  array (
    0 => 'application/x-font-ttf',
  ),
  'pfb' => 
  array (
    0 => 'application/x-font-type1',
    1 => 'application/x-font',
  ),
  'pfm' => 
  array (
    0 => 'application/x-font-type1',
  ),
  'afm' => 
  array (
    0 => 'application/x-font-type1',
  ),
  'arc' => 
  array (
    0 => 'application/x-freearc',
  ),
  'gca' => 
  array (
    0 => 'application/x-gca-compressed',
  ),
  'ulx' => 
  array (
    0 => 'application/x-glulx',
  ),
  'gramps' => 
  array (
    0 => 'application/x-gramps-xml',
  ),
  'install' => 
  array (
    0 => 'application/x-install-instructions',
  ),
  'iso' => 
  array (
    0 => 'application/x-iso9660-image',
    1 => 'application/octet-stream',
  ),
  'lzh' => 
  array (
    0 => 'application/x-lzh-compressed',
    1 => 'application/x-lzh',
  ),
  'lha' => 
  array (
    0 => 'application/x-lzh-compressed',
    1 => 'application/x-lha',
  ),
  'mie' => 
  array (
    0 => 'application/x-mie',
  ),
  'mobi' => 
  array (
    0 => 'application/x-mobipocket-ebook',
  ),
  'lnk' => 
  array (
    0 => 'application/x-ms-shortcut',
  ),
  'dll' => 
  array (
    0 => 'application/x-msdownload',
    1 => 'application/x-msdos-program',
    2 => 'application/octet-stream',
  ),
  'com' => 
  array (
    0 => 'application/x-msdownload',
    1 => 'application/x-msdos-program',
  ),
  'bat' => 
  array (
    0 => 'application/x-msdownload',
    1 => 'application/x-msdos-program',
  ),
  'msi' => 
  array (
    0 => 'application/x-msdownload',
    1 => 'application/x-msi',
    2 => 'application/octet-stream',
  ),
  'm13' => 
  array (
    0 => 'application/x-msmediaview',
  ),
  'm14' => 
  array (
    0 => 'application/x-msmediaview',
  ),
  'emf' => 
  array (
    0 => 'application/x-msmetafile',
  ),
  'emz' => 
  array (
    0 => 'application/x-msmetafile',
  ),
  'cdf' => 
  array (
    0 => 'application/x-netcdf',
    1 => 'application/x-cdf',
  ),
  'nzb' => 
  array (
    0 => 'application/x-nzb',
  ),
  'pfx' => 
  array (
    0 => 'application/x-pkcs12',
  ),
  'spc' => 
  array (
    0 => 'application/x-pkcs7-certificates',
    1 => 'chemical/x-galactic-spc',
  ),
  'ris' => 
  array (
    0 => 'application/x-research-info-systems',
  ),
  'sql' => 
  array (
    0 => 'application/x-sql',
  ),
  'srt' => 
  array (
    0 => 'application/x-subrip',
    1 => 'text/plain',
  ),
  't3' => 
  array (
    0 => 'application/x-t3vm-image',
  ),
  'gam' => 
  array (
    0 => 'application/x-tads',
    1 => 'chemical/x-gamess-input',
  ),
  'texi' => 
  array (
    0 => 'application/x-texinfo',
  ),
  'obj' => 
  array (
    0 => 'application/x-tgif',
  ),
  'crt' => 
  array (
    0 => 'application/x-x509-ca-cert',
  ),
  'xlf' => 
  array (
    0 => 'application/x-xliff+xml',
  ),
  'xz' => 
  array (
    0 => 'application/x-xz',
  ),
  'z1' => 
  array (
    0 => 'application/x-zmachine',
  ),
  'z2' => 
  array (
    0 => 'application/x-zmachine',
  ),
  'z3' => 
  array (
    0 => 'application/x-zmachine',
  ),
  'z4' => 
  array (
    0 => 'application/x-zmachine',
  ),
  'z5' => 
  array (
    0 => 'application/x-zmachine',
  ),
  'z6' => 
  array (
    0 => 'application/x-zmachine',
  ),
  'z7' => 
  array (
    0 => 'application/x-zmachine',
  ),
  'z8' => 
  array (
    0 => 'application/x-zmachine',
  ),
  'xaml' => 
  array (
    0 => 'application/xaml+xml',
  ),
  'xht' => 
  array (
    0 => 'application/xhtml+xml',
  ),
  'xsl' => 
  array (
    0 => 'application/xml',
  ),
  'xpl' => 
  array (
    0 => 'application/xproc+xml',
  ),
  'xhvml' => 
  array (
    0 => 'application/xv+xml',
  ),
  'xvml' => 
  array (
    0 => 'application/xv+xml',
  ),
  'xvm' => 
  array (
    0 => 'application/xv+xml',
  ),
  'amr' => 
  array (
    0 => 'audio/amr',
  ),
  'snd' => 
  array (
    0 => 'audio/basic',
  ),
  'midi' => 
  array (
    0 => 'audio/midi',
  ),
  'kar' => 
  array (
    0 => 'audio/midi',
  ),
  'rmi' => 
  array (
    0 => 'audio/midi',
  ),
  'm4a' => 
  array (
    0 => 'audio/mp4',
  ),
  'mp3' => 
  array (
    0 => 'audio/mpeg',
  ),
  'mp2' => 
  array (
    0 => 'audio/mpeg',
  ),
  'mp2a' => 
  array (
    0 => 'audio/mpeg',
  ),
  'm2a' => 
  array (
    0 => 'audio/mpeg',
  ),
  'm3a' => 
  array (
    0 => 'audio/mpeg',
  ),
  'ogg' => 
  array (
    0 => 'audio/ogg',
  ),
  'spx' => 
  array (
    0 => 'audio/ogg',
  ),
  's3m' => 
  array (
    0 => 'audio/s3m',
  ),
  'sil' => 
  array (
    0 => 'audio/silk',
  ),
  'uvva' => 
  array (
    0 => 'audio/vnd.dece.audio',
  ),
  'aiff' => 
  array (
    0 => 'audio/x-aiff',
  ),
  'aifc' => 
  array (
    0 => 'audio/x-aiff',
  ),
  'caf' => 
  array (
    0 => 'audio/x-caf',
  ),
  'flac' => 
  array (
    0 => 'audio/x-flac',
    1 => 'audio/flac',
  ),
  'mka' => 
  array (
    0 => 'audio/x-matroska',
  ),
  'ra' => 
  array (
    0 => 'audio/x-pn-realaudio',
    1 => 'audio/x-realaudio',
  ),
  'xm' => 
  array (
    0 => 'audio/xm',
  ),
  'jpg' => 
  array (
    0 => 'image/jpeg',
  ),
  'jpe' => 
  array (
    0 => 'image/jpeg',
  ),
  'sgi' => 
  array (
    0 => 'image/sgi',
  ),
  'svgz' => 
  array (
    0 => 'image/svg+xml',
  ),
  'tif' => 
  array (
    0 => 'image/tiff',
  ),
  'uvvi' => 
  array (
    0 => 'image/vnd.dece.graphic',
  ),
  'uvg' => 
  array (
    0 => 'image/vnd.dece.graphic',
  ),
  'uvvg' => 
  array (
    0 => 'image/vnd.dece.graphic',
  ),
  'djv' => 
  array (
    0 => 'image/vnd.djvu',
  ),
  'wdp' => 
  array (
    0 => 'image/vnd.ms-photo',
  ),
  '3ds' => 
  array (
    0 => 'image/x-3ds',
  ),
  'fhc' => 
  array (
    0 => 'image/x-freehand',
  ),
  'fh4' => 
  array (
    0 => 'image/x-freehand',
  ),
  'fh5' => 
  array (
    0 => 'image/x-freehand',
  ),
  'fh7' => 
  array (
    0 => 'image/x-freehand',
  ),
  'sid' => 
  array (
    0 => 'image/x-mrsid-image',
    1 => 'audio/prs.sid',
  ),
  'pct' => 
  array (
    0 => 'image/x-pict',
  ),
  'tga' => 
  array (
    0 => 'image/x-tga',
  ),
  'mime' => 
  array (
    0 => 'message/rfc822',
  ),
  'iges' => 
  array (
    0 => 'model/iges',
  ),
  'mesh' => 
  array (
    0 => 'model/mesh',
  ),
  'silo' => 
  array (
    0 => 'model/mesh',
  ),
  'vrml' => 
  array (
    0 => 'model/vrml',
  ),
  'x3db' => 
  array (
    0 => 'model/x3d+binary',
  ),
  'x3dbz' => 
  array (
    0 => 'model/x3d+binary',
  ),
  'x3dv' => 
  array (
    0 => 'model/x3d+vrml',
  ),
  'x3dvz' => 
  array (
    0 => 'model/x3d+vrml',
  ),
  'x3dz' => 
  array (
    0 => 'model/x3d+xml',
  ),
  'appcache' => 
  array (
    0 => 'text/cache-manifest',
  ),
  'ifb' => 
  array (
    0 => 'text/calendar',
  ),
  'htm' => 
  array (
    0 => 'text/html',
  ),
  'text' => 
  array (
    0 => 'text/plain',
  ),
  'conf' => 
  array (
    0 => 'text/plain',
  ),
  'def' => 
  array (
    0 => 'text/plain',
  ),
  'list' => 
  array (
    0 => 'text/plain',
  ),
  'log' => 
  array (
    0 => 'text/plain',
  ),
  'in' => 
  array (
    0 => 'text/plain',
  ),
  'sgm' => 
  array (
    0 => 'text/sgml',
  ),
  'tr' => 
  array (
    0 => 'text/troff',
    1 => 'application/x-troff',
  ),
  'roff' => 
  array (
    0 => 'text/troff',
    1 => 'application/x-troff',
  ),
  'man' => 
  array (
    0 => 'text/troff',
    1 => 'application/x-troff-man',
  ),
  'me' => 
  array (
    0 => 'text/troff',
    1 => 'application/x-troff-me',
  ),
  'ms' => 
  array (
    0 => 'text/troff',
    1 => 'application/x-troff-ms',
  ),
  'uris' => 
  array (
    0 => 'text/uri-list',
  ),
  'urls' => 
  array (
    0 => 'text/uri-list',
  ),
  'vcard' => 
  array (
    0 => 'text/vcard',
  ),
  'asm' => 
  array (
    0 => 'text/x-asm',
  ),
  'cc' => 
  array (
    0 => 'text/x-c',
  ),
  'cxx' => 
  array (
    0 => 'text/x-c',
  ),
  'cpp' => 
  array (
    0 => 'text/x-c',
  ),
  'h' => 
  array (
    0 => 'text/x-c',
    1 => 'text/x-chdr',
  ),
  'hh' => 
  array (
    0 => 'text/x-c',
  ),
  'dic' => 
  array (
    0 => 'text/x-c',
  ),
  'for' => 
  array (
    0 => 'text/x-fortran',
  ),
  'f77' => 
  array (
    0 => 'text/x-fortran',
  ),
  'f90' => 
  array (
    0 => 'text/x-fortran',
  ),
  'nfo' => 
  array (
    0 => 'text/x-nfo',
  ),
  'opml' => 
  array (
    0 => 'text/x-opml',
  ),
  'pas' => 
  array (
    0 => 'text/x-pascal',
  ),
  'sfv' => 
  array (
    0 => 'text/x-sfv',
  ),
  'jpgm' => 
  array (
    0 => 'video/jpm',
  ),
  'mjp2' => 
  array (
    0 => 'video/mj2',
  ),
  'mp4v' => 
  array (
    0 => 'video/mp4',
  ),
  'mpg4' => 
  array (
    0 => 'video/mp4',
  ),
  'mpg' => 
  array (
    0 => 'video/mpeg',
  ),
  'mpe' => 
  array (
    0 => 'video/mpeg',
  ),
  'm1v' => 
  array (
    0 => 'video/mpeg',
  ),
  'm2v' => 
  array (
    0 => 'video/mpeg',
  ),
  'mov' => 
  array (
    0 => 'video/quicktime',
  ),
  'uvvh' => 
  array (
    0 => 'video/vnd.dece.hd',
  ),
  'uvvm' => 
  array (
    0 => 'video/vnd.dece.mobile',
  ),
  'uvvp' => 
  array (
    0 => 'video/vnd.dece.pd',
  ),
  'uvvs' => 
  array (
    0 => 'video/vnd.dece.sd',
  ),
  'uvvv' => 
  array (
    0 => 'video/vnd.dece.video',
  ),
  'dvb' => 
  array (
    0 => 'video/vnd.dvb.file',
  ),
  'm4u' => 
  array (
    0 => 'video/vnd.mpegurl',
  ),
  'uvvu' => 
  array (
    0 => 'video/vnd.uvvu.mp4',
  ),
  'mkv' => 
  array (
    0 => 'video/x-matroska',
  ),
  'mk3d' => 
  array (
    0 => 'video/x-matroska',
  ),
  'mks' => 
  array (
    0 => 'video/x-matroska',
  ),
  'mng' => 
  array (
    0 => 'video/x-mng',
  ),
  'asx' => 
  array (
    0 => 'video/x-ms-asf',
  ),
  'vob' => 
  array (
    0 => 'video/x-ms-vob',
  ),
  'smv' => 
  array (
    0 => 'video/x-smv',
  ),
  'anx' => 
  array (
    0 => 'application/annodex',
  ),
  'lin' => 
  array (
    0 => 'application/bbolin',
  ),
  'dcm' => 
  array (
    0 => 'application/dicom',
  ),
  'tsp' => 
  array (
    0 => 'application/dsptype',
  ),
  'hta' => 
  array (
    0 => 'application/hta',
  ),
  'msu' => 
  array (
    0 => 'application/octet-stream',
  ),
  'msp' => 
  array (
    0 => 'application/octet-stream',
  ),
  'key' => 
  array (
    0 => 'application/pgp-keys',
  ),
  'xsd' => 
  array (
    0 => 'application/xml',
  ),
  'ddeb' => 
  array (
    0 => 'application/vnd.debian.binary-package',
  ),
  'sfd' => 
  array (
    0 => 'application/vnd.font-fontforge-sfd',
  ),
  'xlb' => 
  array (
    0 => 'application/vnd.ms-excel',
  ),
  'sds' => 
  array (
    0 => 'application/vnd.stardivision.chart',
  ),
  'sdf' => 
  array (
    0 => 'application/vnd.stardivision.math',
    1 => 'chemical/x-mdl-sdfile',
  ),
  'cda' => 
  array (
    0 => 'application/x-cdf',
  ),
  'mph' => 
  array (
    0 => 'application/x-comsol',
  ),
  'mm' => 
  array (
    0 => 'application/x-freemind',
  ),
  'gan' => 
  array (
    0 => 'application/x-ganttproject',
  ),
  'sgf' => 
  array (
    0 => 'application/x-go-sgf',
  ),
  'gcf' => 
  array (
    0 => 'application/x-graphing-calculator',
  ),
  'tgz' => 
  array (
    0 => 'application/x-gtar-compressed',
  ),
  'taz' => 
  array (
    0 => 'application/x-gtar-compressed',
  ),
  'hwp' => 
  array (
    0 => 'application/x-hwp',
  ),
  'ica' => 
  array (
    0 => 'application/x-ica',
  ),
  'info' => 
  array (
    0 => 'application/x-info',
  ),
  'ins' => 
  array (
    0 => 'application/x-internet-signup',
  ),
  'isp' => 
  array (
    0 => 'application/x-internet-signup',
  ),
  'iii' => 
  array (
    0 => 'application/x-iphone',
  ),
  'jmz' => 
  array (
    0 => 'application/x-jmol',
  ),
  'kil' => 
  array (
    0 => 'application/x-killustrator',
  ),
  'lyx' => 
  array (
    0 => 'application/x-lyx',
  ),
  'lzx' => 
  array (
    0 => 'application/x-lzx',
  ),
  'frm' => 
  array (
    0 => 'application/x-maker',
  ),
  'fb' => 
  array (
    0 => 'application/x-maker',
  ),
  'fbdoc' => 
  array (
    0 => 'application/x-maker',
  ),
  'manifest' => 
  array (
    0 => 'application/x-ms-manifest',
  ),
  'pac' => 
  array (
    0 => 'application/x-ns-proxy-autoconfig',
  ),
  'nwc' => 
  array (
    0 => 'application/x-nwc',
  ),
  'o' => 
  array (
    0 => 'application/x-object',
  ),
  'oza' => 
  array (
    0 => 'application/x-oz-application',
  ),
  'pyc' => 
  array (
    0 => 'application/x-python-code',
  ),
  'pyo' => 
  array (
    0 => 'application/x-python-code',
  ),
  'qgs' => 
  array (
    0 => 'application/x-qgis',
  ),
  'shp' => 
  array (
    0 => 'application/x-qgis',
  ),
  'shx' => 
  array (
    0 => 'application/x-qgis',
  ),
  'qtl' => 
  array (
    0 => 'application/x-quicktimeplayer',
  ),
  'rdp' => 
  array (
    0 => 'application/x-rdp',
  ),
  'rpm' => 
  array (
    0 => 'application/x-redhat-package-manager',
  ),
  'rb' => 
  array (
    0 => 'application/x-ruby',
  ),
  'sci' => 
  array (
    0 => 'application/x-scilab',
  ),
  'sce' => 
  array (
    0 => 'application/x-scilab',
  ),
  'xcos' => 
  array (
    0 => 'application/x-scilab-xcos',
  ),
  'swfl' => 
  array (
    0 => 'application/x-shockwave-flash',
  ),
  'scr' => 
  array (
    0 => 'application/x-silverlight',
  ),
  'gf' => 
  array (
    0 => 'application/x-tex-gf',
  ),
  'pk' => 
  array (
    0 => 'application/x-tex-pk',
  ),
  'wz' => 
  array (
    0 => 'application/x-wingz',
  ),
  'xcf' => 
  array (
    0 => 'application/x-xcf',
  ),
  'awb' => 
  array (
    0 => 'audio/amr-wb',
  ),
  'axa' => 
  array (
    0 => 'audio/annodex',
  ),
  'csd' => 
  array (
    0 => 'audio/csound',
  ),
  'orc' => 
  array (
    0 => 'audio/csound',
  ),
  'sco' => 
  array (
    0 => 'audio/csound',
  ),
  'opus' => 
  array (
    0 => 'audio/ogg',
  ),
  'gsm' => 
  array (
    0 => 'audio/x-gsm',
  ),
  'alc' => 
  array (
    0 => 'chemical/x-alchemy',
  ),
  'cac' => 
  array (
    0 => 'chemical/x-cache',
  ),
  'cache' => 
  array (
    0 => 'chemical/x-cache',
  ),
  'csf' => 
  array (
    0 => 'chemical/x-cache-csf',
  ),
  'cbin' => 
  array (
    0 => 'chemical/x-cactvs-binary',
  ),
  'cascii' => 
  array (
    0 => 'chemical/x-cactvs-binary',
  ),
  'ctab' => 
  array (
    0 => 'chemical/x-cactvs-binary',
  ),
  'cpa' => 
  array (
    0 => 'chemical/x-compass',
  ),
  'bsd' => 
  array (
    0 => 'chemical/x-crossfire',
  ),
  'csm' => 
  array (
    0 => 'chemical/x-csml',
  ),
  'ctx' => 
  array (
    0 => 'chemical/x-ctx',
  ),
  'cxf' => 
  array (
    0 => 'chemical/x-cxf',
  ),
  'cef' => 
  array (
    0 => 'chemical/x-cxf',
  ),
  'emb' => 
  array (
    0 => 'chemical/x-embl-dl-nucleotide',
  ),
  'embl' => 
  array (
    0 => 'chemical/x-embl-dl-nucleotide',
  ),
  'inp' => 
  array (
    0 => 'chemical/x-gamess-input',
  ),
  'gamin' => 
  array (
    0 => 'chemical/x-gamess-input',
  ),
  'fch' => 
  array (
    0 => 'chemical/x-gaussian-checkpoint',
  ),
  'fchk' => 
  array (
    0 => 'chemical/x-gaussian-checkpoint',
  ),
  'cub' => 
  array (
    0 => 'chemical/x-gaussian-cube',
  ),
  'gau' => 
  array (
    0 => 'chemical/x-gaussian-input',
  ),
  'gjc' => 
  array (
    0 => 'chemical/x-gaussian-input',
  ),
  'gjf' => 
  array (
    0 => 'chemical/x-gaussian-input',
  ),
  'gal' => 
  array (
    0 => 'chemical/x-gaussian-log',
  ),
  'gen' => 
  array (
    0 => 'chemical/x-genbank',
  ),
  'hin' => 
  array (
    0 => 'chemical/x-hin',
  ),
  'istr' => 
  array (
    0 => 'chemical/x-isostar',
  ),
  'ist' => 
  array (
    0 => 'chemical/x-isostar',
  ),
  'jdx' => 
  array (
    0 => 'chemical/x-jcamp-dx',
  ),
  'dx' => 
  array (
    0 => 'chemical/x-jcamp-dx',
  ),
  'kin' => 
  array (
    0 => 'chemical/x-kinemage',
  ),
  'mcm' => 
  array (
    0 => 'chemical/x-macmolecule',
  ),
  'mmod' => 
  array (
    0 => 'chemical/x-macromodel-input',
  ),
  'mol' => 
  array (
    0 => 'chemical/x-mdl-molfile',
  ),
  'rd' => 
  array (
    0 => 'chemical/x-mdl-rdfile',
  ),
  'rxn' => 
  array (
    0 => 'chemical/x-mdl-rxnfile',
  ),
  'sd' => 
  array (
    0 => 'chemical/x-mdl-sdfile',
  ),
  'tgf' => 
  array (
    0 => 'chemical/x-mdl-tgf',
  ),
  'mcif' => 
  array (
    0 => 'chemical/x-mmcif',
  ),
  'gpt' => 
  array (
    0 => 'chemical/x-mopac-graph',
  ),
  'mop' => 
  array (
    0 => 'chemical/x-mopac-input',
  ),
  'mopcrt' => 
  array (
    0 => 'chemical/x-mopac-input',
  ),
  'zmt' => 
  array (
    0 => 'chemical/x-mopac-input',
  ),
  'moo' => 
  array (
    0 => 'chemical/x-mopac-out',
  ),
  'ent' => 
  array (
    0 => 'chemical/x-pdb',
  ),
  'ros' => 
  array (
    0 => 'chemical/x-rosdal',
  ),
  'sw' => 
  array (
    0 => 'chemical/x-swissprot',
  ),
  'vmd' => 
  array (
    0 => 'chemical/x-vmd',
  ),
  'xtel' => 
  array (
    0 => 'chemical/x-xtel',
  ),
  'jpx' => 
  array (
    0 => 'image/jpx',
  ),
  'jpf' => 
  array (
    0 => 'image/jpx',
  ),
  'crw' => 
  array (
    0 => 'image/x-canon-crw',
  ),
  'cdr' => 
  array (
    0 => 'image/x-coreldraw',
  ),
  'pat' => 
  array (
    0 => 'image/x-coreldrawpattern',
  ),
  'cdt' => 
  array (
    0 => 'image/x-coreldrawtemplate',
  ),
  'erf' => 
  array (
    0 => 'image/x-epson-erf',
  ),
  'art' => 
  array (
    0 => 'image/x-jg',
  ),
  'jng' => 
  array (
    0 => 'image/x-jng',
  ),
  'nef' => 
  array (
    0 => 'image/x-nikon-nef',
  ),
  'orf' => 
  array (
    0 => 'image/x-olympus-orf',
  ),
  'icz' => 
  array (
    0 => 'text/calendar',
  ),
  'shtml' => 
  array (
    0 => 'text/html',
  ),
  'uls' => 
  array (
    0 => 'text/iuls',
  ),
  'mml' => 
  array (
    0 => 'text/mathml',
  ),
  'md' => 
  array (
    0 => 'text/markdown',
  ),
  'markdown' => 
  array (
    0 => 'text/markdown',
  ),
  'brf' => 
  array (
    0 => 'text/plain',
  ),
  'sct' => 
  array (
    0 => 'text/scriptlet',
  ),
  'wsc' => 
  array (
    0 => 'text/scriptlet',
  ),
  'tm' => 
  array (
    0 => 'text/texmacs',
  ),
  'bib' => 
  array (
    0 => 'text/x-bibtex',
  ),
  'boo' => 
  array (
    0 => 'text/x-boo',
  ),
  'htc' => 
  array (
    0 => 'text/x-component',
  ),
  'd' => 
  array (
    0 => 'text/x-dsrc',
  ),
  'diff' => 
  array (
    0 => 'text/x-diff',
  ),
  'patch' => 
  array (
    0 => 'text/x-diff',
  ),
  'hs' => 
  array (
    0 => 'text/x-haskell',
  ),
  'ly' => 
  array (
    0 => 'text/x-lilypond',
  ),
  'lhs' => 
  array (
    0 => 'text/x-literate-haskell',
  ),
  'moc' => 
  array (
    0 => 'text/x-moc',
  ),
  'gcd' => 
  array (
    0 => 'text/x-pcs-gcd',
  ),
  'pl' => 
  array (
    0 => 'text/x-perl',
    1 => 'application/x-perl',
  ),
  'pm' => 
  array (
    0 => 'text/x-perl',
    1 => 'application/x-perl',
  ),
  'py' => 
  array (
    0 => 'text/x-python',
  ),
  'scala' => 
  array (
    0 => 'text/x-scala',
  ),
  'tk' => 
  array (
    0 => 'text/x-tcl',
    1 => 'application/x-tcl',
  ),
  'ltx' => 
  array (
    0 => 'text/x-tex',
  ),
  'sty' => 
  array (
    0 => 'text/x-tex',
  ),
  'cls' => 
  array (
    0 => 'text/x-tex',
  ),
  'axv' => 
  array (
    0 => 'video/annodex',
  ),
  'dl' => 
  array (
    0 => 'video/dl',
  ),
  'dif' => 
  array (
    0 => 'video/dv',
  ),
  'dv' => 
  array (
    0 => 'video/dv',
  ),
  'gl' => 
  array (
    0 => 'video/gl',
  ),
  'lsf' => 
  array (
    0 => 'video/x-la-asf',
  ),
  'lsx' => 
  array (
    0 => 'video/x-la-asf',
  ),
  'mpv' => 
  array (
    0 => 'video/x-matroska',
  ),
  'war' => 
  array (
    0 => 'application/java-archive',
  ),
  'ear' => 
  array (
    0 => 'application/java-archive',
  ),
  'cco' => 
  array (
    0 => 'application/x-cocoa',
  ),
  'jardiff' => 
  array (
    0 => 'application/x-java-archive-diff',
  ),
  'run' => 
  array (
    0 => 'application/x-makeself',
  ),
  'sea' => 
  array (
    0 => 'application/x-sea',
  ),
  'img' => 
  array (
    0 => 'application/octet-stream',
  ),
  'msm' => 
  array (
    0 => 'application/octet-stream',
  ),
);