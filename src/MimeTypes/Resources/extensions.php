<?php return array (
  'application/x-fictionbook' => 
  array (
    0 => 'fb2',
  ),
  'application/x-gzip' => 
  array (
    0 => 'gz',
  ),
  'application/gzip' => 
  array (
    0 => 'gz',
  ),
  'text/rtf' => 
  array (
    0 => 'rtf',
  ),
  'application/rtf' => 
  array (
    0 => 'rtf',
  ),
  'application/vnd.lotus-1-2-3' => 
  array (
    0 => 123,
  ),
  'text/vnd.in3d.3dml' => 
  array (
    0 => '3dml',
  ),
  'video/3gpp2' => 
  array (
    0 => '3g2',
  ),
  'video/3gpp' => 
  array (
    0 => '3gp',
  ),
  'application/x-7z-compressed' => 
  array (
    0 => '7z',
  ),
  'application/x-authorware-bin' => 
  array (
    0 => 'aab',
    1 => 'x32',
    2 => 'u32',
    3 => 'vox',
  ),
  'audio/x-aac' => 
  array (
    0 => 'aac',
  ),
  'application/x-authorware-map' => 
  array (
    0 => 'aam',
  ),
  'application/x-authorware-seg' => 
  array (
    0 => 'aas',
  ),
  'application/x-abiword' => 
  array (
    0 => 'abw',
  ),
  'application/pkix-attr-cert' => 
  array (
    0 => 'ac',
  ),
  'application/vnd.americandynamics.acc' => 
  array (
    0 => 'acc',
  ),
  'application/x-ace-compressed' => 
  array (
    0 => 'ace',
  ),
  'application/vnd.acucobol' => 
  array (
    0 => 'acu',
  ),
  'audio/adpcm' => 
  array (
    0 => 'adp',
  ),
  'application/vnd.audiograph' => 
  array (
    0 => 'aep',
  ),
  'application/vnd.ibm.modcap' => 
  array (
    0 => 'afp',
    1 => 'listafp',
    2 => 'list3820',
  ),
  'application/vnd.ahead.space' => 
  array (
    0 => 'ahead',
  ),
  'application/postscript' => 
  array (
    0 => 'ai',
    1 => 'eps',
    2 => 'ps',
  ),
  'audio/x-aiff' => 
  array (
    0 => 'aif',
    1 => 'aiff',
    2 => 'aifc',
  ),
  'application/vnd.adobe.air-application-installer-package+zip' => 
  array (
    0 => 'air',
  ),
  'application/vnd.dvb.ait' => 
  array (
    0 => 'ait',
  ),
  'application/vnd.amiga.ami' => 
  array (
    0 => 'ami',
  ),
  'application/vnd.android.package-archive' => 
  array (
    0 => 'apk',
  ),
  'application/x-ms-application' => 
  array (
    0 => 'application',
  ),
  'application/vnd.lotus-approach' => 
  array (
    0 => 'apr',
  ),
  'video/x-ms-asf' => 
  array (
    0 => 'asf',
    1 => 'asx',
  ),
  'application/vnd.accpac.simply.aso' => 
  array (
    0 => 'aso',
  ),
  'application/vnd.acucorp' => 
  array (
    0 => 'atc',
    1 => 'acutc',
  ),
  'application/atom+xml' => 
  array (
    0 => 'atom',
  ),
  'application/atomcat+xml' => 
  array (
    0 => 'atomcat',
  ),
  'application/atomsvc+xml' => 
  array (
    0 => 'atomsvc',
  ),
  'application/vnd.antix.game-component' => 
  array (
    0 => 'atx',
  ),
  'audio/basic' => 
  array (
    0 => 'au',
    1 => 'snd',
  ),
  'video/x-msvideo' => 
  array (
    0 => 'avi',
  ),
  'application/applixware' => 
  array (
    0 => 'aw',
  ),
  'application/vnd.airzip.filesecure.azf' => 
  array (
    0 => 'azf',
  ),
  'application/vnd.airzip.filesecure.azs' => 
  array (
    0 => 'azs',
  ),
  'application/vnd.amazon.ebook' => 
  array (
    0 => 'azw',
  ),
  'application/x-bcpio' => 
  array (
    0 => 'bcpio',
  ),
  'application/x-font-bdf' => 
  array (
    0 => 'bdf',
  ),
  'application/vnd.syncml.dm+wbxml' => 
  array (
    0 => 'bdm',
  ),
  'application/vnd.realvnc.bed' => 
  array (
    0 => 'bed',
  ),
  'application/vnd.fujitsu.oasysprs' => 
  array (
    0 => 'bh2',
  ),
  'application/octet-stream' => 
  array (
    0 => 'bin',
    1 => 'deb',
    2 => 'exe',
    3 => 'dms',
    4 => 'lrf',
    5 => 'mar',
    6 => 'so',
    7 => 'dist',
    8 => 'distz',
    9 => 'pkg',
    10 => 'bpk',
    11 => 'dump',
    12 => 'elc',
    13 => 'deploy',
    14 => 'dmg',
    15 => 'iso',
    16 => 'dll',
    17 => 'msi',
    18 => 'msu',
    19 => 'msp',
    20 => 'img',
    21 => 'msm',
  ),
  'application/vnd.bmi' => 
  array (
    0 => 'bmi',
  ),
  'image/bmp' => 
  array (
    0 => 'bmp',
  ),
  'image/x-ms-bmp' => 
  array (
    0 => 'bmp',
  ),
  'application/vnd.previewsystems.box' => 
  array (
    0 => 'box',
  ),
  'image/prs.btif' => 
  array (
    0 => 'btif',
  ),
  'application/x-bzip' => 
  array (
    0 => 'bz',
  ),
  'application/x-bzip2' => 
  array (
    0 => 'bz2',
    1 => 'boz',
  ),
  'text/x-c' => 
  array (
    0 => 'c',
    1 => 'cc',
    2 => 'cxx',
    3 => 'cpp',
    4 => 'h',
    5 => 'hh',
    6 => 'dic',
  ),
  'text/x-csrc' => 
  array (
    0 => 'c',
  ),
  'application/vnd.cluetrust.cartomobile-config' => 
  array (
    0 => 'c11amc',
  ),
  'application/vnd.cluetrust.cartomobile-config-pkg' => 
  array (
    0 => 'c11amz',
  ),
  'application/vnd.clonk.c4group' => 
  array (
    0 => 'c4g',
    1 => 'c4d',
    2 => 'c4f',
    3 => 'c4p',
    4 => 'c4u',
  ),
  'application/vnd.ms-cab-compressed' => 
  array (
    0 => 'cab',
  ),
  'application/x-cab' => 
  array (
    0 => 'cab',
  ),
  'application/vnd.curl.car' => 
  array (
    0 => 'car',
  ),
  'application/vnd.ms-pki.seccat' => 
  array (
    0 => 'cat',
  ),
  'application/ccxml+xml,' => 
  array (
    0 => 'ccxml',
  ),
  'application/ccxml+xml' => 
  array (
    0 => 'ccxml',
  ),
  'application/vnd.contact.cmsg' => 
  array (
    0 => 'cdbcmsg',
  ),
  'application/vnd.mediastation.cdkey' => 
  array (
    0 => 'cdkey',
  ),
  'application/cdmi-capability' => 
  array (
    0 => 'cdmia',
  ),
  'application/cdmi-container' => 
  array (
    0 => 'cdmic',
  ),
  'application/cdmi-domain' => 
  array (
    0 => 'cdmid',
  ),
  'application/cdmi-object' => 
  array (
    0 => 'cdmio',
  ),
  'application/cdmi-queue' => 
  array (
    0 => 'cdmiq',
  ),
  'chemical/x-cdx' => 
  array (
    0 => 'cdx',
  ),
  'application/vnd.chemdraw+xml' => 
  array (
    0 => 'cdxml',
  ),
  'application/vnd.cinderella' => 
  array (
    0 => 'cdy',
  ),
  'application/pkix-cert' => 
  array (
    0 => 'cer',
  ),
  'chemical/x-cerius' => 
  array (
    0 => 'cer',
  ),
  'image/cgm' => 
  array (
    0 => 'cgm',
  ),
  'application/x-chat' => 
  array (
    0 => 'chat',
  ),
  'application/vnd.ms-htmlhelp' => 
  array (
    0 => 'chm',
  ),
  'chemical/x-chemdraw' => 
  array (
    0 => 'chm',
  ),
  'application/vnd.kde.kchart' => 
  array (
    0 => 'chrt',
  ),
  'application/x-kchart' => 
  array (
    0 => 'chrt',
  ),
  'chemical/x-cif' => 
  array (
    0 => 'cif',
  ),
  'application/vnd.anser-web-certificate-issue-initiation' => 
  array (
    0 => 'cii',
  ),
  'application/vnd.ms-artgalry' => 
  array (
    0 => 'cil',
  ),
  'application/vnd.claymore' => 
  array (
    0 => 'cla',
  ),
  'application/java-vm' => 
  array (
    0 => 'class',
  ),
  'application/vnd.crick.clicker.keyboard' => 
  array (
    0 => 'clkk',
  ),
  'application/vnd.crick.clicker.palette' => 
  array (
    0 => 'clkp',
  ),
  'application/vnd.crick.clicker.template' => 
  array (
    0 => 'clkt',
  ),
  'application/vnd.crick.clicker.wordbank' => 
  array (
    0 => 'clkw',
  ),
  'application/vnd.crick.clicker' => 
  array (
    0 => 'clkx',
  ),
  'application/x-msclip' => 
  array (
    0 => 'clp',
  ),
  'application/vnd.cosmocaller' => 
  array (
    0 => 'cmc',
  ),
  'chemical/x-cmdf' => 
  array (
    0 => 'cmdf',
  ),
  'chemical/x-cml' => 
  array (
    0 => 'cml',
  ),
  'application/vnd.yellowriver-custom-menu' => 
  array (
    0 => 'cmp',
  ),
  'image/x-cmx' => 
  array (
    0 => 'cmx',
  ),
  'application/vnd.rim.cod' => 
  array (
    0 => 'cod',
  ),
  'application/x-cpio' => 
  array (
    0 => 'cpio',
  ),
  'application/mac-compactpro' => 
  array (
    0 => 'cpt',
  ),
  'image/x-corelphotopaint' => 
  array (
    0 => 'cpt',
  ),
  'application/x-mscardfile' => 
  array (
    0 => 'crd',
  ),
  'application/pkix-crl' => 
  array (
    0 => 'crl',
  ),
  'application/vnd.rig.cryptonote' => 
  array (
    0 => 'cryptonote',
  ),
  'application/x-csh' => 
  array (
    0 => 'csh',
  ),
  'text/x-csh' => 
  array (
    0 => 'csh',
  ),
  'chemical/x-csml' => 
  array (
    0 => 'csml',
    1 => 'csm',
  ),
  'application/vnd.commonspace' => 
  array (
    0 => 'csp',
  ),
  'text/css' => 
  array (
    0 => 'css',
  ),
  'text/csv' => 
  array (
    0 => 'csv',
  ),
  'application/cu-seeme' => 
  array (
    0 => 'cu',
  ),
  'text/vnd.curl' => 
  array (
    0 => 'curl',
  ),
  'application/prs.cww' => 
  array (
    0 => 'cww',
  ),
  'model/vnd.collada+xml' => 
  array (
    0 => 'dae',
  ),
  'application/vnd.mobius.daf' => 
  array (
    0 => 'daf',
  ),
  'application/davmount+xml' => 
  array (
    0 => 'davmount',
  ),
  'text/vnd.curl.dcurl' => 
  array (
    0 => 'dcurl',
  ),
  'application/vnd.oma.dd2+xml' => 
  array (
    0 => 'dd2',
  ),
  'application/vnd.fujixerox.ddd' => 
  array (
    0 => 'ddd',
  ),
  'application/x-debian-package' => 
  array (
    0 => 'deb',
    1 => 'udeb',
  ),
  'application/vnd.debian.binary-package' => 
  array (
    0 => 'deb',
    1 => 'udeb',
    2 => 'ddeb',
  ),
  'application/x-x509-ca-cert' => 
  array (
    0 => 'der',
    1 => 'crt',
  ),
  'application/vnd.dreamfactory' => 
  array (
    0 => 'dfac',
  ),
  'application/x-director' => 
  array (
    0 => 'dir',
    1 => 'dcr',
    2 => 'dxr',
    3 => 'cst',
    4 => 'cct',
    5 => 'cxt',
    6 => 'w3d',
    7 => 'fgd',
    8 => 'swa',
  ),
  'application/vnd.mobius.dis' => 
  array (
    0 => 'dis',
  ),
  'image/vnd.djvu' => 
  array (
    0 => 'djvu',
    1 => 'djv',
  ),
  'application/vnd.dna' => 
  array (
    0 => 'dna',
  ),
  'application/msword' => 
  array (
    0 => 'doc',
    1 => 'dot',
  ),
  'application/vnd.ms-word.document.macroenabled.12' => 
  array (
    0 => 'docm',
  ),
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 
  array (
    0 => 'docx',
  ),
  'application/vnd.ms-word.template.macroenabled.12' => 
  array (
    0 => 'dotm',
  ),
  'application/vnd.openxmlformats-officedocument.wordprocessingml.template' => 
  array (
    0 => 'dotx',
  ),
  'application/vnd.osgi.dp' => 
  array (
    0 => 'dp',
  ),
  'application/vnd.dpgraph' => 
  array (
    0 => 'dpg',
  ),
  'audio/vnd.dra' => 
  array (
    0 => 'dra',
  ),
  'text/prs.lines.tag' => 
  array (
    0 => 'dsc',
  ),
  'application/dssc+der' => 
  array (
    0 => 'dssc',
  ),
  'application/x-dtbook+xml' => 
  array (
    0 => 'dtb',
  ),
  'application/xml-dtd' => 
  array (
    0 => 'dtd',
  ),
  'audio/vnd.dts' => 
  array (
    0 => 'dts',
  ),
  'audio/vnd.dts.hd' => 
  array (
    0 => 'dtshd',
  ),
  'application/x-dvi' => 
  array (
    0 => 'dvi',
  ),
  'model/vnd.dwf' => 
  array (
    0 => 'dwf',
  ),
  'image/vnd.dwg' => 
  array (
    0 => 'dwg',
  ),
  'image/vnd.dxf' => 
  array (
    0 => 'dxf',
  ),
  'application/vnd.spotfire.dxp' => 
  array (
    0 => 'dxp',
  ),
  'audio/vnd.nuera.ecelp4800' => 
  array (
    0 => 'ecelp4800',
  ),
  'audio/vnd.nuera.ecelp7470' => 
  array (
    0 => 'ecelp7470',
  ),
  'audio/vnd.nuera.ecelp9600' => 
  array (
    0 => 'ecelp9600',
  ),
  'application/vnd.novadigm.edm' => 
  array (
    0 => 'edm',
  ),
  'application/vnd.novadigm.edx' => 
  array (
    0 => 'edx',
  ),
  'application/vnd.picsel' => 
  array (
    0 => 'efif',
  ),
  'application/vnd.pg.osasli' => 
  array (
    0 => 'ei6',
  ),
  'message/rfc822' => 
  array (
    0 => 'eml',
    1 => 'mime',
  ),
  'application/emma+xml' => 
  array (
    0 => 'emma',
  ),
  'audio/vnd.digital-winds' => 
  array (
    0 => 'eol',
  ),
  'application/vnd.ms-fontobject' => 
  array (
    0 => 'eot',
  ),
  'application/epub+zip' => 
  array (
    0 => 'epub',
  ),
  'application/ecmascript' => 
  array (
    0 => 'es',
    1 => 'ecma',
  ),
  'application/vnd.eszigno3+xml' => 
  array (
    0 => 'es3',
    1 => 'et3',
  ),
  'application/vnd.epson.esf' => 
  array (
    0 => 'esf',
  ),
  'text/x-setext' => 
  array (
    0 => 'etx',
  ),
  'application/x-msdownload' => 
  array (
    0 => 'exe',
    1 => 'dll',
    2 => 'com',
    3 => 'bat',
    4 => 'msi',
  ),
  'application/x-msdos-program' => 
  array (
    0 => 'exe',
    1 => 'dll',
    2 => 'com',
    3 => 'bat',
  ),
  'application/exi' => 
  array (
    0 => 'exi',
  ),
  'application/vnd.novadigm.ext' => 
  array (
    0 => 'ext',
  ),
  'application/vnd.ezpix-album' => 
  array (
    0 => 'ez2',
  ),
  'application/vnd.ezpix-package' => 
  array (
    0 => 'ez3',
  ),
  'text/x-fortran' => 
  array (
    0 => 'f',
    1 => 'for',
    2 => 'f77',
    3 => 'f90',
  ),
  'video/x-f4v' => 
  array (
    0 => 'f4v',
  ),
  'image/vnd.fastbidsheet' => 
  array (
    0 => 'fbs',
  ),
  'application/vnd.isac.fcs' => 
  array (
    0 => 'fcs',
  ),
  'application/vnd.fdf' => 
  array (
    0 => 'fdf',
  ),
  'application/vnd.denovo.fcselayout-link' => 
  array (
    0 => 'fe_launch',
  ),
  'application/vnd.fujitsu.oasysgp' => 
  array (
    0 => 'fg5',
  ),
  'image/x-freehand' => 
  array (
    0 => 'fh',
    1 => 'fhc',
    2 => 'fh4',
    3 => 'fh5',
    4 => 'fh7',
  ),
  'application/x-xfig' => 
  array (
    0 => 'fig',
  ),
  'video/x-fli' => 
  array (
    0 => 'fli',
  ),
  'video/fli' => 
  array (
    0 => 'fli',
  ),
  'application/vnd.micrografx.flo' => 
  array (
    0 => 'flo',
  ),
  'video/x-flv' => 
  array (
    0 => 'flv',
  ),
  'application/vnd.kde.kivio' => 
  array (
    0 => 'flw',
  ),
  'text/vnd.fmi.flexstor' => 
  array (
    0 => 'flx',
  ),
  'text/vnd.fly' => 
  array (
    0 => 'fly',
  ),
  'application/vnd.framemaker' => 
  array (
    0 => 'fm',
    1 => 'frame',
    2 => 'maker',
    3 => 'book',
  ),
  'application/x-maker' => 
  array (
    0 => 'fm',
    1 => 'frame',
    2 => 'maker',
    3 => 'book',
    4 => 'frm',
    5 => 'fb',
    6 => 'fbdoc',
  ),
  'application/vnd.frogans.fnc' => 
  array (
    0 => 'fnc',
  ),
  'image/vnd.fpx' => 
  array (
    0 => 'fpx',
  ),
  'application/vnd.fsc.weblaunch' => 
  array (
    0 => 'fsc',
  ),
  'image/vnd.fst' => 
  array (
    0 => 'fst',
  ),
  'application/vnd.fluxtime.clip' => 
  array (
    0 => 'ftc',
  ),
  'application/vnd.anser-web-funds-transfer-initiation' => 
  array (
    0 => 'fti',
  ),
  'video/vnd.fvt' => 
  array (
    0 => 'fvt',
  ),
  'application/vnd.adobe.fxp' => 
  array (
    0 => 'fxp',
    1 => 'fxpl',
  ),
  'application/vnd.fuzzysheet' => 
  array (
    0 => 'fzs',
  ),
  'application/vnd.geoplan' => 
  array (
    0 => 'g2w',
  ),
  'image/g3fax' => 
  array (
    0 => 'g3',
  ),
  'application/vnd.geospace' => 
  array (
    0 => 'g3w',
  ),
  'application/vnd.groove-account' => 
  array (
    0 => 'gac',
  ),
  'model/vnd.gdl' => 
  array (
    0 => 'gdl',
  ),
  'application/vnd.dynageo' => 
  array (
    0 => 'geo',
  ),
  'application/vnd.geometry-explorer' => 
  array (
    0 => 'gex',
    1 => 'gre',
  ),
  'application/vnd.geogebra.file' => 
  array (
    0 => 'ggb',
  ),
  'application/vnd.geogebra.tool' => 
  array (
    0 => 'ggt',
  ),
  'application/vnd.groove-help' => 
  array (
    0 => 'ghf',
  ),
  'image/gif' => 
  array (
    0 => 'gif',
  ),
  'application/vnd.groove-identity-message' => 
  array (
    0 => 'gim',
  ),
  'application/vnd.gmx' => 
  array (
    0 => 'gmx',
  ),
  'application/x-gnumeric' => 
  array (
    0 => 'gnumeric',
  ),
  'application/vnd.flographit' => 
  array (
    0 => 'gph',
  ),
  'application/vnd.grafeq' => 
  array (
    0 => 'gqf',
    1 => 'gqs',
  ),
  'application/srgs' => 
  array (
    0 => 'gram',
  ),
  'application/vnd.groove-injector' => 
  array (
    0 => 'grv',
  ),
  'application/srgs+xml' => 
  array (
    0 => 'grxml',
  ),
  'application/x-font-ghostscript' => 
  array (
    0 => 'gsf',
  ),
  'application/x-font' => 
  array (
    0 => 'gsf',
    1 => 'pfa',
    2 => 'pfb',
  ),
  'application/x-gtar' => 
  array (
    0 => 'gtar',
  ),
  'application/vnd.groove-tool-message' => 
  array (
    0 => 'gtm',
  ),
  'model/vnd.gtw' => 
  array (
    0 => 'gtw',
  ),
  'text/vnd.graphviz' => 
  array (
    0 => 'gv',
  ),
  'application/vnd.geonext' => 
  array (
    0 => 'gxt',
  ),
  'video/h261' => 
  array (
    0 => 'h261',
  ),
  'video/h263' => 
  array (
    0 => 'h263',
  ),
  'video/h264' => 
  array (
    0 => 'h264',
  ),
  'application/vnd.hal+xml' => 
  array (
    0 => 'hal',
  ),
  'application/vnd.hbci' => 
  array (
    0 => 'hbci',
  ),
  'application/x-hdf' => 
  array (
    0 => 'hdf',
  ),
  'application/winhlp' => 
  array (
    0 => 'hlp',
  ),
  'application/vnd.hp-hpgl' => 
  array (
    0 => 'hpgl',
  ),
  'application/vnd.hp-hpid' => 
  array (
    0 => 'hpid',
  ),
  'application/vnd.hp-hps' => 
  array (
    0 => 'hps',
  ),
  'application/mac-binhex40' => 
  array (
    0 => 'hqx',
  ),
  'application/vnd.kenameaapp' => 
  array (
    0 => 'htke',
  ),
  'text/html' => 
  array (
    0 => 'html',
    1 => 'htm',
    2 => 'shtml',
  ),
  'application/vnd.yamaha.hv-dic' => 
  array (
    0 => 'hvd',
  ),
  'application/vnd.yamaha.hv-voice' => 
  array (
    0 => 'hvp',
  ),
  'application/vnd.yamaha.hv-script' => 
  array (
    0 => 'hvs',
  ),
  'application/vnd.intergeo' => 
  array (
    0 => 'i2g',
  ),
  'application/vnd.iccprofile' => 
  array (
    0 => 'icc',
    1 => 'icm',
  ),
  'x-conference/x-cooltalk' => 
  array (
    0 => 'ice',
  ),
  'image/x-icon' => 
  array (
    0 => 'ico',
  ),
  'image/vnd.microsoft.icon' => 
  array (
    0 => 'ico',
  ),
  'text/calendar' => 
  array (
    0 => 'ics',
    1 => 'ifb',
    2 => 'icz',
  ),
  'image/ief' => 
  array (
    0 => 'ief',
  ),
  'application/vnd.shana.informed.formdata' => 
  array (
    0 => 'ifm',
  ),
  'application/vnd.igloader' => 
  array (
    0 => 'igl',
  ),
  'application/vnd.insors.igm' => 
  array (
    0 => 'igm',
  ),
  'model/iges' => 
  array (
    0 => 'igs',
    1 => 'iges',
  ),
  'application/vnd.micrografx.igx' => 
  array (
    0 => 'igx',
  ),
  'application/vnd.shana.informed.interchange' => 
  array (
    0 => 'iif',
  ),
  'application/vnd.accpac.simply.imp' => 
  array (
    0 => 'imp',
  ),
  'application/vnd.ms-ims' => 
  array (
    0 => 'ims',
  ),
  'application/ipfix' => 
  array (
    0 => 'ipfix',
  ),
  'application/vnd.shana.informed.package' => 
  array (
    0 => 'ipk',
  ),
  'application/vnd.ibm.rights-management' => 
  array (
    0 => 'irm',
  ),
  'application/vnd.irepository.package+xml' => 
  array (
    0 => 'irp',
  ),
  'application/vnd.shana.informed.formtemplate' => 
  array (
    0 => 'itp',
  ),
  'application/vnd.immervision-ivp' => 
  array (
    0 => 'ivp',
  ),
  'application/vnd.immervision-ivu' => 
  array (
    0 => 'ivu',
  ),
  'text/vnd.sun.j2me.app-descriptor' => 
  array (
    0 => 'jad',
  ),
  'application/vnd.jam' => 
  array (
    0 => 'jam',
  ),
  'application/x-jam' => 
  array (
    0 => 'jam',
  ),
  'application/java-archive' => 
  array (
    0 => 'jar',
    1 => 'war',
    2 => 'ear',
  ),
  'text/x-java-source,java' => 
  array (
    0 => 'java',
  ),
  'text/x-java-source' => 
  array (
    0 => 'java',
  ),
  'text/x-java' => 
  array (
    0 => 'java',
  ),
  'application/vnd.jisp' => 
  array (
    0 => 'jisp',
  ),
  'application/vnd.hp-jlyt' => 
  array (
    0 => 'jlt',
  ),
  'application/x-java-jnlp-file' => 
  array (
    0 => 'jnlp',
  ),
  'application/vnd.joost.joda-archive' => 
  array (
    0 => 'joda',
  ),
  'image/jpeg' => 
  array (
    0 => 'jpeg',
    1 => 'jpg',
    2 => 'jpe',
  ),
  'video/jpeg' => 
  array (
    0 => 'jpgv',
  ),
  'video/jpm' => 
  array (
    0 => 'jpm',
    1 => 'jpgm',
  ),
  'image/jpm' => 
  array (
    0 => 'jpm',
  ),
  'application/javascript' => 
  array (
    0 => 'js',
  ),
  'application/json' => 
  array (
    0 => 'json',
    1 => 'jsond',
  ),
  'application/vnd.kde.karbon' => 
  array (
    0 => 'karbon',
  ),
  'application/vnd.kde.kformula' => 
  array (
    0 => 'kfo',
  ),
  'application/vnd.kidspiration' => 
  array (
    0 => 'kia',
  ),
  'application/vnd.google-earth.kml+xml' => 
  array (
    0 => 'kml',
  ),
  'application/vnd.google-earth.kmz' => 
  array (
    0 => 'kmz',
  ),
  'application/vnd.kinar' => 
  array (
    0 => 'kne',
    1 => 'knp',
  ),
  'application/vnd.kde.kontour' => 
  array (
    0 => 'kon',
  ),
  'application/vnd.kde.kpresenter' => 
  array (
    0 => 'kpr',
    1 => 'kpt',
  ),
  'application/x-kpresenter' => 
  array (
    0 => 'kpr',
    1 => 'kpt',
  ),
  'application/vnd.kde.kspread' => 
  array (
    0 => 'ksp',
  ),
  'application/x-kspread' => 
  array (
    0 => 'ksp',
  ),
  'image/ktx' => 
  array (
    0 => 'ktx',
  ),
  'application/vnd.kahootz' => 
  array (
    0 => 'ktz',
    1 => 'ktr',
  ),
  'application/vnd.kde.kword' => 
  array (
    0 => 'kwd',
    1 => 'kwt',
  ),
  'application/x-kword' => 
  array (
    0 => 'kwd',
    1 => 'kwt',
  ),
  'application/vnd.las.las+xml' => 
  array (
    0 => 'lasxml',
  ),
  'application/x-latex' => 
  array (
    0 => 'latex',
  ),
  'application/vnd.llamagraphics.life-balance.desktop' => 
  array (
    0 => 'lbd',
  ),
  'application/vnd.llamagraphics.life-balance.exchange+xml' => 
  array (
    0 => 'lbe',
  ),
  'application/vnd.hhe.lesson-player' => 
  array (
    0 => 'les',
  ),
  'application/vnd.route66.link66+xml' => 
  array (
    0 => 'link66',
  ),
  'application/vnd.ms-lrm' => 
  array (
    0 => 'lrm',
  ),
  'application/vnd.frogans.ltf' => 
  array (
    0 => 'ltf',
  ),
  'audio/vnd.lucent.voice' => 
  array (
    0 => 'lvp',
  ),
  'application/vnd.lotus-wordpro' => 
  array (
    0 => 'lwp',
  ),
  'application/mp21' => 
  array (
    0 => 'm21',
    1 => 'mp21',
  ),
  'audio/x-mpegurl' => 
  array (
    0 => 'm3u',
  ),
  'application/vnd.apple.mpegurl' => 
  array (
    0 => 'm3u8',
  ),
  'video/x-m4v' => 
  array (
    0 => 'm4v',
  ),
  'application/mathematica' => 
  array (
    0 => 'ma',
    1 => 'nbp',
    2 => 'nb',
    3 => 'mb',
  ),
  'application/mads+xml' => 
  array (
    0 => 'mads',
  ),
  'application/vnd.ecowin.chart' => 
  array (
    0 => 'mag',
  ),
  'application/mathml+xml' => 
  array (
    0 => 'mathml',
  ),
  'application/vnd.mobius.mbk' => 
  array (
    0 => 'mbk',
  ),
  'application/mbox' => 
  array (
    0 => 'mbox',
  ),
  'application/vnd.medcalcdata' => 
  array (
    0 => 'mc1',
  ),
  'application/vnd.mcd' => 
  array (
    0 => 'mcd',
  ),
  'text/vnd.curl.mcurl' => 
  array (
    0 => 'mcurl',
  ),
  'application/x-msaccess' => 
  array (
    0 => 'mdb',
  ),
  'application/msaccess' => 
  array (
    0 => 'mdb',
  ),
  'image/vnd.ms-modi' => 
  array (
    0 => 'mdi',
  ),
  'application/metalink4+xml' => 
  array (
    0 => 'meta4',
  ),
  'application/mets+xml' => 
  array (
    0 => 'mets',
  ),
  'application/vnd.mfmp' => 
  array (
    0 => 'mfm',
  ),
  'application/vnd.osgeo.mapguide.package' => 
  array (
    0 => 'mgp',
  ),
  'application/vnd.proteus.magazine' => 
  array (
    0 => 'mgz',
  ),
  'audio/midi' => 
  array (
    0 => 'mid',
    1 => 'midi',
    2 => 'kar',
    3 => 'rmi',
  ),
  'application/vnd.mif' => 
  array (
    0 => 'mif',
  ),
  'application/x-mif' => 
  array (
    0 => 'mif',
  ),
  'video/mj2' => 
  array (
    0 => 'mj2',
    1 => 'mjp2',
  ),
  'application/vnd.dolby.mlp' => 
  array (
    0 => 'mlp',
  ),
  'application/vnd.chipnuts.karaoke-mmd' => 
  array (
    0 => 'mmd',
  ),
  'chemical/x-macromodel-input' => 
  array (
    0 => 'mmd',
    1 => 'mmod',
  ),
  'application/vnd.smaf' => 
  array (
    0 => 'mmf',
  ),
  'image/vnd.fujixerox.edmics-mmr' => 
  array (
    0 => 'mmr',
  ),
  'application/x-msmoney' => 
  array (
    0 => 'mny',
  ),
  'application/mods+xml' => 
  array (
    0 => 'mods',
  ),
  'video/x-sgi-movie' => 
  array (
    0 => 'movie',
  ),
  'video/mp4' => 
  array (
    0 => 'mp4',
    1 => 'mp4v',
    2 => 'mpg4',
  ),
  'audio/mp4' => 
  array (
    0 => 'mp4a',
    1 => 'm4a',
  ),
  'application/vnd.mophun.certificate' => 
  array (
    0 => 'mpc',
  ),
  'chemical/x-mopac-input' => 
  array (
    0 => 'mpc',
    1 => 'mop',
    2 => 'mopcrt',
    3 => 'zmt',
  ),
  'video/mpeg' => 
  array (
    0 => 'mpeg',
    1 => 'mpg',
    2 => 'mpe',
    3 => 'm1v',
    4 => 'm2v',
  ),
  'audio/mpeg' => 
  array (
    0 => 'mpga',
    1 => 'mp3',
    2 => 'mp2',
    3 => 'mp2a',
    4 => 'm2a',
    5 => 'm3a',
  ),
  'application/vnd.apple.installer+xml' => 
  array (
    0 => 'mpkg',
  ),
  'application/vnd.blueice.multipass' => 
  array (
    0 => 'mpm',
  ),
  'application/vnd.mophun.application' => 
  array (
    0 => 'mpn',
  ),
  'application/vnd.ms-project' => 
  array (
    0 => 'mpp',
    1 => 'mpt',
  ),
  'application/vnd.ibm.minipay' => 
  array (
    0 => 'mpy',
  ),
  'application/vnd.mobius.mqy' => 
  array (
    0 => 'mqy',
  ),
  'application/marc' => 
  array (
    0 => 'mrc',
  ),
  'application/marcxml+xml' => 
  array (
    0 => 'mrcx',
  ),
  'application/mediaservercontrol+xml' => 
  array (
    0 => 'mscml',
  ),
  'application/vnd.mseq' => 
  array (
    0 => 'mseq',
  ),
  'application/vnd.epson.msf' => 
  array (
    0 => 'msf',
  ),
  'model/mesh' => 
  array (
    0 => 'msh',
    1 => 'mesh',
    2 => 'silo',
  ),
  'application/vnd.mobius.msl' => 
  array (
    0 => 'msl',
  ),
  'application/vnd.muvee.style' => 
  array (
    0 => 'msty',
  ),
  'model/vnd.mts' => 
  array (
    0 => 'mts',
  ),
  'application/vnd.musician' => 
  array (
    0 => 'mus',
  ),
  'application/vnd.recordare.musicxml+xml' => 
  array (
    0 => 'musicxml',
  ),
  'application/x-msmediaview' => 
  array (
    0 => 'mvb',
    1 => 'm13',
    2 => 'm14',
  ),
  'chemical/x-mopac-vib' => 
  array (
    0 => 'mvb',
  ),
  'application/vnd.mfer' => 
  array (
    0 => 'mwf',
  ),
  'application/mxf' => 
  array (
    0 => 'mxf',
  ),
  'application/vnd.recordare.musicxml' => 
  array (
    0 => 'mxl',
  ),
  'application/xv+xml' => 
  array (
    0 => 'mxml',
    1 => 'xhvml',
    2 => 'xvml',
    3 => 'xvm',
  ),
  'application/vnd.triscape.mxs' => 
  array (
    0 => 'mxs',
  ),
  'video/vnd.mpegurl' => 
  array (
    0 => 'mxu',
    1 => 'm4u',
  ),
  'application/vnd.nokia.n-gage.symbian.install' => 
  array (
    0 => 'n-gage',
  ),
  'text/n3' => 
  array (
    0 => 'n3',
  ),
  'application/vnd.wolfram.player' => 
  array (
    0 => 'nbp',
  ),
  'application/x-netcdf' => 
  array (
    0 => 'nc',
    1 => 'cdf',
  ),
  'application/x-dtbncx+xml' => 
  array (
    0 => 'ncx',
  ),
  'application/vnd.nokia.n-gage.data' => 
  array (
    0 => 'ngdat',
  ),
  'application/vnd.neurolanguage.nlu' => 
  array (
    0 => 'nlu',
  ),
  'application/vnd.enliven' => 
  array (
    0 => 'nml',
  ),
  'application/vnd.noblenet-directory' => 
  array (
    0 => 'nnd',
  ),
  'application/vnd.noblenet-sealer' => 
  array (
    0 => 'nns',
  ),
  'application/vnd.noblenet-web' => 
  array (
    0 => 'nnw',
  ),
  'image/vnd.net-fpx' => 
  array (
    0 => 'npx',
  ),
  'application/vnd.lotus-notes' => 
  array (
    0 => 'nsf',
  ),
  'application/vnd.fujitsu.oasys2' => 
  array (
    0 => 'oa2',
  ),
  'application/vnd.fujitsu.oasys3' => 
  array (
    0 => 'oa3',
  ),
  'application/vnd.fujitsu.oasys' => 
  array (
    0 => 'oas',
  ),
  'application/x-msbinder' => 
  array (
    0 => 'obd',
  ),
  'application/oda' => 
  array (
    0 => 'oda',
  ),
  'application/vnd.oasis.opendocument.database' => 
  array (
    0 => 'odb',
  ),
  'application/vnd.oasis.opendocument.chart' => 
  array (
    0 => 'odc',
  ),
  'application/vnd.oasis.opendocument.formula' => 
  array (
    0 => 'odf',
  ),
  'application/vnd.oasis.opendocument.formula-template' => 
  array (
    0 => 'odft',
  ),
  'application/vnd.oasis.opendocument.graphics' => 
  array (
    0 => 'odg',
  ),
  'application/vnd.oasis.opendocument.image' => 
  array (
    0 => 'odi',
  ),
  'application/vnd.oasis.opendocument.text-master' => 
  array (
    0 => 'odm',
  ),
  'application/vnd.oasis.opendocument.presentation' => 
  array (
    0 => 'odp',
  ),
  'application/vnd.oasis.opendocument.spreadsheet' => 
  array (
    0 => 'ods',
  ),
  'application/vnd.oasis.opendocument.text' => 
  array (
    0 => 'odt',
  ),
  'audio/ogg' => 
  array (
    0 => 'oga',
    1 => 'ogg',
    2 => 'spx',
    3 => 'opus',
  ),
  'video/ogg' => 
  array (
    0 => 'ogv',
  ),
  'application/ogg' => 
  array (
    0 => 'ogx',
  ),
  'application/onenote' => 
  array (
    0 => 'onetoc',
    1 => 'onetoc2',
    2 => 'onetmp',
    3 => 'onepkg',
  ),
  'application/oebps-package+xml' => 
  array (
    0 => 'opf',
  ),
  'application/vnd.lotus-organizer' => 
  array (
    0 => 'org',
  ),
  'application/vnd.yamaha.openscoreformat' => 
  array (
    0 => 'osf',
  ),
  'application/vnd.yamaha.openscoreformat.osfpvg+xml' => 
  array (
    0 => 'osfpvg',
  ),
  'application/vnd.oasis.opendocument.chart-template' => 
  array (
    0 => 'otc',
  ),
  'application/x-font-otf' => 
  array (
    0 => 'otf',
  ),
  'application/font-sfnt' => 
  array (
    0 => 'otf',
    1 => 'ttf',
  ),
  'application/vnd.oasis.opendocument.graphics-template' => 
  array (
    0 => 'otg',
  ),
  'application/vnd.oasis.opendocument.text-web' => 
  array (
    0 => 'oth',
  ),
  'application/vnd.oasis.opendocument.image-template' => 
  array (
    0 => 'oti',
  ),
  'application/vnd.oasis.opendocument.presentation-template' => 
  array (
    0 => 'otp',
  ),
  'application/vnd.oasis.opendocument.spreadsheet-template' => 
  array (
    0 => 'ots',
  ),
  'application/vnd.oasis.opendocument.text-template' => 
  array (
    0 => 'ott',
  ),
  'application/vnd.openofficeorg.extension' => 
  array (
    0 => 'oxt',
  ),
  'text/x-pascal' => 
  array (
    0 => 'p',
    1 => 'pas',
  ),
  'application/pkcs10' => 
  array (
    0 => 'p10',
  ),
  'application/x-pkcs12' => 
  array (
    0 => 'p12',
    1 => 'pfx',
  ),
  'application/x-pkcs7-certificates' => 
  array (
    0 => 'p7b',
    1 => 'spc',
  ),
  'application/pkcs7-mime' => 
  array (
    0 => 'p7m',
    1 => 'p7c',
  ),
  'application/x-pkcs7-certreqresp' => 
  array (
    0 => 'p7r',
  ),
  'application/pkcs7-signature' => 
  array (
    0 => 'p7s',
  ),
  'application/pkcs8' => 
  array (
    0 => 'p8',
  ),
  'text/plain-bas' => 
  array (
    0 => 'par',
  ),
  'application/vnd.pawaafile' => 
  array (
    0 => 'paw',
  ),
  'application/vnd.powerbuilder6' => 
  array (
    0 => 'pbd',
  ),
  'image/x-portable-bitmap' => 
  array (
    0 => 'pbm',
  ),
  'application/x-font-pcf' => 
  array (
    0 => 'pcf',
  ),
  'application/vnd.hp-pcl' => 
  array (
    0 => 'pcl',
  ),
  'application/vnd.hp-pclxl' => 
  array (
    0 => 'pclxl',
  ),
  'application/vnd.curl.pcurl' => 
  array (
    0 => 'pcurl',
  ),
  'image/x-pcx' => 
  array (
    0 => 'pcx',
  ),
  'image/pcx' => 
  array (
    0 => 'pcx',
  ),
  'application/vnd.palm' => 
  array (
    0 => 'pdb',
    1 => 'pqa',
    2 => 'oprc',
  ),
  'chemical/x-pdb' => 
  array (
    0 => 'pdb',
    1 => 'ent',
  ),
  'application/x-pilot' => 
  array (
    0 => 'pdb',
    1 => 'prc',
  ),
  'application/pdf' => 
  array (
    0 => 'pdf',
  ),
  'application/x-font-type1' => 
  array (
    0 => 'pfa',
    1 => 'pfb',
    2 => 'pfm',
    3 => 'afm',
  ),
  'application/font-tdpfr' => 
  array (
    0 => 'pfr',
  ),
  'image/x-portable-graymap' => 
  array (
    0 => 'pgm',
  ),
  'application/x-chess-pgn' => 
  array (
    0 => 'pgn',
  ),
  'application/pgp-signature' => 
  array (
    0 => 'pgp',
    1 => 'asc',
    2 => 'sig',
  ),
  'application/pgp-encrypted' => 
  array (
    0 => 'pgp',
  ),
  'image/x-pict' => 
  array (
    0 => 'pic',
    1 => 'pct',
  ),
  'application/pkixcmp' => 
  array (
    0 => 'pki',
  ),
  'application/pkix-pkipath' => 
  array (
    0 => 'pkipath',
  ),
  'application/vnd.3gpp.pic-bw-large' => 
  array (
    0 => 'plb',
  ),
  'application/vnd.mobius.plc' => 
  array (
    0 => 'plc',
  ),
  'application/vnd.pocketlearn' => 
  array (
    0 => 'plf',
  ),
  'application/pls+xml' => 
  array (
    0 => 'pls',
  ),
  'audio/x-scpls' => 
  array (
    0 => 'pls',
  ),
  'application/vnd.ctc-posml' => 
  array (
    0 => 'pml',
  ),
  'image/png' => 
  array (
    0 => 'png',
  ),
  'image/x-portable-anymap' => 
  array (
    0 => 'pnm',
  ),
  'application/vnd.macports.portpkg' => 
  array (
    0 => 'portpkg',
  ),
  'application/vnd.ms-powerpoint.template.macroenabled.12' => 
  array (
    0 => 'potm',
  ),
  'application/vnd.openxmlformats-officedocument.presentationml.template' => 
  array (
    0 => 'potx',
  ),
  'application/vnd.ms-powerpoint.addin.macroenabled.12' => 
  array (
    0 => 'ppam',
  ),
  'application/vnd.cups-ppd' => 
  array (
    0 => 'ppd',
  ),
  'image/x-portable-pixmap' => 
  array (
    0 => 'ppm',
  ),
  'application/vnd.ms-powerpoint.slideshow.macroenabled.12' => 
  array (
    0 => 'ppsm',
  ),
  'application/vnd.openxmlformats-officedocument.presentationml.slideshow' => 
  array (
    0 => 'ppsx',
  ),
  'application/vnd.ms-powerpoint' => 
  array (
    0 => 'ppt',
    1 => 'pps',
    2 => 'pot',
  ),
  'application/vnd.ms-powerpoint.presentation.macroenabled.12' => 
  array (
    0 => 'pptm',
  ),
  'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 
  array (
    0 => 'pptx',
  ),
  'application/x-mobipocket-ebook' => 
  array (
    0 => 'prc',
    1 => 'mobi',
  ),
  'application/vnd.lotus-freelance' => 
  array (
    0 => 'pre',
  ),
  'application/pics-rules' => 
  array (
    0 => 'prf',
  ),
  'application/vnd.3gpp.pic-bw-small' => 
  array (
    0 => 'psb',
  ),
  'image/vnd.adobe.photoshop' => 
  array (
    0 => 'psd',
  ),
  'image/x-photoshop' => 
  array (
    0 => 'psd',
  ),
  'application/x-font-linux-psf' => 
  array (
    0 => 'psf',
  ),
  'application/pskc+xml' => 
  array (
    0 => 'pskcxml',
  ),
  'application/vnd.pvi.ptid1' => 
  array (
    0 => 'ptid',
  ),
  'application/x-mspublisher' => 
  array (
    0 => 'pub',
  ),
  'application/vnd.3gpp.pic-bw-var' => 
  array (
    0 => 'pvb',
  ),
  'application/vnd.3m.post-it-notes' => 
  array (
    0 => 'pwn',
  ),
  'audio/vnd.ms-playready.media.pya' => 
  array (
    0 => 'pya',
  ),
  'video/vnd.ms-playready.media.pyv' => 
  array (
    0 => 'pyv',
  ),
  'application/vnd.epson.quickanime' => 
  array (
    0 => 'qam',
  ),
  'application/vnd.intu.qbo' => 
  array (
    0 => 'qbo',
  ),
  'application/vnd.intu.qfx' => 
  array (
    0 => 'qfx',
  ),
  'application/vnd.publishare-delta-tree' => 
  array (
    0 => 'qps',
  ),
  'video/quicktime' => 
  array (
    0 => 'qt',
    1 => 'mov',
  ),
  'application/vnd.quark.quarkxpress' => 
  array (
    0 => 'qxd',
    1 => 'qxt',
    2 => 'qwd',
    3 => 'qwt',
    4 => 'qxl',
    5 => 'qxb',
  ),
  'audio/x-pn-realaudio' => 
  array (
    0 => 'ram',
    1 => 'rm',
    2 => 'ra',
  ),
  'application/x-rar-compressed' => 
  array (
    0 => 'rar',
  ),
  'application/rar' => 
  array (
    0 => 'rar',
  ),
  'image/x-cmu-raster' => 
  array (
    0 => 'ras',
  ),
  'application/vnd.ipunplugged.rcprofile' => 
  array (
    0 => 'rcprofile',
  ),
  'application/rdf+xml' => 
  array (
    0 => 'rdf',
  ),
  'application/vnd.data-vision.rdz' => 
  array (
    0 => 'rdz',
  ),
  'application/vnd.businessobjects' => 
  array (
    0 => 'rep',
  ),
  'application/x-dtbresource+xml' => 
  array (
    0 => 'res',
  ),
  'image/x-rgb' => 
  array (
    0 => 'rgb',
  ),
  'application/reginfo+xml' => 
  array (
    0 => 'rif',
  ),
  'audio/vnd.rip' => 
  array (
    0 => 'rip',
  ),
  'application/resource-lists+xml' => 
  array (
    0 => 'rl',
  ),
  'image/vnd.fujixerox.edmics-rlc' => 
  array (
    0 => 'rlc',
  ),
  'application/resource-lists-diff+xml' => 
  array (
    0 => 'rld',
  ),
  'application/vnd.rn-realmedia' => 
  array (
    0 => 'rm',
  ),
  'audio/x-pn-realaudio-plugin' => 
  array (
    0 => 'rmp',
  ),
  'application/vnd.jcp.javame.midlet-rms' => 
  array (
    0 => 'rms',
  ),
  'application/relax-ng-compact-syntax' => 
  array (
    0 => 'rnc',
  ),
  'application/vnd.cloanto.rp9' => 
  array (
    0 => 'rp9',
  ),
  'application/vnd.nokia.radio-presets' => 
  array (
    0 => 'rpss',
  ),
  'application/vnd.nokia.radio-preset' => 
  array (
    0 => 'rpst',
  ),
  'application/sparql-query' => 
  array (
    0 => 'rq',
  ),
  'application/rls-services+xml' => 
  array (
    0 => 'rs',
  ),
  'application/rsd+xml' => 
  array (
    0 => 'rsd',
  ),
  'application/rss+xml' => 
  array (
    0 => 'rss',
  ),
  'text/richtext' => 
  array (
    0 => 'rtx',
  ),
  'text/x-asm' => 
  array (
    0 => 's',
    1 => 'asm',
  ),
  'application/vnd.yamaha.smaf-audio' => 
  array (
    0 => 'saf',
  ),
  'application/sbml+xml' => 
  array (
    0 => 'sbml',
  ),
  'application/vnd.ibm.secure-container' => 
  array (
    0 => 'sc',
  ),
  'application/x-msschedule' => 
  array (
    0 => 'scd',
  ),
  'application/vnd.lotus-screencam' => 
  array (
    0 => 'scm',
  ),
  'application/scvp-cv-request' => 
  array (
    0 => 'scq',
  ),
  'application/scvp-cv-response' => 
  array (
    0 => 'scs',
  ),
  'text/vnd.curl.scurl' => 
  array (
    0 => 'scurl',
  ),
  'application/vnd.stardivision.draw' => 
  array (
    0 => 'sda',
  ),
  'application/vnd.stardivision.calc' => 
  array (
    0 => 'sdc',
  ),
  'application/vnd.stardivision.impress' => 
  array (
    0 => 'sdd',
  ),
  'application/vnd.solent.sdkm+xml' => 
  array (
    0 => 'sdkm',
    1 => 'sdkd',
  ),
  'application/sdp' => 
  array (
    0 => 'sdp',
  ),
  'application/vnd.stardivision.writer' => 
  array (
    0 => 'sdw',
    1 => 'vor',
  ),
  'application/vnd.seemail' => 
  array (
    0 => 'see',
  ),
  'application/vnd.fdsn.seed' => 
  array (
    0 => 'seed',
    1 => 'dataless',
  ),
  'application/vnd.sema' => 
  array (
    0 => 'sema',
  ),
  'application/vnd.semd' => 
  array (
    0 => 'semd',
  ),
  'application/vnd.semf' => 
  array (
    0 => 'semf',
  ),
  'application/java-serialized-object' => 
  array (
    0 => 'ser',
  ),
  'application/set-payment-initiation' => 
  array (
    0 => 'setpay',
  ),
  'application/set-registration-initiation' => 
  array (
    0 => 'setreg',
  ),
  'application/vnd.hydrostatix.sof-data' => 
  array (
    0 => 'sfd-hdstx',
  ),
  'application/vnd.spotfire.sfs' => 
  array (
    0 => 'sfs',
  ),
  'application/vnd.stardivision.writer-global' => 
  array (
    0 => 'sgl',
  ),
  'text/sgml' => 
  array (
    0 => 'sgml',
    1 => 'sgm',
  ),
  'application/x-sh' => 
  array (
    0 => 'sh',
  ),
  'text/x-sh' => 
  array (
    0 => 'sh',
  ),
  'application/x-shar' => 
  array (
    0 => 'shar',
  ),
  'application/shf+xml' => 
  array (
    0 => 'shf',
  ),
  'application/vnd.symbian.install' => 
  array (
    0 => 'sis',
    1 => 'sisx',
  ),
  'application/x-stuffit' => 
  array (
    0 => 'sit',
    1 => 'sitx',
  ),
  'application/x-stuffitx' => 
  array (
    0 => 'sitx',
  ),
  'application/vnd.koan' => 
  array (
    0 => 'skp',
    1 => 'skd',
    2 => 'skt',
    3 => 'skm',
  ),
  'application/x-koan' => 
  array (
    0 => 'skp',
    1 => 'skd',
    2 => 'skt',
    3 => 'skm',
  ),
  'application/vnd.ms-powerpoint.slide.macroenabled.12' => 
  array (
    0 => 'sldm',
  ),
  'application/vnd.openxmlformats-officedocument.presentationml.slide' => 
  array (
    0 => 'sldx',
  ),
  'application/vnd.epson.salt' => 
  array (
    0 => 'slt',
  ),
  'application/vnd.stepmania.stepchart' => 
  array (
    0 => 'sm',
  ),
  'application/vnd.stardivision.math' => 
  array (
    0 => 'smf',
    1 => 'sdf',
  ),
  'application/smil+xml' => 
  array (
    0 => 'smi',
    1 => 'smil',
  ),
  'application/x-font-snf' => 
  array (
    0 => 'snf',
  ),
  'application/vnd.yamaha.smaf-phrase' => 
  array (
    0 => 'spf',
  ),
  'application/x-futuresplash' => 
  array (
    0 => 'spl',
  ),
  'application/futuresplash' => 
  array (
    0 => 'spl',
  ),
  'text/vnd.in3d.spot' => 
  array (
    0 => 'spot',
  ),
  'application/scvp-vp-response' => 
  array (
    0 => 'spp',
  ),
  'application/scvp-vp-request' => 
  array (
    0 => 'spq',
  ),
  'application/x-wais-source' => 
  array (
    0 => 'src',
  ),
  'application/sru+xml' => 
  array (
    0 => 'sru',
  ),
  'application/sparql-results+xml' => 
  array (
    0 => 'srx',
  ),
  'application/vnd.kodak-descriptor' => 
  array (
    0 => 'sse',
  ),
  'application/vnd.epson.ssf' => 
  array (
    0 => 'ssf',
  ),
  'application/ssml+xml' => 
  array (
    0 => 'ssml',
  ),
  'application/vnd.sailingtracker.track' => 
  array (
    0 => 'st',
  ),
  'application/vnd.sun.xml.calc.template' => 
  array (
    0 => 'stc',
  ),
  'application/vnd.sun.xml.draw.template' => 
  array (
    0 => 'std',
  ),
  'application/vnd.wt.stf' => 
  array (
    0 => 'stf',
  ),
  'application/vnd.sun.xml.impress.template' => 
  array (
    0 => 'sti',
  ),
  'application/hyperstudio' => 
  array (
    0 => 'stk',
  ),
  'application/vnd.ms-pki.stl' => 
  array (
    0 => 'stl',
  ),
  'application/sla' => 
  array (
    0 => 'stl',
  ),
  'application/vnd.pg.format' => 
  array (
    0 => 'str',
  ),
  'application/vnd.sun.xml.writer.template' => 
  array (
    0 => 'stw',
  ),
  'image/vnd.dvb.subtitle' => 
  array (
    0 => 'sub',
  ),
  'text/vnd.dvb.subtitle' => 
  array (
    0 => 'sub',
  ),
  'application/vnd.sus-calendar' => 
  array (
    0 => 'sus',
    1 => 'susp',
  ),
  'application/x-sv4cpio' => 
  array (
    0 => 'sv4cpio',
  ),
  'application/x-sv4crc' => 
  array (
    0 => 'sv4crc',
  ),
  'application/vnd.dvb.service' => 
  array (
    0 => 'svc',
  ),
  'application/vnd.svd' => 
  array (
    0 => 'svd',
  ),
  'image/svg+xml' => 
  array (
    0 => 'svg',
    1 => 'svgz',
  ),
  'application/x-shockwave-flash' => 
  array (
    0 => 'swf',
    1 => 'swfl',
  ),
  'application/vnd.aristanetworks.swi' => 
  array (
    0 => 'swi',
  ),
  'application/vnd.sun.xml.calc' => 
  array (
    0 => 'sxc',
  ),
  'application/vnd.sun.xml.draw' => 
  array (
    0 => 'sxd',
  ),
  'application/vnd.sun.xml.writer.global' => 
  array (
    0 => 'sxg',
  ),
  'application/vnd.sun.xml.impress' => 
  array (
    0 => 'sxi',
  ),
  'application/vnd.sun.xml.math' => 
  array (
    0 => 'sxm',
  ),
  'application/vnd.sun.xml.writer' => 
  array (
    0 => 'sxw',
  ),
  'text/troff' => 
  array (
    0 => 't',
    1 => 'tr',
    2 => 'roff',
    3 => 'man',
    4 => 'me',
    5 => 'ms',
  ),
  'application/x-troff' => 
  array (
    0 => 't',
    1 => 'tr',
    2 => 'roff',
  ),
  'application/vnd.tao.intent-module-archive' => 
  array (
    0 => 'tao',
  ),
  'application/x-tar' => 
  array (
    0 => 'tar',
  ),
  'application/vnd.3gpp2.tcap' => 
  array (
    0 => 'tcap',
  ),
  'application/x-tcl' => 
  array (
    0 => 'tcl',
    1 => 'tk',
  ),
  'text/x-tcl' => 
  array (
    0 => 'tcl',
    1 => 'tk',
  ),
  'application/vnd.smart.teacher' => 
  array (
    0 => 'teacher',
  ),
  'application/tei+xml' => 
  array (
    0 => 'tei',
    1 => 'teicorpus',
  ),
  'application/x-tex' => 
  array (
    0 => 'tex',
  ),
  'text/x-tex' => 
  array (
    0 => 'tex',
    1 => 'ltx',
    2 => 'sty',
    3 => 'cls',
  ),
  'application/x-texinfo' => 
  array (
    0 => 'texinfo',
    1 => 'texi',
  ),
  'application/thraud+xml' => 
  array (
    0 => 'tfi',
  ),
  'application/x-tex-tfm' => 
  array (
    0 => 'tfm',
  ),
  'application/vnd.ms-officetheme' => 
  array (
    0 => 'thmx',
  ),
  'image/tiff' => 
  array (
    0 => 'tiff',
    1 => 'tif',
  ),
  'application/vnd.tmobile-livetv' => 
  array (
    0 => 'tmo',
  ),
  'application/x-bittorrent' => 
  array (
    0 => 'torrent',
  ),
  'application/vnd.groove-tool-template' => 
  array (
    0 => 'tpl',
  ),
  'application/vnd.trid.tpt' => 
  array (
    0 => 'tpt',
  ),
  'application/vnd.trueapp' => 
  array (
    0 => 'tra',
  ),
  'application/x-msterminal' => 
  array (
    0 => 'trm',
  ),
  'application/timestamped-data' => 
  array (
    0 => 'tsd',
  ),
  'text/tab-separated-values' => 
  array (
    0 => 'tsv',
  ),
  'application/x-font-ttf' => 
  array (
    0 => 'ttf',
    1 => 'ttc',
  ),
  'text/turtle' => 
  array (
    0 => 'ttl',
  ),
  'application/vnd.simtech-mindmapper' => 
  array (
    0 => 'twd',
    1 => 'twds',
  ),
  'application/vnd.genomatix.tuxedo' => 
  array (
    0 => 'txd',
  ),
  'application/vnd.mobius.txf' => 
  array (
    0 => 'txf',
  ),
  'text/plain' => 
  array (
    0 => 'txt',
    1 => 'asc',
    2 => 'pot',
    3 => 'srt',
    4 => 'text',
    5 => 'conf',
    6 => 'def',
    7 => 'list',
    8 => 'log',
    9 => 'in',
    10 => 'brf',
  ),
  'application/vnd.ufdl' => 
  array (
    0 => 'ufd',
    1 => 'ufdl',
  ),
  'application/vnd.umajin' => 
  array (
    0 => 'umj',
  ),
  'application/vnd.unity' => 
  array (
    0 => 'unityweb',
  ),
  'application/vnd.uoml+xml' => 
  array (
    0 => 'uoml',
  ),
  'text/uri-list' => 
  array (
    0 => 'uri',
    1 => 'uris',
    2 => 'urls',
  ),
  'application/x-ustar' => 
  array (
    0 => 'ustar',
  ),
  'application/vnd.uiq.theme' => 
  array (
    0 => 'utz',
  ),
  'text/x-uuencode' => 
  array (
    0 => 'uu',
  ),
  'audio/vnd.dece.audio' => 
  array (
    0 => 'uva',
    1 => 'uvva',
  ),
  'video/vnd.dece.hd' => 
  array (
    0 => 'uvh',
    1 => 'uvvh',
  ),
  'image/vnd.dece.graphic' => 
  array (
    0 => 'uvi',
    1 => 'uvvi',
    2 => 'uvg',
    3 => 'uvvg',
  ),
  'video/vnd.dece.mobile' => 
  array (
    0 => 'uvm',
    1 => 'uvvm',
  ),
  'video/vnd.dece.pd' => 
  array (
    0 => 'uvp',
    1 => 'uvvp',
  ),
  'video/vnd.dece.sd' => 
  array (
    0 => 'uvs',
    1 => 'uvvs',
  ),
  'video/vnd.uvvu.mp4' => 
  array (
    0 => 'uvu',
    1 => 'uvvu',
  ),
  'video/vnd.dece.video' => 
  array (
    0 => 'uvv',
    1 => 'uvvv',
  ),
  'application/x-cdlink' => 
  array (
    0 => 'vcd',
  ),
  'text/x-vcard' => 
  array (
    0 => 'vcf',
  ),
  'text/vcard' => 
  array (
    0 => 'vcf',
    1 => 'vcard',
  ),
  'application/vnd.groove-vcard' => 
  array (
    0 => 'vcg',
  ),
  'text/x-vcalendar' => 
  array (
    0 => 'vcs',
  ),
  'application/vnd.vcx' => 
  array (
    0 => 'vcx',
  ),
  'application/vnd.visionary' => 
  array (
    0 => 'vis',
  ),
  'video/vnd.vivo' => 
  array (
    0 => 'viv',
  ),
  'application/vnd.visio' => 
  array (
    0 => 'vsd',
    1 => 'vst',
    2 => 'vss',
    3 => 'vsw',
  ),
  'application/vnd.vsf' => 
  array (
    0 => 'vsf',
  ),
  'model/vnd.vtu' => 
  array (
    0 => 'vtu',
  ),
  'application/voicexml+xml' => 
  array (
    0 => 'vxml',
  ),
  'application/x-doom' => 
  array (
    0 => 'wad',
  ),
  'audio/x-wav' => 
  array (
    0 => 'wav',
  ),
  'audio/x-ms-wax' => 
  array (
    0 => 'wax',
  ),
  'image/vnd.wap.wbmp' => 
  array (
    0 => 'wbmp',
  ),
  'application/vnd.criticaltools.wbs+xml' => 
  array (
    0 => 'wbs',
  ),
  'application/vnd.wap.wbxml' => 
  array (
    0 => 'wbxml',
  ),
  'audio/webm' => 
  array (
    0 => 'weba',
  ),
  'video/webm' => 
  array (
    0 => 'webm',
  ),
  'image/webp' => 
  array (
    0 => 'webp',
  ),
  'application/vnd.pmi.widget' => 
  array (
    0 => 'wg',
  ),
  'application/widget' => 
  array (
    0 => 'wgt',
  ),
  'video/x-ms-wm' => 
  array (
    0 => 'wm',
  ),
  'audio/x-ms-wma' => 
  array (
    0 => 'wma',
  ),
  'application/x-ms-wmd' => 
  array (
    0 => 'wmd',
  ),
  'application/x-msmetafile' => 
  array (
    0 => 'wmf',
    1 => 'wmz',
    2 => 'emf',
    3 => 'emz',
  ),
  'text/vnd.wap.wml' => 
  array (
    0 => 'wml',
  ),
  'application/vnd.wap.wmlc' => 
  array (
    0 => 'wmlc',
  ),
  'text/vnd.wap.wmlscript' => 
  array (
    0 => 'wmls',
  ),
  'application/vnd.wap.wmlscriptc' => 
  array (
    0 => 'wmlsc',
  ),
  'video/x-ms-wmv' => 
  array (
    0 => 'wmv',
  ),
  'video/x-ms-wmx' => 
  array (
    0 => 'wmx',
  ),
  'application/x-ms-wmz' => 
  array (
    0 => 'wmz',
  ),
  'application/x-font-woff' => 
  array (
    0 => 'woff',
  ),
  'application/font-woff' => 
  array (
    0 => 'woff',
  ),
  'application/x-font-woff2' => 
  array (
    0 => 'woff2',
  ),
  'application/vnd.wordperfect' => 
  array (
    0 => 'wpd',
  ),
  'application/vnd.ms-wpl' => 
  array (
    0 => 'wpl',
  ),
  'application/vnd.ms-works' => 
  array (
    0 => 'wps',
    1 => 'wks',
    2 => 'wcm',
    3 => 'wdb',
  ),
  'application/vnd.wqd' => 
  array (
    0 => 'wqd',
  ),
  'application/x-mswrite' => 
  array (
    0 => 'wri',
  ),
  'model/vrml' => 
  array (
    0 => 'wrl',
    1 => 'vrml',
  ),
  'application/wsdl+xml' => 
  array (
    0 => 'wsdl',
  ),
  'application/wspolicy+xml' => 
  array (
    0 => 'wspolicy',
  ),
  'application/vnd.webturbo' => 
  array (
    0 => 'wtb',
  ),
  'video/x-ms-wvx' => 
  array (
    0 => 'wvx',
  ),
  'application/vnd.hzn-3d-crossword' => 
  array (
    0 => 'x3d',
  ),
  'model/x3d+xml' => 
  array (
    0 => 'x3d',
    1 => 'x3dz',
  ),
  'application/x-silverlight-app' => 
  array (
    0 => 'xap',
  ),
  'application/vnd.xara' => 
  array (
    0 => 'xar',
  ),
  'application/x-ms-xbap' => 
  array (
    0 => 'xbap',
  ),
  'application/vnd.fujixerox.docuworks.binder' => 
  array (
    0 => 'xbd',
  ),
  'image/x-xbitmap' => 
  array (
    0 => 'xbm',
  ),
  'application/xcap-diff+xml' => 
  array (
    0 => 'xdf',
  ),
  'application/vnd.syncml.dm+xml' => 
  array (
    0 => 'xdm',
  ),
  'application/vnd.adobe.xdp+xml' => 
  array (
    0 => 'xdp',
  ),
  'application/dssc+xml' => 
  array (
    0 => 'xdssc',
  ),
  'application/vnd.fujixerox.docuworks' => 
  array (
    0 => 'xdw',
  ),
  'application/xenc+xml' => 
  array (
    0 => 'xenc',
  ),
  'application/patch-ops-error+xml' => 
  array (
    0 => 'xer',
  ),
  'application/vnd.adobe.xfdf' => 
  array (
    0 => 'xfdf',
  ),
  'application/vnd.xfdl' => 
  array (
    0 => 'xfdl',
  ),
  'application/xhtml+xml' => 
  array (
    0 => 'xhtml',
    1 => 'xht',
  ),
  'image/vnd.xiff' => 
  array (
    0 => 'xif',
  ),
  'application/vnd.ms-excel.addin.macroenabled.12' => 
  array (
    0 => 'xlam',
  ),
  'application/vnd.ms-excel' => 
  array (
    0 => 'xls',
    1 => 'xlm',
    2 => 'xla',
    3 => 'xlc',
    4 => 'xlt',
    5 => 'xlw',
    6 => 'xlb',
  ),
  'application/vnd.ms-excel.sheet.binary.macroenabled.12' => 
  array (
    0 => 'xlsb',
  ),
  'application/vnd.ms-excel.sheet.macroenabled.12' => 
  array (
    0 => 'xlsm',
  ),
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 
  array (
    0 => 'xlsx',
  ),
  'application/vnd.ms-excel.template.macroenabled.12' => 
  array (
    0 => 'xltm',
  ),
  'application/vnd.openxmlformats-officedocument.spreadsheetml.template' => 
  array (
    0 => 'xltx',
  ),
  'application/xml' => 
  array (
    0 => 'xml',
    1 => 'xsl',
    2 => 'xsd',
  ),
  'text/xml' => 
  array (
    0 => 'xml',
  ),
  'application/vnd.olpc-sugar' => 
  array (
    0 => 'xo',
  ),
  'application/xop+xml' => 
  array (
    0 => 'xop',
  ),
  'application/x-xpinstall' => 
  array (
    0 => 'xpi',
  ),
  'image/x-xpixmap' => 
  array (
    0 => 'xpm',
  ),
  'application/vnd.is-xpr' => 
  array (
    0 => 'xpr',
  ),
  'application/vnd.ms-xpsdocument' => 
  array (
    0 => 'xps',
  ),
  'application/vnd.intercon.formnet' => 
  array (
    0 => 'xpw',
    1 => 'xpx',
  ),
  'application/xslt+xml' => 
  array (
    0 => 'xslt',
  ),
  'application/vnd.syncml+xml' => 
  array (
    0 => 'xsm',
  ),
  'application/xspf+xml' => 
  array (
    0 => 'xspf',
  ),
  'application/vnd.mozilla.xul+xml' => 
  array (
    0 => 'xul',
  ),
  'image/x-xwindowdump' => 
  array (
    0 => 'xwd',
  ),
  'chemical/x-xyz' => 
  array (
    0 => 'xyz',
  ),
  'text/yaml' => 
  array (
    0 => 'yaml',
  ),
  'application/yang' => 
  array (
    0 => 'yang',
  ),
  'application/yin+xml' => 
  array (
    0 => 'yin',
  ),
  'application/vnd.zzazz.deck+xml' => 
  array (
    0 => 'zaz',
  ),
  'application/zip' => 
  array (
    0 => 'zip',
  ),
  'application/vnd.zul' => 
  array (
    0 => 'zir',
    1 => 'zirz',
  ),
  'application/andrew-inset' => 
  array (
    0 => 'ez',
  ),
  'application/docbook+xml' => 
  array (
    0 => 'dbk',
  ),
  'application/gml+xml' => 
  array (
    0 => 'gml',
  ),
  'application/gpx+xml' => 
  array (
    0 => 'gpx',
  ),
  'application/gxf' => 
  array (
    0 => 'gxf',
  ),
  'application/inkml+xml' => 
  array (
    0 => 'ink',
    1 => 'inkml',
  ),
  'application/jsonml+json' => 
  array (
    0 => 'jsonml',
  ),
  'application/lost+xml' => 
  array (
    0 => 'lostxml',
  ),
  'application/metalink+xml' => 
  array (
    0 => 'metalink',
  ),
  'application/mp4' => 
  array (
    0 => 'mp4s',
  ),
  'application/x-dms' => 
  array (
    0 => 'dms',
  ),
  'application/omdoc+xml' => 
  array (
    0 => 'omdoc',
  ),
  'application/oxps' => 
  array (
    0 => 'oxps',
  ),
  'application/rpki-ghostbusters' => 
  array (
    0 => 'gbr',
  ),
  'application/rpki-manifest' => 
  array (
    0 => 'mft',
  ),
  'application/rpki-roa' => 
  array (
    0 => 'roa',
  ),
  'application/ssdl+xml' => 
  array (
    0 => 'ssdl',
  ),
  'application/vnd.adobe.formscentral.fcdt' => 
  array (
    0 => 'fcdt',
  ),
  'application/vnd.astraea-software.iota' => 
  array (
    0 => 'iota',
  ),
  'application/vnd.dart' => 
  array (
    0 => 'dart',
  ),
  'application/vnd.dece.data' => 
  array (
    0 => 'uvf',
    1 => 'uvvf',
    2 => 'uvd',
    3 => 'uvvd',
  ),
  'application/vnd.dece.ttml+xml' => 
  array (
    0 => 'uvt',
    1 => 'uvvt',
  ),
  'application/vnd.dece.unspecified' => 
  array (
    0 => 'uvx',
    1 => 'uvvx',
  ),
  'application/vnd.dece.zip' => 
  array (
    0 => 'uvz',
    1 => 'uvvz',
  ),
  'application/vnd.ds-keypoint' => 
  array (
    0 => 'kpxx',
  ),
  'application/vnd.fdsn.mseed' => 
  array (
    0 => 'mseed',
  ),
  'application/vnd.handheld-entertainment+xml' => 
  array (
    0 => 'zmm',
  ),
  'application/vnd.mynfc' => 
  array (
    0 => 'taglet',
  ),
  'application/vnd.nitf' => 
  array (
    0 => 'ntf',
    1 => 'nitf',
  ),
  'application/vnd.osgi.subsystem' => 
  array (
    0 => 'esa',
  ),
  'application/vnd.rn-realmedia-vbr' => 
  array (
    0 => 'rmvb',
  ),
  'application/vnd.stepmania.package' => 
  array (
    0 => 'smzip',
  ),
  'application/vnd.tcpdump.pcap' => 
  array (
    0 => 'pcap',
    1 => 'cap',
    2 => 'dmp',
  ),
  'application/x-apple-diskimage' => 
  array (
    0 => 'dmg',
  ),
  'application/x-blorb' => 
  array (
    0 => 'blb',
    1 => 'blorb',
  ),
  'application/x-cbr' => 
  array (
    0 => 'cbr',
    1 => 'cba',
    2 => 'cbt',
    3 => 'cbz',
    4 => 'cb7',
  ),
  'application/x-cbz' => 
  array (
    0 => 'cbz',
  ),
  'application/x-cfs-compressed' => 
  array (
    0 => 'cfs',
  ),
  'application/x-conference' => 
  array (
    0 => 'nsc',
  ),
  'application/x-dgc-compressed' => 
  array (
    0 => 'dgc',
  ),
  'application/x-envoy' => 
  array (
    0 => 'evy',
  ),
  'application/x-eva' => 
  array (
    0 => 'eva',
  ),
  'application/x-freearc' => 
  array (
    0 => 'arc',
  ),
  'application/x-gca-compressed' => 
  array (
    0 => 'gca',
  ),
  'application/x-glulx' => 
  array (
    0 => 'ulx',
  ),
  'application/x-gramps-xml' => 
  array (
    0 => 'gramps',
  ),
  'application/x-install-instructions' => 
  array (
    0 => 'install',
  ),
  'application/x-iso9660-image' => 
  array (
    0 => 'iso',
  ),
  'application/x-lzh-compressed' => 
  array (
    0 => 'lzh',
    1 => 'lha',
  ),
  'application/x-lzh' => 
  array (
    0 => 'lzh',
  ),
  'application/x-lha' => 
  array (
    0 => 'lha',
  ),
  'application/x-mie' => 
  array (
    0 => 'mie',
  ),
  'application/x-ms-shortcut' => 
  array (
    0 => 'lnk',
  ),
  'application/x-msi' => 
  array (
    0 => 'msi',
  ),
  'application/x-cdf' => 
  array (
    0 => 'cdf',
    1 => 'cda',
  ),
  'application/x-nzb' => 
  array (
    0 => 'nzb',
  ),
  'chemical/x-galactic-spc' => 
  array (
    0 => 'spc',
  ),
  'application/x-research-info-systems' => 
  array (
    0 => 'ris',
  ),
  'application/x-sql' => 
  array (
    0 => 'sql',
  ),
  'application/x-subrip' => 
  array (
    0 => 'srt',
  ),
  'application/x-t3vm-image' => 
  array (
    0 => 't3',
  ),
  'application/x-tads' => 
  array (
    0 => 'gam',
  ),
  'chemical/x-gamess-input' => 
  array (
    0 => 'gam',
    1 => 'inp',
    2 => 'gamin',
  ),
  'application/x-tgif' => 
  array (
    0 => 'obj',
  ),
  'application/x-xliff+xml' => 
  array (
    0 => 'xlf',
  ),
  'application/x-xz' => 
  array (
    0 => 'xz',
  ),
  'application/x-zmachine' => 
  array (
    0 => 'z1',
    1 => 'z2',
    2 => 'z3',
    3 => 'z4',
    4 => 'z5',
    5 => 'z6',
    6 => 'z7',
    7 => 'z8',
  ),
  'application/xaml+xml' => 
  array (
    0 => 'xaml',
  ),
  'application/xproc+xml' => 
  array (
    0 => 'xpl',
  ),
  'audio/amr' => 
  array (
    0 => 'amr',
  ),
  'audio/s3m' => 
  array (
    0 => 's3m',
  ),
  'audio/silk' => 
  array (
    0 => 'sil',
  ),
  'audio/x-caf' => 
  array (
    0 => 'caf',
  ),
  'audio/x-flac' => 
  array (
    0 => 'flac',
  ),
  'audio/flac' => 
  array (
    0 => 'flac',
  ),
  'audio/x-matroska' => 
  array (
    0 => 'mka',
  ),
  'audio/x-realaudio' => 
  array (
    0 => 'ra',
  ),
  'audio/xm' => 
  array (
    0 => 'xm',
  ),
  'image/sgi' => 
  array (
    0 => 'sgi',
  ),
  'image/vnd.ms-photo' => 
  array (
    0 => 'wdp',
  ),
  'image/x-3ds' => 
  array (
    0 => '3ds',
  ),
  'image/x-mrsid-image' => 
  array (
    0 => 'sid',
  ),
  'audio/prs.sid' => 
  array (
    0 => 'sid',
  ),
  'image/x-tga' => 
  array (
    0 => 'tga',
  ),
  'model/x3d+binary' => 
  array (
    0 => 'x3db',
    1 => 'x3dbz',
  ),
  'model/x3d+vrml' => 
  array (
    0 => 'x3dv',
    1 => 'x3dvz',
  ),
  'text/cache-manifest' => 
  array (
    0 => 'appcache',
  ),
  'application/x-troff-man' => 
  array (
    0 => 'man',
  ),
  'application/x-troff-me' => 
  array (
    0 => 'me',
  ),
  'application/x-troff-ms' => 
  array (
    0 => 'ms',
  ),
  'text/x-chdr' => 
  array (
    0 => 'h',
  ),
  'text/x-nfo' => 
  array (
    0 => 'nfo',
  ),
  'text/x-opml' => 
  array (
    0 => 'opml',
  ),
  'text/x-sfv' => 
  array (
    0 => 'sfv',
  ),
  'video/vnd.dvb.file' => 
  array (
    0 => 'dvb',
  ),
  'video/x-matroska' => 
  array (
    0 => 'mkv',
    1 => 'mk3d',
    2 => 'mks',
    3 => 'mpv',
  ),
  'video/x-mng' => 
  array (
    0 => 'mng',
  ),
  'video/x-ms-vob' => 
  array (
    0 => 'vob',
  ),
  'video/x-smv' => 
  array (
    0 => 'smv',
  ),
  'application/annodex' => 
  array (
    0 => 'anx',
  ),
  'application/bbolin' => 
  array (
    0 => 'lin',
  ),
  'application/dicom' => 
  array (
    0 => 'dcm',
  ),
  'application/dsptype' => 
  array (
    0 => 'tsp',
  ),
  'application/hta' => 
  array (
    0 => 'hta',
  ),
  'application/pgp-keys' => 
  array (
    0 => 'key',
  ),
  'application/vnd.font-fontforge-sfd' => 
  array (
    0 => 'sfd',
  ),
  'application/vnd.stardivision.chart' => 
  array (
    0 => 'sds',
  ),
  'chemical/x-mdl-sdfile' => 
  array (
    0 => 'sdf',
    1 => 'sd',
  ),
  'application/x-comsol' => 
  array (
    0 => 'mph',
  ),
  'application/x-freemind' => 
  array (
    0 => 'mm',
  ),
  'application/x-ganttproject' => 
  array (
    0 => 'gan',
  ),
  'application/x-go-sgf' => 
  array (
    0 => 'sgf',
  ),
  'application/x-graphing-calculator' => 
  array (
    0 => 'gcf',
  ),
  'application/x-gtar-compressed' => 
  array (
    0 => 'tgz',
    1 => 'taz',
  ),
  'application/x-hwp' => 
  array (
    0 => 'hwp',
  ),
  'application/x-ica' => 
  array (
    0 => 'ica',
  ),
  'application/x-info' => 
  array (
    0 => 'info',
  ),
  'application/x-internet-signup' => 
  array (
    0 => 'ins',
    1 => 'isp',
  ),
  'application/x-iphone' => 
  array (
    0 => 'iii',
  ),
  'application/x-jmol' => 
  array (
    0 => 'jmz',
  ),
  'application/x-killustrator' => 
  array (
    0 => 'kil',
  ),
  'application/x-lyx' => 
  array (
    0 => 'lyx',
  ),
  'application/x-lzx' => 
  array (
    0 => 'lzx',
  ),
  'application/x-ms-manifest' => 
  array (
    0 => 'manifest',
  ),
  'application/x-ns-proxy-autoconfig' => 
  array (
    0 => 'pac',
  ),
  'application/x-nwc' => 
  array (
    0 => 'nwc',
  ),
  'application/x-object' => 
  array (
    0 => 'o',
  ),
  'application/x-oz-application' => 
  array (
    0 => 'oza',
  ),
  'application/x-python-code' => 
  array (
    0 => 'pyc',
    1 => 'pyo',
  ),
  'application/x-qgis' => 
  array (
    0 => 'qgs',
    1 => 'shp',
    2 => 'shx',
  ),
  'application/x-quicktimeplayer' => 
  array (
    0 => 'qtl',
  ),
  'application/x-rdp' => 
  array (
    0 => 'rdp',
  ),
  'application/x-redhat-package-manager' => 
  array (
    0 => 'rpm',
  ),
  'application/x-ruby' => 
  array (
    0 => 'rb',
  ),
  'application/x-scilab' => 
  array (
    0 => 'sci',
    1 => 'sce',
  ),
  'application/x-scilab-xcos' => 
  array (
    0 => 'xcos',
  ),
  'application/x-silverlight' => 
  array (
    0 => 'scr',
  ),
  'application/x-tex-gf' => 
  array (
    0 => 'gf',
  ),
  'application/x-tex-pk' => 
  array (
    0 => 'pk',
  ),
  'application/x-wingz' => 
  array (
    0 => 'wz',
  ),
  'application/x-xcf' => 
  array (
    0 => 'xcf',
  ),
  'audio/amr-wb' => 
  array (
    0 => 'awb',
  ),
  'audio/annodex' => 
  array (
    0 => 'axa',
  ),
  'audio/csound' => 
  array (
    0 => 'csd',
    1 => 'orc',
    2 => 'sco',
  ),
  'audio/x-gsm' => 
  array (
    0 => 'gsm',
  ),
  'chemical/x-alchemy' => 
  array (
    0 => 'alc',
  ),
  'chemical/x-cache' => 
  array (
    0 => 'cac',
    1 => 'cache',
  ),
  'chemical/x-cache-csf' => 
  array (
    0 => 'csf',
  ),
  'chemical/x-cactvs-binary' => 
  array (
    0 => 'cbin',
    1 => 'cascii',
    2 => 'ctab',
  ),
  'chemical/x-compass' => 
  array (
    0 => 'cpa',
  ),
  'chemical/x-crossfire' => 
  array (
    0 => 'bsd',
  ),
  'chemical/x-ctx' => 
  array (
    0 => 'ctx',
  ),
  'chemical/x-cxf' => 
  array (
    0 => 'cxf',
    1 => 'cef',
  ),
  'chemical/x-embl-dl-nucleotide' => 
  array (
    0 => 'emb',
    1 => 'embl',
  ),
  'chemical/x-gaussian-checkpoint' => 
  array (
    0 => 'fch',
    1 => 'fchk',
  ),
  'chemical/x-gaussian-cube' => 
  array (
    0 => 'cub',
  ),
  'chemical/x-gaussian-input' => 
  array (
    0 => 'gau',
    1 => 'gjc',
    2 => 'gjf',
  ),
  'chemical/x-gaussian-log' => 
  array (
    0 => 'gal',
  ),
  'chemical/x-genbank' => 
  array (
    0 => 'gen',
  ),
  'chemical/x-hin' => 
  array (
    0 => 'hin',
  ),
  'chemical/x-isostar' => 
  array (
    0 => 'istr',
    1 => 'ist',
  ),
  'chemical/x-jcamp-dx' => 
  array (
    0 => 'jdx',
    1 => 'dx',
  ),
  'chemical/x-kinemage' => 
  array (
    0 => 'kin',
  ),
  'chemical/x-macmolecule' => 
  array (
    0 => 'mcm',
  ),
  'chemical/x-mdl-molfile' => 
  array (
    0 => 'mol',
  ),
  'chemical/x-mdl-rdfile' => 
  array (
    0 => 'rd',
  ),
  'chemical/x-mdl-rxnfile' => 
  array (
    0 => 'rxn',
  ),
  'chemical/x-mdl-tgf' => 
  array (
    0 => 'tgf',
  ),
  'chemical/x-mmcif' => 
  array (
    0 => 'mcif',
  ),
  'chemical/x-mopac-graph' => 
  array (
    0 => 'gpt',
  ),
  'chemical/x-mopac-out' => 
  array (
    0 => 'moo',
  ),
  'chemical/x-rosdal' => 
  array (
    0 => 'ros',
  ),
  'chemical/x-swissprot' => 
  array (
    0 => 'sw',
  ),
  'chemical/x-vmd' => 
  array (
    0 => 'vmd',
  ),
  'chemical/x-xtel' => 
  array (
    0 => 'xtel',
  ),
  'image/jpx' => 
  array (
    0 => 'jpx',
    1 => 'jpf',
  ),
  'image/x-canon-crw' => 
  array (
    0 => 'crw',
  ),
  'image/x-coreldraw' => 
  array (
    0 => 'cdr',
  ),
  'image/x-coreldrawpattern' => 
  array (
    0 => 'pat',
  ),
  'image/x-coreldrawtemplate' => 
  array (
    0 => 'cdt',
  ),
  'image/x-epson-erf' => 
  array (
    0 => 'erf',
  ),
  'image/x-jg' => 
  array (
    0 => 'art',
  ),
  'image/x-jng' => 
  array (
    0 => 'jng',
  ),
  'image/x-nikon-nef' => 
  array (
    0 => 'nef',
  ),
  'image/x-olympus-orf' => 
  array (
    0 => 'orf',
  ),
  'text/iuls' => 
  array (
    0 => 'uls',
  ),
  'text/mathml' => 
  array (
    0 => 'mml',
  ),
  'text/markdown' => 
  array (
    0 => 'md',
    1 => 'markdown',
  ),
  'text/scriptlet' => 
  array (
    0 => 'sct',
    1 => 'wsc',
  ),
  'text/texmacs' => 
  array (
    0 => 'tm',
  ),
  'text/x-bibtex' => 
  array (
    0 => 'bib',
  ),
  'text/x-boo' => 
  array (
    0 => 'boo',
  ),
  'text/x-component' => 
  array (
    0 => 'htc',
  ),
  'text/x-dsrc' => 
  array (
    0 => 'd',
  ),
  'text/x-diff' => 
  array (
    0 => 'diff',
    1 => 'patch',
  ),
  'text/x-haskell' => 
  array (
    0 => 'hs',
  ),
  'text/x-lilypond' => 
  array (
    0 => 'ly',
  ),
  'text/x-literate-haskell' => 
  array (
    0 => 'lhs',
  ),
  'text/x-moc' => 
  array (
    0 => 'moc',
  ),
  'text/x-pcs-gcd' => 
  array (
    0 => 'gcd',
  ),
  'text/x-perl' => 
  array (
    0 => 'pl',
    1 => 'pm',
  ),
  'application/x-perl' => 
  array (
    0 => 'pl',
    1 => 'pm',
  ),
  'text/x-python' => 
  array (
    0 => 'py',
  ),
  'text/x-scala' => 
  array (
    0 => 'scala',
  ),
  'video/annodex' => 
  array (
    0 => 'axv',
  ),
  'video/dl' => 
  array (
    0 => 'dl',
  ),
  'video/dv' => 
  array (
    0 => 'dif',
    1 => 'dv',
  ),
  'video/gl' => 
  array (
    0 => 'gl',
  ),
  'video/x-la-asf' => 
  array (
    0 => 'lsf',
    1 => 'lsx',
  ),
  'application/x-cocoa' => 
  array (
    0 => 'cco',
  ),
  'application/x-java-archive-diff' => 
  array (
    0 => 'jardiff',
  ),
  'application/x-makeself' => 
  array (
    0 => 'run',
  ),
  'application/x-sea' => 
  array (
    0 => 'sea',
  ),
);