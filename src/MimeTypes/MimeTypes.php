<?php

namespace TextMedia\MimeTypes;

use \SplFileInfo;
use \ZipArchive;

/**
 * Сопоставление расширений и mime-типов
 */
class MimeTypes
{
    /** Неопределённый тип. */
    const UNDEFINED_MIME = 'application/octet-stream';

    /**
     * @var array Сопоставление extension => [mime-types]
     */
    protected static $mimeTypes = null;

    /**
     * @var array Сопоставление mime-type => [extensions]
     */
    protected static $extensions = null;

    /**
     * Получить расширения по mime-типу
     *
     * @param string $mimeType mime-тип
     *
     * @return array
     */
    public static function getExtenstionsByMime(string $mimeType): array
    {
        if (is_null(self::$extensions)) {
            self::$extensions = require(__DIR__ . '/Resources/extensions.php');
        }
        return array_key_exists($mimeType, self::$extensions) ? self::$extensions[$mimeType] : [];
    }

    /**
     * Получить mime-типы по расширению
     *
     * @param string $extension расширение
     *
     * @return array
     */
    public static function getMimesByExtension(string $extension): array
    {
        if (is_null(self::$mimeTypes)) {
            self::$mimeTypes = require(__DIR__ . '/Resources/mime-types.php');
        }
        $extension = strtolower(ltrim($extension, '.'));
        return array_key_exists($extension, self::$mimeTypes) ? self::$mimeTypes[$extension] : [];
    }

    /**
     * Определение mime-типа файла по содержимому
     *
     * @param string $file Имя файла
     *
     * @return string
     */
    public static function getFileMimeType(string $file): string
    {
        // определим стандартной функцией, если файл доступен для чтения
        $fileExists = (is_file($file) and is_readable($file));
        $mime = ($fileExists and function_exists('mime_content_type'))
            ? mime_content_type($file)
            : self::UNDEFINED_MIME;

        // не получилось - определим через finfo_file()
        if ((empty($mime) or self::UNDEFINED_MIME === $mime)
            and $fileExists
            and function_exists('finfo_open')
            and false !== ($finfo = finfo_open(FILEINFO_MIME_TYPE))
        ) {
            $mime = finfo_file($finfo, $file) ?: self::UNDEFINED_MIME;
            finfo_close($finfo);
        }

        // если тип неопределённый - проверим содержимое своими лапками
        switch ($mime) {
            case 'text/xml':
            case 'application/xml':
                $mime = self::checkXmlContent($file, $mime);
                break;
            case 'application/zip':
            case self::UNDEFINED_MIME:
                $mime = self::checkZipArchive($file, $mime);
                break;
        }

        // если так и не определились - попробуем определить по расширению
        if (empty($mime) or self::UNDEFINED_MIME === $mime) {
            $ext = self::getFileExtension($file);
            $mimes = $ext ? self::getMimesByExtension($ext) : [];
            if (0 !== count($mimes)) {
                $mime = reset($mimes);
            }
        }

        return $mime;
    }

    /**
     * Получение расширения файла.
     *
     * @param string $file
     */
    public static function getFileExtension(string $file): string
    {
        return strtolower((new SplFileInfo($file))->getExtension());
    }

    /**
     * Пост-обработка zip-архивов.
     *
     * @param string $file Путь к файлу.
     * @param string $mime Исходный тип.
     *
     * @return string
     */
    protected static function checkZipArchive(string $file, string $mime): string
    {
        $zip = new ZipArchive;
        if (true === $zip->open($file)) {
            if (false !== ($index = $zip->locateName('mimetype'))) {
                $mime = $zip->getFromIndex($index);
            } elseif (false !== $zip->locateName('word/document.xml')) {
                $mime = self::getMimesByExtension('docx')[0];
            } else {
                $mime = self::getMimesByExtension('zip')[0];
            }
            $zip->close();
        }
        return $mime;
    }

    /**
     * Пост-обработка xml-файлов.
     *
     * @param string $file Путь к файлу.
     * @param string $mime Исходный тип.
     *
     * @return string
     */
    protected static function checkXmlContent(string $file, string $mime): string
    {
        $head = file_get_contents($file, false, null, 0, 100);
        if (false !== strpos($head, '<FictionBook')) {
            $mime = self::getMimesByExtension('fb2')[0];
        }
        return $mime;
    }
}
