# MimeTypes

[![Packagist](https://img.shields.io/packagist/v/text-media/mime-types.svg)](https://packagist.org/packages/text-media/mime-types)
[![Packagist](https://img.shields.io/packagist/l/text-media/mime-types.svg)](https://packagist.org/packages/text-media/mime-types)

Пакет решает три задачи, не реализованные (первые две) или реализованные частично (третья - `mime_content_type` не даёт правильно все типы) в `php`:

 * определение списка расширений файла по его mime-типу;
 * определение списка mime-типов файла по его расширению;
 * опрерделение mime-типа файла по его содержимому.

## Установка

```bash
composer require text-media/mime-types
```

## Использование

```php
use TextMedia\MimeTypes\MimeTypes;

// определение списка расширений файла по его mime-типу
print_r(MimeTypes::getExtenstionsByMime('text/html'));

// определение списка mime-типов файла по его расширению
print_r(MimeTypes::getMimesByExtension('html'));

// опрерделение mime-типа файла по его содержимому
print_r(MimeTypes::getFileMimeType('/tmp/upload_xxxxx'));
```
