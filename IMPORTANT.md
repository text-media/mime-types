# ВАЖНО

Перед коммитом следует прогнать код через CodeSniffer: `vendor/bin/phpcs` в корне пакета.

Для стабильной версии следует назначить метку версии (см. https://semver.org/lang/ru/).

При обновлении пакета следует так же обновить сервисы, в которых он используется:

 * https://bitbucket.org/text-media/doccheck
 * https://bitbucket.org/text-media/regcheck
 * https://bitbucket.org/text-media/urlcheck
