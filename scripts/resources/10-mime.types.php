<?php return array (
  123 =>
  array (
    0 => 'application/vnd.lotus-1-2-3',
  ),
  '3dml' =>
  array (
    0 => 'text/vnd.in3d.3dml',
  ),
  '3g2' =>
  array (
    0 => 'video/3gpp2',
  ),
  '3gp' =>
  array (
    0 => 'video/3gpp',
  ),
  '7z' =>
  array (
    0 => 'application/x-7z-compressed',
  ),
  'aab' =>
  array (
    0 => 'application/x-authorware-bin',
  ),
  'aac' =>
  array (
    0 => 'audio/x-aac',
  ),
  'aam' =>
  array (
    0 => 'application/x-authorware-map',
  ),
  'aas' =>
  array (
    0 => 'application/x-authorware-seg',
  ),
  'abw' =>
  array (
    0 => 'application/x-abiword',
  ),
  'ac' =>
  array (
    0 => 'application/pkix-attr-cert',
  ),
  'acc' =>
  array (
    0 => 'application/vnd.americandynamics.acc',
  ),
  'ace' =>
  array (
    0 => 'application/x-ace-compressed',
  ),
  'acu' =>
  array (
    0 => 'application/vnd.acucobol',
  ),
  'adp' =>
  array (
    0 => 'audio/adpcm',
  ),
  'aep' =>
  array (
    0 => 'application/vnd.audiograph',
  ),
  'afp' =>
  array (
    0 => 'application/vnd.ibm.modcap',
  ),
  'ahead' =>
  array (
    0 => 'application/vnd.ahead.space',
  ),
  'ai' =>
  array (
    0 => 'application/postscript',
  ),
  'aif' =>
  array (
    0 => 'audio/x-aiff',
  ),
  'air' =>
  array (
    0 => 'application/vnd.adobe.air-application-installer-package+zip',
  ),
  'ait' =>
  array (
    0 => 'application/vnd.dvb.ait',
  ),
  'ami' =>
  array (
    0 => 'application/vnd.amiga.ami',
  ),
  'apk' =>
  array (
    0 => 'application/vnd.android.package-archive',
  ),
  'application' =>
  array (
    0 => 'application/x-ms-application',
  ),
  'apr' =>
  array (
    0 => 'application/vnd.lotus-approach',
  ),
  'asf' =>
  array (
    0 => 'video/x-ms-asf',
  ),
  'aso' =>
  array (
    0 => 'application/vnd.accpac.simply.aso',
  ),
  'atc' =>
  array (
    0 => 'application/vnd.acucorp',
  ),
  'atom' =>
  array (
    0 => 'application/atom+xml',
  ),
  'atomcat' =>
  array (
    0 => 'application/atomcat+xml',
  ),
  'atomsvc' =>
  array (
    0 => 'application/atomsvc+xml',
  ),
  'atx' =>
  array (
    0 => 'application/vnd.antix.game-component',
  ),
  'au' =>
  array (
    0 => 'audio/basic',
  ),
  'avi' =>
  array (
    0 => 'video/x-msvideo',
  ),
  'aw' =>
  array (
    0 => 'application/applixware',
  ),
  'azf' =>
  array (
    0 => 'application/vnd.airzip.filesecure.azf',
  ),
  'azs' =>
  array (
    0 => 'application/vnd.airzip.filesecure.azs',
  ),
  'azw' =>
  array (
    0 => 'application/vnd.amazon.ebook',
  ),
  'bcpio' =>
  array (
    0 => 'application/x-bcpio',
  ),
  'bdf' =>
  array (
    0 => 'application/x-font-bdf',
  ),
  'bdm' =>
  array (
    0 => 'application/vnd.syncml.dm+wbxml',
  ),
  'bed' =>
  array (
    0 => 'application/vnd.realvnc.bed',
  ),
  'bh2' =>
  array (
    0 => 'application/vnd.fujitsu.oasysprs',
  ),
  'bin' =>
  array (
    0 => 'application/octet-stream',
  ),
  'bmi' =>
  array (
    0 => 'application/vnd.bmi',
  ),
  'bmp' =>
  array (
    0 => 'image/bmp',
  ),
  'box' =>
  array (
    0 => 'application/vnd.previewsystems.box',
  ),
  'btif' =>
  array (
    0 => 'image/prs.btif',
  ),
  'bz' =>
  array (
    0 => 'application/x-bzip',
  ),
  'bz2' =>
  array (
    0 => 'application/x-bzip2',
  ),
  'c' =>
  array (
    0 => 'text/x-c',
  ),
  'c11amc' =>
  array (
    0 => 'application/vnd.cluetrust.cartomobile-config',
  ),
  'c11amz' =>
  array (
    0 => 'application/vnd.cluetrust.cartomobile-config-pkg',
  ),
  'c4g' =>
  array (
    0 => 'application/vnd.clonk.c4group',
  ),
  'cab' =>
  array (
    0 => 'application/vnd.ms-cab-compressed',
  ),
  'car' =>
  array (
    0 => 'application/vnd.curl.car',
  ),
  'cat' =>
  array (
    0 => 'application/vnd.ms-pki.seccat',
  ),
  'ccxml' =>
  array (
    0 => 'application/ccxml+xml,',
  ),
  'cdbcmsg' =>
  array (
    0 => 'application/vnd.contact.cmsg',
  ),
  'cdkey' =>
  array (
    0 => 'application/vnd.mediastation.cdkey',
  ),
  'cdmia' =>
  array (
    0 => 'application/cdmi-capability',
  ),
  'cdmic' =>
  array (
    0 => 'application/cdmi-container',
  ),
  'cdmid' =>
  array (
    0 => 'application/cdmi-domain',
  ),
  'cdmio' =>
  array (
    0 => 'application/cdmi-object',
  ),
  'cdmiq' =>
  array (
    0 => 'application/cdmi-queue',
  ),
  'cdx' =>
  array (
    0 => 'chemical/x-cdx',
  ),
  'cdxml' =>
  array (
    0 => 'application/vnd.chemdraw+xml',
  ),
  'cdy' =>
  array (
    0 => 'application/vnd.cinderella',
  ),
  'cer' =>
  array (
    0 => 'application/pkix-cert',
  ),
  'cgm' =>
  array (
    0 => 'image/cgm',
  ),
  'chat' =>
  array (
    0 => 'application/x-chat',
  ),
  'chm' =>
  array (
    0 => 'application/vnd.ms-htmlhelp',
  ),
  'chrt' =>
  array (
    0 => 'application/vnd.kde.kchart',
  ),
  'cif' =>
  array (
    0 => 'chemical/x-cif',
  ),
  'cii' =>
  array (
    0 => 'application/vnd.anser-web-certificate-issue-initiation',
  ),
  'cil' =>
  array (
    0 => 'application/vnd.ms-artgalry',
  ),
  'cla' =>
  array (
    0 => 'application/vnd.claymore',
  ),
  'class' =>
  array (
    0 => 'application/java-vm',
  ),
  'clkk' =>
  array (
    0 => 'application/vnd.crick.clicker.keyboard',
  ),
  'clkp' =>
  array (
    0 => 'application/vnd.crick.clicker.palette',
  ),
  'clkt' =>
  array (
    0 => 'application/vnd.crick.clicker.template',
  ),
  'clkw' =>
  array (
    0 => 'application/vnd.crick.clicker.wordbank',
  ),
  'clkx' =>
  array (
    0 => 'application/vnd.crick.clicker',
  ),
  'clp' =>
  array (
    0 => 'application/x-msclip',
  ),
  'cmc' =>
  array (
    0 => 'application/vnd.cosmocaller',
  ),
  'cmdf' =>
  array (
    0 => 'chemical/x-cmdf',
  ),
  'cml' =>
  array (
    0 => 'chemical/x-cml',
  ),
  'cmp' =>
  array (
    0 => 'application/vnd.yellowriver-custom-menu',
  ),
  'cmx' =>
  array (
    0 => 'image/x-cmx',
  ),
  'cod' =>
  array (
    0 => 'application/vnd.rim.cod',
  ),
  'cpio' =>
  array (
    0 => 'application/x-cpio',
  ),
  'cpt' =>
  array (
    0 => 'application/mac-compactpro',
  ),
  'crd' =>
  array (
    0 => 'application/x-mscardfile',
  ),
  'crl' =>
  array (
    0 => 'application/pkix-crl',
  ),
  'cryptonote' =>
  array (
    0 => 'application/vnd.rig.cryptonote',
  ),
  'csh' =>
  array (
    0 => 'application/x-csh',
  ),
  'csml' =>
  array (
    0 => 'chemical/x-csml',
  ),
  'csp' =>
  array (
    0 => 'application/vnd.commonspace',
  ),
  'css' =>
  array (
    0 => 'text/css',
  ),
  'csv' =>
  array (
    0 => 'text/csv',
  ),
  'cu' =>
  array (
    0 => 'application/cu-seeme',
  ),
  'curl' =>
  array (
    0 => 'text/vnd.curl',
  ),
  'cww' =>
  array (
    0 => 'application/prs.cww',
  ),
  'dae' =>
  array (
    0 => 'model/vnd.collada+xml',
  ),
  'daf' =>
  array (
    0 => 'application/vnd.mobius.daf',
  ),
  'davmount' =>
  array (
    0 => 'application/davmount+xml',
  ),
  'dcurl' =>
  array (
    0 => 'text/vnd.curl.dcurl',
  ),
  'dd2' =>
  array (
    0 => 'application/vnd.oma.dd2+xml',
  ),
  'ddd' =>
  array (
    0 => 'application/vnd.fujixerox.ddd',
  ),
  'deb' =>
  array (
    0 => 'application/x-debian-package',
  ),
  'der' =>
  array (
    0 => 'application/x-x509-ca-cert',
  ),
  'dfac' =>
  array (
    0 => 'application/vnd.dreamfactory',
  ),
  'dir' =>
  array (
    0 => 'application/x-director',
  ),
  'dis' =>
  array (
    0 => 'application/vnd.mobius.dis',
  ),
  'djvu' =>
  array (
    0 => 'image/vnd.djvu',
  ),
  'dna' =>
  array (
    0 => 'application/vnd.dna',
  ),
  'doc' =>
  array (
    0 => 'application/msword',
  ),
  'docm' =>
  array (
    0 => 'application/vnd.ms-word.document.macroenabled.12',
  ),
  'docx' =>
  array (
    0 => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  ),
  'dotm' =>
  array (
    0 => 'application/vnd.ms-word.template.macroenabled.12',
  ),
  'dotx' =>
  array (
    0 => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
  ),
  'dp' =>
  array (
    0 => 'application/vnd.osgi.dp',
  ),
  'dpg' =>
  array (
    0 => 'application/vnd.dpgraph',
  ),
  'dra' =>
  array (
    0 => 'audio/vnd.dra',
  ),
  'dsc' =>
  array (
    0 => 'text/prs.lines.tag',
  ),
  'dssc' =>
  array (
    0 => 'application/dssc+der',
  ),
  'dtb' =>
  array (
    0 => 'application/x-dtbook+xml',
  ),
  'dtd' =>
  array (
    0 => 'application/xml-dtd',
  ),
  'dts' =>
  array (
    0 => 'audio/vnd.dts',
  ),
  'dtshd' =>
  array (
    0 => 'audio/vnd.dts.hd',
  ),
  'dvi' =>
  array (
    0 => 'application/x-dvi',
  ),
  'dwf' =>
  array (
    0 => 'model/vnd.dwf',
  ),
  'dwg' =>
  array (
    0 => 'image/vnd.dwg',
  ),
  'dxf' =>
  array (
    0 => 'image/vnd.dxf',
  ),
  'dxp' =>
  array (
    0 => 'application/vnd.spotfire.dxp',
  ),
  'ecelp4800' =>
  array (
    0 => 'audio/vnd.nuera.ecelp4800',
  ),
  'ecelp7470' =>
  array (
    0 => 'audio/vnd.nuera.ecelp7470',
  ),
  'ecelp9600' =>
  array (
    0 => 'audio/vnd.nuera.ecelp9600',
  ),
  'edm' =>
  array (
    0 => 'application/vnd.novadigm.edm',
  ),
  'edx' =>
  array (
    0 => 'application/vnd.novadigm.edx',
  ),
  'efif' =>
  array (
    0 => 'application/vnd.picsel',
  ),
  'ei6' =>
  array (
    0 => 'application/vnd.pg.osasli',
  ),
  'eml' =>
  array (
    0 => 'message/rfc822',
  ),
  'emma' =>
  array (
    0 => 'application/emma+xml',
  ),
  'eol' =>
  array (
    0 => 'audio/vnd.digital-winds',
  ),
  'eot' =>
  array (
    0 => 'application/vnd.ms-fontobject',
  ),
  'epub' =>
  array (
    0 => 'application/epub+zip',
  ),
  'es' =>
  array (
    0 => 'application/ecmascript',
  ),
  'es3' =>
  array (
    0 => 'application/vnd.eszigno3+xml',
  ),
  'esf' =>
  array (
    0 => 'application/vnd.epson.esf',
  ),
  'etx' =>
  array (
    0 => 'text/x-setext',
  ),
  'exe' =>
  array (
    0 => 'application/x-msdownload',
  ),
  'exi' =>
  array (
    0 => 'application/exi',
  ),
  'ext' =>
  array (
    0 => 'application/vnd.novadigm.ext',
  ),
  'ez2' =>
  array (
    0 => 'application/vnd.ezpix-album',
  ),
  'ez3' =>
  array (
    0 => 'application/vnd.ezpix-package',
  ),
  'f' =>
  array (
    0 => 'text/x-fortran',
  ),
  'f4v' =>
  array (
    0 => 'video/x-f4v',
  ),
  'fbs' =>
  array (
    0 => 'image/vnd.fastbidsheet',
  ),
  'fcs' =>
  array (
    0 => 'application/vnd.isac.fcs',
  ),
  'fdf' =>
  array (
    0 => 'application/vnd.fdf',
  ),
  'fe_launch' =>
  array (
    0 => 'application/vnd.denovo.fcselayout-link',
  ),
  'fg5' =>
  array (
    0 => 'application/vnd.fujitsu.oasysgp',
  ),
  'fh' =>
  array (
    0 => 'image/x-freehand',
  ),
  'fig' =>
  array (
    0 => 'application/x-xfig',
  ),
  'fli' =>
  array (
    0 => 'video/x-fli',
  ),
  'flo' =>
  array (
    0 => 'application/vnd.micrografx.flo',
  ),
  'flv' =>
  array (
    0 => 'video/x-flv',
  ),
  'flw' =>
  array (
    0 => 'application/vnd.kde.kivio',
  ),
  'flx' =>
  array (
    0 => 'text/vnd.fmi.flexstor',
  ),
  'fly' =>
  array (
    0 => 'text/vnd.fly',
  ),
  'fm' =>
  array (
    0 => 'application/vnd.framemaker',
  ),
  'fnc' =>
  array (
    0 => 'application/vnd.frogans.fnc',
  ),
  'fpx' =>
  array (
    0 => 'image/vnd.fpx',
  ),
  'fsc' =>
  array (
    0 => 'application/vnd.fsc.weblaunch',
  ),
  'fst' =>
  array (
    0 => 'image/vnd.fst',
  ),
  'ftc' =>
  array (
    0 => 'application/vnd.fluxtime.clip',
  ),
  'fti' =>
  array (
    0 => 'application/vnd.anser-web-funds-transfer-initiation',
  ),
  'fvt' =>
  array (
    0 => 'video/vnd.fvt',
  ),
  'fxp' =>
  array (
    0 => 'application/vnd.adobe.fxp',
  ),
  'fzs' =>
  array (
    0 => 'application/vnd.fuzzysheet',
  ),
  'g2w' =>
  array (
    0 => 'application/vnd.geoplan',
  ),
  'g3' =>
  array (
    0 => 'image/g3fax',
  ),
  'g3w' =>
  array (
    0 => 'application/vnd.geospace',
  ),
  'gac' =>
  array (
    0 => 'application/vnd.groove-account',
  ),
  'gdl' =>
  array (
    0 => 'model/vnd.gdl',
  ),
  'geo' =>
  array (
    0 => 'application/vnd.dynageo',
  ),
  'gex' =>
  array (
    0 => 'application/vnd.geometry-explorer',
  ),
  'ggb' =>
  array (
    0 => 'application/vnd.geogebra.file',
  ),
  'ggt' =>
  array (
    0 => 'application/vnd.geogebra.tool',
  ),
  'ghf' =>
  array (
    0 => 'application/vnd.groove-help',
  ),
  'gif' =>
  array (
    0 => 'image/gif',
  ),
  'gim' =>
  array (
    0 => 'application/vnd.groove-identity-message',
  ),
  'gmx' =>
  array (
    0 => 'application/vnd.gmx',
  ),
  'gnumeric' =>
  array (
    0 => 'application/x-gnumeric',
  ),
  'gph' =>
  array (
    0 => 'application/vnd.flographit',
  ),
  'gqf' =>
  array (
    0 => 'application/vnd.grafeq',
  ),
  'gram' =>
  array (
    0 => 'application/srgs',
  ),
  'grv' =>
  array (
    0 => 'application/vnd.groove-injector',
  ),
  'grxml' =>
  array (
    0 => 'application/srgs+xml',
  ),
  'gsf' =>
  array (
    0 => 'application/x-font-ghostscript',
  ),
  'gtar' =>
  array (
    0 => 'application/x-gtar',
  ),
  'gtm' =>
  array (
    0 => 'application/vnd.groove-tool-message',
  ),
  'gtw' =>
  array (
    0 => 'model/vnd.gtw',
  ),
  'gv' =>
  array (
    0 => 'text/vnd.graphviz',
  ),
  'gxt' =>
  array (
    0 => 'application/vnd.geonext',
  ),
  'h261' =>
  array (
    0 => 'video/h261',
  ),
  'h263' =>
  array (
    0 => 'video/h263',
  ),
  'h264' =>
  array (
    0 => 'video/h264',
  ),
  'hal' =>
  array (
    0 => 'application/vnd.hal+xml',
  ),
  'hbci' =>
  array (
    0 => 'application/vnd.hbci',
  ),
  'hdf' =>
  array (
    0 => 'application/x-hdf',
  ),
  'hlp' =>
  array (
    0 => 'application/winhlp',
  ),
  'hpgl' =>
  array (
    0 => 'application/vnd.hp-hpgl',
  ),
  'hpid' =>
  array (
    0 => 'application/vnd.hp-hpid',
  ),
  'hps' =>
  array (
    0 => 'application/vnd.hp-hps',
  ),
  'hqx' =>
  array (
    0 => 'application/mac-binhex40',
  ),
  'htke' =>
  array (
    0 => 'application/vnd.kenameaapp',
  ),
  'html' =>
  array (
    0 => 'text/html',
  ),
  'hvd' =>
  array (
    0 => 'application/vnd.yamaha.hv-dic',
  ),
  'hvp' =>
  array (
    0 => 'application/vnd.yamaha.hv-voice',
  ),
  'hvs' =>
  array (
    0 => 'application/vnd.yamaha.hv-script',
  ),
  'i2g' =>
  array (
    0 => 'application/vnd.intergeo',
  ),
  'icc' =>
  array (
    0 => 'application/vnd.iccprofile',
  ),
  'ice' =>
  array (
    0 => 'x-conference/x-cooltalk',
  ),
  'ico' =>
  array (
    0 => 'image/x-icon',
  ),
  'ics' =>
  array (
    0 => 'text/calendar',
  ),
  'ief' =>
  array (
    0 => 'image/ief',
  ),
  'ifm' =>
  array (
    0 => 'application/vnd.shana.informed.formdata',
  ),
  'igl' =>
  array (
    0 => 'application/vnd.igloader',
  ),
  'igm' =>
  array (
    0 => 'application/vnd.insors.igm',
  ),
  'igs' =>
  array (
    0 => 'model/iges',
  ),
  'igx' =>
  array (
    0 => 'application/vnd.micrografx.igx',
  ),
  'iif' =>
  array (
    0 => 'application/vnd.shana.informed.interchange',
  ),
  'imp' =>
  array (
    0 => 'application/vnd.accpac.simply.imp',
  ),
  'ims' =>
  array (
    0 => 'application/vnd.ms-ims',
  ),
  'ipfix' =>
  array (
    0 => 'application/ipfix',
  ),
  'ipk' =>
  array (
    0 => 'application/vnd.shana.informed.package',
  ),
  'irm' =>
  array (
    0 => 'application/vnd.ibm.rights-management',
  ),
  'irp' =>
  array (
    0 => 'application/vnd.irepository.package+xml',
  ),
  'itp' =>
  array (
    0 => 'application/vnd.shana.informed.formtemplate',
  ),
  'ivp' =>
  array (
    0 => 'application/vnd.immervision-ivp',
  ),
  'ivu' =>
  array (
    0 => 'application/vnd.immervision-ivu',
  ),
  'jad' =>
  array (
    0 => 'text/vnd.sun.j2me.app-descriptor',
  ),
  'jam' =>
  array (
    0 => 'application/vnd.jam',
  ),
  'jar' =>
  array (
    0 => 'application/java-archive',
  ),
  'java' =>
  array (
    0 => 'text/x-java-source,java',
  ),
  'jisp' =>
  array (
    0 => 'application/vnd.jisp',
  ),
  'jlt' =>
  array (
    0 => 'application/vnd.hp-jlyt',
  ),
  'jnlp' =>
  array (
    0 => 'application/x-java-jnlp-file',
  ),
  'joda' =>
  array (
    0 => 'application/vnd.joost.joda-archive',
  ),
  'jpeg' =>
  array (
    0 => 'image/jpeg',
  ),
  'jpgv' =>
  array (
    0 => 'video/jpeg',
  ),
  'jpm' =>
  array (
    0 => 'video/jpm',
  ),
  'js' =>
  array (
    0 => 'application/javascript',
  ),
  'json' =>
  array (
    0 => 'application/json',
  ),
  'jsond' =>
  array (
    0 => 'application/json',
  ),
  'karbon' =>
  array (
    0 => 'application/vnd.kde.karbon',
  ),
  'kfo' =>
  array (
    0 => 'application/vnd.kde.kformula',
  ),
  'kia' =>
  array (
    0 => 'application/vnd.kidspiration',
  ),
  'kml' =>
  array (
    0 => 'application/vnd.google-earth.kml+xml',
  ),
  'kmz' =>
  array (
    0 => 'application/vnd.google-earth.kmz',
  ),
  'kne' =>
  array (
    0 => 'application/vnd.kinar',
  ),
  'kon' =>
  array (
    0 => 'application/vnd.kde.kontour',
  ),
  'kpr' =>
  array (
    0 => 'application/vnd.kde.kpresenter',
  ),
  'ksp' =>
  array (
    0 => 'application/vnd.kde.kspread',
  ),
  'ktx' =>
  array (
    0 => 'image/ktx',
  ),
  'ktz' =>
  array (
    0 => 'application/vnd.kahootz',
  ),
  'kwd' =>
  array (
    0 => 'application/vnd.kde.kword',
  ),
  'lasxml' =>
  array (
    0 => 'application/vnd.las.las+xml',
  ),
  'latex' =>
  array (
    0 => 'application/x-latex',
  ),
  'lbd' =>
  array (
    0 => 'application/vnd.llamagraphics.life-balance.desktop',
  ),
  'lbe' =>
  array (
    0 => 'application/vnd.llamagraphics.life-balance.exchange+xml',
  ),
  'les' =>
  array (
    0 => 'application/vnd.hhe.lesson-player',
  ),
  'link66' =>
  array (
    0 => 'application/vnd.route66.link66+xml',
  ),
  'lrm' =>
  array (
    0 => 'application/vnd.ms-lrm',
  ),
  'ltf' =>
  array (
    0 => 'application/vnd.frogans.ltf',
  ),
  'lvp' =>
  array (
    0 => 'audio/vnd.lucent.voice',
  ),
  'lwp' =>
  array (
    0 => 'application/vnd.lotus-wordpro',
  ),
  'm21' =>
  array (
    0 => 'application/mp21',
  ),
  'm3u' =>
  array (
    0 => 'audio/x-mpegurl',
  ),
  'm3u8' =>
  array (
    0 => 'application/vnd.apple.mpegurl',
  ),
  'm4v' =>
  array (
    0 => 'video/x-m4v',
  ),
  'ma' =>
  array (
    0 => 'application/mathematica',
  ),
  'mads' =>
  array (
    0 => 'application/mads+xml',
  ),
  'mag' =>
  array (
    0 => 'application/vnd.ecowin.chart',
  ),
  'mathml' =>
  array (
    0 => 'application/mathml+xml',
  ),
  'mbk' =>
  array (
    0 => 'application/vnd.mobius.mbk',
  ),
  'mbox' =>
  array (
    0 => 'application/mbox',
  ),
  'mc1' =>
  array (
    0 => 'application/vnd.medcalcdata',
  ),
  'mcd' =>
  array (
    0 => 'application/vnd.mcd',
  ),
  'mcurl' =>
  array (
    0 => 'text/vnd.curl.mcurl',
  ),
  'mdb' =>
  array (
    0 => 'application/x-msaccess',
  ),
  'mdi' =>
  array (
    0 => 'image/vnd.ms-modi',
  ),
  'meta4' =>
  array (
    0 => 'application/metalink4+xml',
  ),
  'mets' =>
  array (
    0 => 'application/mets+xml',
  ),
  'mfm' =>
  array (
    0 => 'application/vnd.mfmp',
  ),
  'mgp' =>
  array (
    0 => 'application/vnd.osgeo.mapguide.package',
  ),
  'mgz' =>
  array (
    0 => 'application/vnd.proteus.magazine',
  ),
  'mid' =>
  array (
    0 => 'audio/midi',
  ),
  'mif' =>
  array (
    0 => 'application/vnd.mif',
  ),
  'mj2' =>
  array (
    0 => 'video/mj2',
  ),
  'mlp' =>
  array (
    0 => 'application/vnd.dolby.mlp',
  ),
  'mmd' =>
  array (
    0 => 'application/vnd.chipnuts.karaoke-mmd',
  ),
  'mmf' =>
  array (
    0 => 'application/vnd.smaf',
  ),
  'mmr' =>
  array (
    0 => 'image/vnd.fujixerox.edmics-mmr',
  ),
  'mny' =>
  array (
    0 => 'application/x-msmoney',
  ),
  'mods' =>
  array (
    0 => 'application/mods+xml',
  ),
  'movie' =>
  array (
    0 => 'video/x-sgi-movie',
  ),
  'mp4' =>
  array (
    0 => 'video/mp4',
  ),
  'mp4a' =>
  array (
    0 => 'audio/mp4',
  ),
  'mpc' =>
  array (
    0 => 'application/vnd.mophun.certificate',
  ),
  'mpeg' =>
  array (
    0 => 'video/mpeg',
  ),
  'mpga' =>
  array (
    0 => 'audio/mpeg',
  ),
  'mpkg' =>
  array (
    0 => 'application/vnd.apple.installer+xml',
  ),
  'mpm' =>
  array (
    0 => 'application/vnd.blueice.multipass',
  ),
  'mpn' =>
  array (
    0 => 'application/vnd.mophun.application',
  ),
  'mpp' =>
  array (
    0 => 'application/vnd.ms-project',
  ),
  'mpy' =>
  array (
    0 => 'application/vnd.ibm.minipay',
  ),
  'mqy' =>
  array (
    0 => 'application/vnd.mobius.mqy',
  ),
  'mrc' =>
  array (
    0 => 'application/marc',
  ),
  'mrcx' =>
  array (
    0 => 'application/marcxml+xml',
  ),
  'mscml' =>
  array (
    0 => 'application/mediaservercontrol+xml',
  ),
  'mseq' =>
  array (
    0 => 'application/vnd.mseq',
  ),
  'msf' =>
  array (
    0 => 'application/vnd.epson.msf',
  ),
  'msh' =>
  array (
    0 => 'model/mesh',
  ),
  'msl' =>
  array (
    0 => 'application/vnd.mobius.msl',
  ),
  'msty' =>
  array (
    0 => 'application/vnd.muvee.style',
  ),
  'mts' =>
  array (
    0 => 'model/vnd.mts',
  ),
  'mus' =>
  array (
    0 => 'application/vnd.musician',
  ),
  'musicxml' =>
  array (
    0 => 'application/vnd.recordare.musicxml+xml',
  ),
  'mvb' =>
  array (
    0 => 'application/x-msmediaview',
  ),
  'mwf' =>
  array (
    0 => 'application/vnd.mfer',
  ),
  'mxf' =>
  array (
    0 => 'application/mxf',
  ),
  'mxl' =>
  array (
    0 => 'application/vnd.recordare.musicxml',
  ),
  'mxml' =>
  array (
    0 => 'application/xv+xml',
  ),
  'mxs' =>
  array (
    0 => 'application/vnd.triscape.mxs',
  ),
  'mxu' =>
  array (
    0 => 'video/vnd.mpegurl',
  ),
  'n-gage' =>
  array (
    0 => 'application/vnd.nokia.n-gage.symbian.install',
  ),
  'n3' =>
  array (
    0 => 'text/n3',
  ),
  'nbp' =>
  array (
    0 => 'application/vnd.wolfram.player',
  ),
  'nc' =>
  array (
    0 => 'application/x-netcdf',
  ),
  'ncx' =>
  array (
    0 => 'application/x-dtbncx+xml',
  ),
  'ngdat' =>
  array (
    0 => 'application/vnd.nokia.n-gage.data',
  ),
  'nlu' =>
  array (
    0 => 'application/vnd.neurolanguage.nlu',
  ),
  'nml' =>
  array (
    0 => 'application/vnd.enliven',
  ),
  'nnd' =>
  array (
    0 => 'application/vnd.noblenet-directory',
  ),
  'nns' =>
  array (
    0 => 'application/vnd.noblenet-sealer',
  ),
  'nnw' =>
  array (
    0 => 'application/vnd.noblenet-web',
  ),
  'npx' =>
  array (
    0 => 'image/vnd.net-fpx',
  ),
  'nsf' =>
  array (
    0 => 'application/vnd.lotus-notes',
  ),
  'oa2' =>
  array (
    0 => 'application/vnd.fujitsu.oasys2',
  ),
  'oa3' =>
  array (
    0 => 'application/vnd.fujitsu.oasys3',
  ),
  'oas' =>
  array (
    0 => 'application/vnd.fujitsu.oasys',
  ),
  'obd' =>
  array (
    0 => 'application/x-msbinder',
  ),
  'oda' =>
  array (
    0 => 'application/oda',
  ),
  'odb' =>
  array (
    0 => 'application/vnd.oasis.opendocument.database',
  ),
  'odc' =>
  array (
    0 => 'application/vnd.oasis.opendocument.chart',
  ),
  'odf' =>
  array (
    0 => 'application/vnd.oasis.opendocument.formula',
  ),
  'odft' =>
  array (
    0 => 'application/vnd.oasis.opendocument.formula-template',
  ),
  'odg' =>
  array (
    0 => 'application/vnd.oasis.opendocument.graphics',
  ),
  'odi' =>
  array (
    0 => 'application/vnd.oasis.opendocument.image',
  ),
  'odm' =>
  array (
    0 => 'application/vnd.oasis.opendocument.text-master',
  ),
  'odp' =>
  array (
    0 => 'application/vnd.oasis.opendocument.presentation',
  ),
  'ods' =>
  array (
    0 => 'application/vnd.oasis.opendocument.spreadsheet',
  ),
  'odt' =>
  array (
    0 => 'application/vnd.oasis.opendocument.text',
  ),
  'oga' =>
  array (
    0 => 'audio/ogg',
  ),
  'ogv' =>
  array (
    0 => 'video/ogg',
  ),
  'ogx' =>
  array (
    0 => 'application/ogg',
  ),
  'onetoc' =>
  array (
    0 => 'application/onenote',
  ),
  'opf' =>
  array (
    0 => 'application/oebps-package+xml',
  ),
  'org' =>
  array (
    0 => 'application/vnd.lotus-organizer',
  ),
  'osf' =>
  array (
    0 => 'application/vnd.yamaha.openscoreformat',
  ),
  'osfpvg' =>
  array (
    0 => 'application/vnd.yamaha.openscoreformat.osfpvg+xml',
  ),
  'otc' =>
  array (
    0 => 'application/vnd.oasis.opendocument.chart-template',
  ),
  'otf' =>
  array (
    0 => 'application/x-font-otf',
  ),
  'otg' =>
  array (
    0 => 'application/vnd.oasis.opendocument.graphics-template',
  ),
  'oth' =>
  array (
    0 => 'application/vnd.oasis.opendocument.text-web',
  ),
  'oti' =>
  array (
    0 => 'application/vnd.oasis.opendocument.image-template',
  ),
  'otp' =>
  array (
    0 => 'application/vnd.oasis.opendocument.presentation-template',
  ),
  'ots' =>
  array (
    0 => 'application/vnd.oasis.opendocument.spreadsheet-template',
  ),
  'ott' =>
  array (
    0 => 'application/vnd.oasis.opendocument.text-template',
  ),
  'oxt' =>
  array (
    0 => 'application/vnd.openofficeorg.extension',
  ),
  'p' =>
  array (
    0 => 'text/x-pascal',
  ),
  'p10' =>
  array (
    0 => 'application/pkcs10',
  ),
  'p12' =>
  array (
    0 => 'application/x-pkcs12',
  ),
  'p7b' =>
  array (
    0 => 'application/x-pkcs7-certificates',
  ),
  'p7m' =>
  array (
    0 => 'application/pkcs7-mime',
  ),
  'p7r' =>
  array (
    0 => 'application/x-pkcs7-certreqresp',
  ),
  'p7s' =>
  array (
    0 => 'application/pkcs7-signature',
  ),
  'p8' =>
  array (
    0 => 'application/pkcs8',
  ),
  'par' =>
  array (
    0 => 'text/plain-bas',
  ),
  'paw' =>
  array (
    0 => 'application/vnd.pawaafile',
  ),
  'pbd' =>
  array (
    0 => 'application/vnd.powerbuilder6',
  ),
  'pbm' =>
  array (
    0 => 'image/x-portable-bitmap',
  ),
  'pcf' =>
  array (
    0 => 'application/x-font-pcf',
  ),
  'pcl' =>
  array (
    0 => 'application/vnd.hp-pcl',
  ),
  'pclxl' =>
  array (
    0 => 'application/vnd.hp-pclxl',
  ),
  'pcurl' =>
  array (
    0 => 'application/vnd.curl.pcurl',
  ),
  'pcx' =>
  array (
    0 => 'image/x-pcx',
  ),
  'pdb' =>
  array (
    0 => 'application/vnd.palm',
  ),
  'pdf' =>
  array (
    0 => 'application/pdf',
  ),
  'pfa' =>
  array (
    0 => 'application/x-font-type1',
  ),
  'pfr' =>
  array (
    0 => 'application/font-tdpfr',
  ),
  'pgm' =>
  array (
    0 => 'image/x-portable-graymap',
  ),
  'pgn' =>
  array (
    0 => 'application/x-chess-pgn',
  ),
  'pgp' =>
  array (
    0 => 'application/pgp-signature',
  ),
  'pic' =>
  array (
    0 => 'image/x-pict',
  ),
  'pki' =>
  array (
    0 => 'application/pkixcmp',
  ),
  'pkipath' =>
  array (
    0 => 'application/pkix-pkipath',
  ),
  'plb' =>
  array (
    0 => 'application/vnd.3gpp.pic-bw-large',
  ),
  'plc' =>
  array (
    0 => 'application/vnd.mobius.plc',
  ),
  'plf' =>
  array (
    0 => 'application/vnd.pocketlearn',
  ),
  'pls' =>
  array (
    0 => 'application/pls+xml',
  ),
  'pml' =>
  array (
    0 => 'application/vnd.ctc-posml',
  ),
  'png' =>
  array (
    0 => 'image/png',
  ),
  'pnm' =>
  array (
    0 => 'image/x-portable-anymap',
  ),
  'portpkg' =>
  array (
    0 => 'application/vnd.macports.portpkg',
  ),
  'potm' =>
  array (
    0 => 'application/vnd.ms-powerpoint.template.macroenabled.12',
  ),
  'potx' =>
  array (
    0 => 'application/vnd.openxmlformats-officedocument.presentationml.template',
  ),
  'ppam' =>
  array (
    0 => 'application/vnd.ms-powerpoint.addin.macroenabled.12',
  ),
  'ppd' =>
  array (
    0 => 'application/vnd.cups-ppd',
  ),
  'ppm' =>
  array (
    0 => 'image/x-portable-pixmap',
  ),
  'ppsm' =>
  array (
    0 => 'application/vnd.ms-powerpoint.slideshow.macroenabled.12',
  ),
  'ppsx' =>
  array (
    0 => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
  ),
  'ppt' =>
  array (
    0 => 'application/vnd.ms-powerpoint',
  ),
  'pptm' =>
  array (
    0 => 'application/vnd.ms-powerpoint.presentation.macroenabled.12',
  ),
  'pptx' =>
  array (
    0 => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  ),
  'prc' =>
  array (
    0 => 'application/x-mobipocket-ebook',
  ),
  'pre' =>
  array (
    0 => 'application/vnd.lotus-freelance',
  ),
  'prf' =>
  array (
    0 => 'application/pics-rules',
  ),
  'psb' =>
  array (
    0 => 'application/vnd.3gpp.pic-bw-small',
  ),
  'psd' =>
  array (
    0 => 'image/vnd.adobe.photoshop',
  ),
  'psf' =>
  array (
    0 => 'application/x-font-linux-psf',
  ),
  'pskcxml' =>
  array (
    0 => 'application/pskc+xml',
  ),
  'ptid' =>
  array (
    0 => 'application/vnd.pvi.ptid1',
  ),
  'pub' =>
  array (
    0 => 'application/x-mspublisher',
  ),
  'pvb' =>
  array (
    0 => 'application/vnd.3gpp.pic-bw-var',
  ),
  'pwn' =>
  array (
    0 => 'application/vnd.3m.post-it-notes',
  ),
  'pya' =>
  array (
    0 => 'audio/vnd.ms-playready.media.pya',
  ),
  'pyv' =>
  array (
    0 => 'video/vnd.ms-playready.media.pyv',
  ),
  'qam' =>
  array (
    0 => 'application/vnd.epson.quickanime',
  ),
  'qbo' =>
  array (
    0 => 'application/vnd.intu.qbo',
  ),
  'qfx' =>
  array (
    0 => 'application/vnd.intu.qfx',
  ),
  'qps' =>
  array (
    0 => 'application/vnd.publishare-delta-tree',
  ),
  'qt' =>
  array (
    0 => 'video/quicktime',
  ),
  'qxd' =>
  array (
    0 => 'application/vnd.quark.quarkxpress',
  ),
  'ram' =>
  array (
    0 => 'audio/x-pn-realaudio',
  ),
  'rar' =>
  array (
    0 => 'application/x-rar-compressed',
  ),
  'ras' =>
  array (
    0 => 'image/x-cmu-raster',
  ),
  'rcprofile' =>
  array (
    0 => 'application/vnd.ipunplugged.rcprofile',
  ),
  'rdf' =>
  array (
    0 => 'application/rdf+xml',
  ),
  'rdz' =>
  array (
    0 => 'application/vnd.data-vision.rdz',
  ),
  'rep' =>
  array (
    0 => 'application/vnd.businessobjects',
  ),
  'res' =>
  array (
    0 => 'application/x-dtbresource+xml',
  ),
  'rgb' =>
  array (
    0 => 'image/x-rgb',
  ),
  'rif' =>
  array (
    0 => 'application/reginfo+xml',
  ),
  'rip' =>
  array (
    0 => 'audio/vnd.rip',
  ),
  'rl' =>
  array (
    0 => 'application/resource-lists+xml',
  ),
  'rlc' =>
  array (
    0 => 'image/vnd.fujixerox.edmics-rlc',
  ),
  'rld' =>
  array (
    0 => 'application/resource-lists-diff+xml',
  ),
  'rm' =>
  array (
    0 => 'application/vnd.rn-realmedia',
  ),
  'rmp' =>
  array (
    0 => 'audio/x-pn-realaudio-plugin',
  ),
  'rms' =>
  array (
    0 => 'application/vnd.jcp.javame.midlet-rms',
  ),
  'rnc' =>
  array (
    0 => 'application/relax-ng-compact-syntax',
  ),
  'rp9' =>
  array (
    0 => 'application/vnd.cloanto.rp9',
  ),
  'rpss' =>
  array (
    0 => 'application/vnd.nokia.radio-presets',
  ),
  'rpst' =>
  array (
    0 => 'application/vnd.nokia.radio-preset',
  ),
  'rq' =>
  array (
    0 => 'application/sparql-query',
  ),
  'rs' =>
  array (
    0 => 'application/rls-services+xml',
  ),
  'rsd' =>
  array (
    0 => 'application/rsd+xml',
  ),
  'rss' =>
  array (
    0 => 'application/rss+xml',
  ),
  'rtf' =>
  array (
    0 => 'application/rtf',
  ),
  'rtx' =>
  array (
    0 => 'text/richtext',
  ),
  's' =>
  array (
    0 => 'text/x-asm',
  ),
  'saf' =>
  array (
    0 => 'application/vnd.yamaha.smaf-audio',
  ),
  'sbml' =>
  array (
    0 => 'application/sbml+xml',
  ),
  'sc' =>
  array (
    0 => 'application/vnd.ibm.secure-container',
  ),
  'scd' =>
  array (
    0 => 'application/x-msschedule',
  ),
  'scm' =>
  array (
    0 => 'application/vnd.lotus-screencam',
  ),
  'scq' =>
  array (
    0 => 'application/scvp-cv-request',
  ),
  'scs' =>
  array (
    0 => 'application/scvp-cv-response',
  ),
  'scurl' =>
  array (
    0 => 'text/vnd.curl.scurl',
  ),
  'sda' =>
  array (
    0 => 'application/vnd.stardivision.draw',
  ),
  'sdc' =>
  array (
    0 => 'application/vnd.stardivision.calc',
  ),
  'sdd' =>
  array (
    0 => 'application/vnd.stardivision.impress',
  ),
  'sdkm' =>
  array (
    0 => 'application/vnd.solent.sdkm+xml',
  ),
  'sdp' =>
  array (
    0 => 'application/sdp',
  ),
  'sdw' =>
  array (
    0 => 'application/vnd.stardivision.writer',
  ),
  'see' =>
  array (
    0 => 'application/vnd.seemail',
  ),
  'seed' =>
  array (
    0 => 'application/vnd.fdsn.seed',
  ),
  'sema' =>
  array (
    0 => 'application/vnd.sema',
  ),
  'semd' =>
  array (
    0 => 'application/vnd.semd',
  ),
  'semf' =>
  array (
    0 => 'application/vnd.semf',
  ),
  'ser' =>
  array (
    0 => 'application/java-serialized-object',
  ),
  'setpay' =>
  array (
    0 => 'application/set-payment-initiation',
  ),
  'setreg' =>
  array (
    0 => 'application/set-registration-initiation',
  ),
  'sfd-hdstx' =>
  array (
    0 => 'application/vnd.hydrostatix.sof-data',
  ),
  'sfs' =>
  array (
    0 => 'application/vnd.spotfire.sfs',
  ),
  'sgl' =>
  array (
    0 => 'application/vnd.stardivision.writer-global',
  ),
  'sgml' =>
  array (
    0 => 'text/sgml',
  ),
  'sh' =>
  array (
    0 => 'application/x-sh',
  ),
  'shar' =>
  array (
    0 => 'application/x-shar',
  ),
  'shf' =>
  array (
    0 => 'application/shf+xml',
  ),
  'sis' =>
  array (
    0 => 'application/vnd.symbian.install',
  ),
  'sit' =>
  array (
    0 => 'application/x-stuffit',
  ),
  'sitx' =>
  array (
    0 => 'application/x-stuffitx',
  ),
  'skp' =>
  array (
    0 => 'application/vnd.koan',
  ),
  'sldm' =>
  array (
    0 => 'application/vnd.ms-powerpoint.slide.macroenabled.12',
  ),
  'sldx' =>
  array (
    0 => 'application/vnd.openxmlformats-officedocument.presentationml.slide',
  ),
  'slt' =>
  array (
    0 => 'application/vnd.epson.salt',
  ),
  'sm' =>
  array (
    0 => 'application/vnd.stepmania.stepchart',
  ),
  'smf' =>
  array (
    0 => 'application/vnd.stardivision.math',
  ),
  'smi' =>
  array (
    0 => 'application/smil+xml',
  ),
  'snf' =>
  array (
    0 => 'application/x-font-snf',
  ),
  'spf' =>
  array (
    0 => 'application/vnd.yamaha.smaf-phrase',
  ),
  'spl' =>
  array (
    0 => 'application/x-futuresplash',
  ),
  'spot' =>
  array (
    0 => 'text/vnd.in3d.spot',
  ),
  'spp' =>
  array (
    0 => 'application/scvp-vp-response',
  ),
  'spq' =>
  array (
    0 => 'application/scvp-vp-request',
  ),
  'src' =>
  array (
    0 => 'application/x-wais-source',
  ),
  'sru' =>
  array (
    0 => 'application/sru+xml',
  ),
  'srx' =>
  array (
    0 => 'application/sparql-results+xml',
  ),
  'sse' =>
  array (
    0 => 'application/vnd.kodak-descriptor',
  ),
  'ssf' =>
  array (
    0 => 'application/vnd.epson.ssf',
  ),
  'ssml' =>
  array (
    0 => 'application/ssml+xml',
  ),
  'st' =>
  array (
    0 => 'application/vnd.sailingtracker.track',
  ),
  'stc' =>
  array (
    0 => 'application/vnd.sun.xml.calc.template',
  ),
  'std' =>
  array (
    0 => 'application/vnd.sun.xml.draw.template',
  ),
  'stf' =>
  array (
    0 => 'application/vnd.wt.stf',
  ),
  'sti' =>
  array (
    0 => 'application/vnd.sun.xml.impress.template',
  ),
  'stk' =>
  array (
    0 => 'application/hyperstudio',
  ),
  'stl' =>
  array (
    0 => 'application/vnd.ms-pki.stl',
  ),
  'str' =>
  array (
    0 => 'application/vnd.pg.format',
  ),
  'stw' =>
  array (
    0 => 'application/vnd.sun.xml.writer.template',
  ),
  'sub' =>
  array (
    0 => 'image/vnd.dvb.subtitle',
  ),
  'sus' =>
  array (
    0 => 'application/vnd.sus-calendar',
  ),
  'sv4cpio' =>
  array (
    0 => 'application/x-sv4cpio',
  ),
  'sv4crc' =>
  array (
    0 => 'application/x-sv4crc',
  ),
  'svc' =>
  array (
    0 => 'application/vnd.dvb.service',
  ),
  'svd' =>
  array (
    0 => 'application/vnd.svd',
  ),
  'svg' =>
  array (
    0 => 'image/svg+xml',
  ),
  'swf' =>
  array (
    0 => 'application/x-shockwave-flash',
  ),
  'swi' =>
  array (
    0 => 'application/vnd.aristanetworks.swi',
  ),
  'sxc' =>
  array (
    0 => 'application/vnd.sun.xml.calc',
  ),
  'sxd' =>
  array (
    0 => 'application/vnd.sun.xml.draw',
  ),
  'sxg' =>
  array (
    0 => 'application/vnd.sun.xml.writer.global',
  ),
  'sxi' =>
  array (
    0 => 'application/vnd.sun.xml.impress',
  ),
  'sxm' =>
  array (
    0 => 'application/vnd.sun.xml.math',
  ),
  'sxw' =>
  array (
    0 => 'application/vnd.sun.xml.writer',
  ),
  't' =>
  array (
    0 => 'text/troff',
  ),
  'tao' =>
  array (
    0 => 'application/vnd.tao.intent-module-archive',
  ),
  'tar' =>
  array (
    0 => 'application/x-tar',
  ),
  'tcap' =>
  array (
    0 => 'application/vnd.3gpp2.tcap',
  ),
  'tcl' =>
  array (
    0 => 'application/x-tcl',
  ),
  'teacher' =>
  array (
    0 => 'application/vnd.smart.teacher',
  ),
  'tei' =>
  array (
    0 => 'application/tei+xml',
  ),
  'tex' =>
  array (
    0 => 'application/x-tex',
  ),
  'texinfo' =>
  array (
    0 => 'application/x-texinfo',
  ),
  'tfi' =>
  array (
    0 => 'application/thraud+xml',
  ),
  'tfm' =>
  array (
    0 => 'application/x-tex-tfm',
  ),
  'thmx' =>
  array (
    0 => 'application/vnd.ms-officetheme',
  ),
  'tiff' =>
  array (
    0 => 'image/tiff',
  ),
  'tmo' =>
  array (
    0 => 'application/vnd.tmobile-livetv',
  ),
  'torrent' =>
  array (
    0 => 'application/x-bittorrent',
  ),
  'tpl' =>
  array (
    0 => 'application/vnd.groove-tool-template',
  ),
  'tpt' =>
  array (
    0 => 'application/vnd.trid.tpt',
  ),
  'tra' =>
  array (
    0 => 'application/vnd.trueapp',
  ),
  'trm' =>
  array (
    0 => 'application/x-msterminal',
  ),
  'tsd' =>
  array (
    0 => 'application/timestamped-data',
  ),
  'tsv' =>
  array (
    0 => 'text/tab-separated-values',
  ),
  'ttf' =>
  array (
    0 => 'application/x-font-ttf',
  ),
  'ttl' =>
  array (
    0 => 'text/turtle',
  ),
  'twd' =>
  array (
    0 => 'application/vnd.simtech-mindmapper',
  ),
  'txd' =>
  array (
    0 => 'application/vnd.genomatix.tuxedo',
  ),
  'txf' =>
  array (
    0 => 'application/vnd.mobius.txf',
  ),
  'txt' =>
  array (
    0 => 'text/plain',
  ),
  'ufd' =>
  array (
    0 => 'application/vnd.ufdl',
  ),
  'umj' =>
  array (
    0 => 'application/vnd.umajin',
  ),
  'unityweb' =>
  array (
    0 => 'application/vnd.unity',
  ),
  'uoml' =>
  array (
    0 => 'application/vnd.uoml+xml',
  ),
  'uri' =>
  array (
    0 => 'text/uri-list',
  ),
  'ustar' =>
  array (
    0 => 'application/x-ustar',
  ),
  'utz' =>
  array (
    0 => 'application/vnd.uiq.theme',
  ),
  'uu' =>
  array (
    0 => 'text/x-uuencode',
  ),
  'uva' =>
  array (
    0 => 'audio/vnd.dece.audio',
  ),
  'uvh' =>
  array (
    0 => 'video/vnd.dece.hd',
  ),
  'uvi' =>
  array (
    0 => 'image/vnd.dece.graphic',
  ),
  'uvm' =>
  array (
    0 => 'video/vnd.dece.mobile',
  ),
  'uvp' =>
  array (
    0 => 'video/vnd.dece.pd',
  ),
  'uvs' =>
  array (
    0 => 'video/vnd.dece.sd',
  ),
  'uvu' =>
  array (
    0 => 'video/vnd.uvvu.mp4',
  ),
  'uvv' =>
  array (
    0 => 'video/vnd.dece.video',
  ),
  'vcd' =>
  array (
    0 => 'application/x-cdlink',
  ),
  'vcf' =>
  array (
    0 => 'text/x-vcard',
  ),
  'vcg' =>
  array (
    0 => 'application/vnd.groove-vcard',
  ),
  'vcs' =>
  array (
    0 => 'text/x-vcalendar',
  ),
  'vcx' =>
  array (
    0 => 'application/vnd.vcx',
  ),
  'vis' =>
  array (
    0 => 'application/vnd.visionary',
  ),
  'viv' =>
  array (
    0 => 'video/vnd.vivo',
  ),
  'vsd' =>
  array (
    0 => 'application/vnd.visio',
  ),
  'vsf' =>
  array (
    0 => 'application/vnd.vsf',
  ),
  'vtu' =>
  array (
    0 => 'model/vnd.vtu',
  ),
  'vxml' =>
  array (
    0 => 'application/voicexml+xml',
  ),
  'wad' =>
  array (
    0 => 'application/x-doom',
  ),
  'wav' =>
  array (
    0 => 'audio/x-wav',
  ),
  'wax' =>
  array (
    0 => 'audio/x-ms-wax',
  ),
  'wbmp' =>
  array (
    0 => 'image/vnd.wap.wbmp',
  ),
  'wbs' =>
  array (
    0 => 'application/vnd.criticaltools.wbs+xml',
  ),
  'wbxml' =>
  array (
    0 => 'application/vnd.wap.wbxml',
  ),
  'weba' =>
  array (
    0 => 'audio/webm',
  ),
  'webm' =>
  array (
    0 => 'video/webm',
  ),
  'webp' =>
  array (
    0 => 'image/webp',
  ),
  'wg' =>
  array (
    0 => 'application/vnd.pmi.widget',
  ),
  'wgt' =>
  array (
    0 => 'application/widget',
  ),
  'wm' =>
  array (
    0 => 'video/x-ms-wm',
  ),
  'wma' =>
  array (
    0 => 'audio/x-ms-wma',
  ),
  'wmd' =>
  array (
    0 => 'application/x-ms-wmd',
  ),
  'wmf' =>
  array (
    0 => 'application/x-msmetafile',
  ),
  'wml' =>
  array (
    0 => 'text/vnd.wap.wml',
  ),
  'wmlc' =>
  array (
    0 => 'application/vnd.wap.wmlc',
  ),
  'wmls' =>
  array (
    0 => 'text/vnd.wap.wmlscript',
  ),
  'wmlsc' =>
  array (
    0 => 'application/vnd.wap.wmlscriptc',
  ),
  'wmv' =>
  array (
    0 => 'video/x-ms-wmv',
  ),
  'wmx' =>
  array (
    0 => 'video/x-ms-wmx',
  ),
  'wmz' =>
  array (
    0 => 'application/x-ms-wmz',
  ),
  'woff' =>
  array (
    0 => 'application/x-font-woff',
  ),
  'woff2' =>
  array (
    0 => 'application/x-font-woff2',
  ),
  'wpd' =>
  array (
    0 => 'application/vnd.wordperfect',
  ),
  'wpl' =>
  array (
    0 => 'application/vnd.ms-wpl',
  ),
  'wps' =>
  array (
    0 => 'application/vnd.ms-works',
  ),
  'wqd' =>
  array (
    0 => 'application/vnd.wqd',
  ),
  'wri' =>
  array (
    0 => 'application/x-mswrite',
  ),
  'wrl' =>
  array (
    0 => 'model/vrml',
  ),
  'wsdl' =>
  array (
    0 => 'application/wsdl+xml',
  ),
  'wspolicy' =>
  array (
    0 => 'application/wspolicy+xml',
  ),
  'wtb' =>
  array (
    0 => 'application/vnd.webturbo',
  ),
  'wvx' =>
  array (
    0 => 'video/x-ms-wvx',
  ),
  'x3d' =>
  array (
    0 => 'application/vnd.hzn-3d-crossword',
  ),
  'xap' =>
  array (
    0 => 'application/x-silverlight-app',
  ),
  'xar' =>
  array (
    0 => 'application/vnd.xara',
  ),
  'xbap' =>
  array (
    0 => 'application/x-ms-xbap',
  ),
  'xbd' =>
  array (
    0 => 'application/vnd.fujixerox.docuworks.binder',
  ),
  'xbm' =>
  array (
    0 => 'image/x-xbitmap',
  ),
  'xdf' =>
  array (
    0 => 'application/xcap-diff+xml',
  ),
  'xdm' =>
  array (
    0 => 'application/vnd.syncml.dm+xml',
  ),
  'xdp' =>
  array (
    0 => 'application/vnd.adobe.xdp+xml',
  ),
  'xdssc' =>
  array (
    0 => 'application/dssc+xml',
  ),
  'xdw' =>
  array (
    0 => 'application/vnd.fujixerox.docuworks',
  ),
  'xenc' =>
  array (
    0 => 'application/xenc+xml',
  ),
  'xer' =>
  array (
    0 => 'application/patch-ops-error+xml',
  ),
  'xfdf' =>
  array (
    0 => 'application/vnd.adobe.xfdf',
  ),
  'xfdl' =>
  array (
    0 => 'application/vnd.xfdl',
  ),
  'xhtml' =>
  array (
    0 => 'application/xhtml+xml',
  ),
  'xif' =>
  array (
    0 => 'image/vnd.xiff',
  ),
  'xlam' =>
  array (
    0 => 'application/vnd.ms-excel.addin.macroenabled.12',
  ),
  'xls' =>
  array (
    0 => 'application/vnd.ms-excel',
  ),
  'xlsb' =>
  array (
    0 => 'application/vnd.ms-excel.sheet.binary.macroenabled.12',
  ),
  'xlsm' =>
  array (
    0 => 'application/vnd.ms-excel.sheet.macroenabled.12',
  ),
  'xlsx' =>
  array (
    0 => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  ),
  'xltm' =>
  array (
    0 => 'application/vnd.ms-excel.template.macroenabled.12',
  ),
  'xltx' =>
  array (
    0 => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
  ),
  'xml' =>
  array (
    0 => 'application/xml',
  ),
  'xo' =>
  array (
    0 => 'application/vnd.olpc-sugar',
  ),
  'xop' =>
  array (
    0 => 'application/xop+xml',
  ),
  'xpi' =>
  array (
    0 => 'application/x-xpinstall',
  ),
  'xpm' =>
  array (
    0 => 'image/x-xpixmap',
  ),
  'xpr' =>
  array (
    0 => 'application/vnd.is-xpr',
  ),
  'xps' =>
  array (
    0 => 'application/vnd.ms-xpsdocument',
  ),
  'xpw' =>
  array (
    0 => 'application/vnd.intercon.formnet',
  ),
  'xslt' =>
  array (
    0 => 'application/xslt+xml',
  ),
  'xsm' =>
  array (
    0 => 'application/vnd.syncml+xml',
  ),
  'xspf' =>
  array (
    0 => 'application/xspf+xml',
  ),
  'xul' =>
  array (
    0 => 'application/vnd.mozilla.xul+xml',
  ),
  'xwd' =>
  array (
    0 => 'image/x-xwindowdump',
  ),
  'xyz' =>
  array (
    0 => 'chemical/x-xyz',
  ),
  'yaml' =>
  array (
    0 => 'text/yaml',
  ),
  'yang' =>
  array (
    0 => 'application/yang',
  ),
  'yin' =>
  array (
    0 => 'application/yin+xml',
  ),
  'zaz' =>
  array (
    0 => 'application/vnd.zzazz.deck+xml',
  ),
  'zip' =>
  array (
    0 => 'application/zip',
  ),
  'zir' =>
  array (
    0 => 'application/vnd.zul',
  ),
);
