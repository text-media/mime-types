#!/usr/bin/env php
<?php

/**
 * Вывод ошибки и завершение скрипта
 *
 * @param string $message Текст ошибки
 */
$throwError = function (string $message) {
    printf("\e[41;97m %s \e[0m\n", $message) and exit(0xff);
};

// Сопоставления mime-type и extension
$mimeTypes = $extensions = [];

// Прочтём кастомные php-файлы (часть взята из других проектов)
$currPath = dirname(realpath(__FILE__));
foreach (glob("{$currPath}/resources/*-mime.types.php") as $file) {
    foreach (require($file) as $ext => $types) {
        if (!array_key_exists($ext, $mimeTypes)) {
            $mimeTypes[$ext] = [];
        }
        foreach ($types as $type) {
            if (!in_array($type, $mimeTypes[$ext])) {
                array_push($mimeTypes[$ext], $type);
            }
        }
    }
}

// Прочтём системные файлы и их копии в репозитории
$sources = [
    "{$currPath}/resources/linux.mime.types",
    "{$currPath}/resources/nginx.mime.types",
    '/etc/mime.types',
    '/etc/nginx/mime.types',
];
foreach ($sources as $file) {
    if (!is_file($file) or ! is_readable($file)) {
        continue;
    }

    foreach (explode("\n", file_get_contents($file)) as $line) {
        $line = trim(preg_replace('#\s\s+#', ' ', $line), " \t\n\r\0\x0B;");
        if (empty($line)) {
            continue;
        }

        if (!preg_match('#^([a-z]+/[a-z\-\.]+)( [a-z]+)+$#', $line)) {
            continue;
        }

        list($type, $exts) = explode(' ', $line, 2);
        foreach (explode(' ', $exts) as $ext) {
            if (!array_key_exists($ext, $mimeTypes)) {
                $mimeTypes[$ext] = [];
            }
            if (!in_array($type, $mimeTypes[$ext])) {
                array_push($mimeTypes[$ext], $type);
            }
        }
    }
}

if (0 === count($mimeTypes)) {
    $throwError('Не удалось определить набор mime-типов');
}

$savePath = realpath("{$currPath}/../src/MimeTypes/Resources");
if (!is_writable($savePath)) {
    $throwError("Папка {$savePath} не доступна для записи");
}

/**
 * Сохранение ресурсов
 *
 * @param string $file Файл
 * @param array  $data Данные
 */
$saveResource = function (string $file, array $data) use ($throwError) {
    if (is_file($file)) {
        if (is_writable($file)) {
            unlink($file);
        } else {
            $throwError("Невозможно переписать {$file}");
        }
    }
    file_put_contents($file, '<?php return ' . var_export($data, true) . ';');
};

// Сохраним сопоставление extension => [mime-types]
$saveResource("{$savePath}/mime-types.php", $mimeTypes);

// Сопоставим mime-type => [extensions]
foreach ($mimeTypes as $ext => $types) {
    foreach ($types as $type) {
        if (!array_key_exists($type, $extensions)) {
            $extensions[$type] = [];
        }
        if (!in_array($ext, $extensions[$type])) {
            array_push($extensions[$type], $ext);
        }
    }
}

// Сохраним сопоставление mime-type => [extensions]
$saveResource("{$savePath}/extensions.php", $extensions);

// Готово
exit(0);
