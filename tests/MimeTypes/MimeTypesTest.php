<?php

namespace TextMedia\MimeTypes\Tests;

use \TextMedia\MimeTypes\MimeTypes;

/**
 * Тесты.
 */
class MimeTypesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Проверка разбора файла
     *
     * @param string $file Файл для проверки
     * @param string $hash Хэш правильного mime-типа
     *
     * @dataProvider fileProvider
     */
    public function testParser(string $file, string $hash)
    {
        $mime = MimeTypes::getFileMimeType($file);
        $this->assertEquals(md5($mime), $hash);
    }

    /**
     * @return array
     */
    public function fileProvider(): array
    {
        return array_filter(array_map(function ($file) {
            return [$file, explode('-', basename($file))[1]];
        }, glob(__DIR__ . '/Resources/*')));
    }
}
