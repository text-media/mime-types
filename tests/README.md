# Тесты

В корне проекта выполните:

```bash
./vendor/bin/phpunit -c ./
```

Файлы для тестов находятся в папке `tests/FileParser/Resources`.
Имена тестируемых файлов должны иметь вид: <префикс>-<имя_типа>, где

 * <префикс> - произвольная последовательность для введения различия междй файлами одного типа;
 * <имя_типа> - `md5`-хэш от `mime-type`.
